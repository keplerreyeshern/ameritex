<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComunicadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comunicados', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug');
            $table->text('titulo');
            $table->text('subtitulo')->nullable();
            $table->text('intro')->nullable();
            $table->text('descripcion');
            $table->string('autor')->nullable();
            $table->integer('fecha')->nullable();
            $table->boolean('active')->default(true);
            $table->boolean('destacado')->default(false);
            $table->string('fotocomunicado')->nullable();
            $table->string('fotolistado')->nullable();
            $table->string('pdf')->nullable();
            $table->unsignedInteger('categoria_id');
            $table->foreign('categoria_id')->references('id')->on('categoria_comunicados')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comunicados');
    }
}
