<?php

use Illuminate\Database\Seeder;
use App\Seccion;

class SeccionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $seccionServicios = new Seccion();
        $seccionServicios->modelo = 'servicios';
        $seccionServicios->contenido = '<p>Contenido de mi secci&oacute;n de servicios&nbsp;</p>';
        $seccionServicios->save();


        $seccionQuienesSomos = new Seccion();
        $seccionQuienesSomos->modelo = "quienes-somos";
        $seccionQuienesSomos->contenido =
            '<p>Con m&aacute;s de 5 d&eacute;cadas de experiencia marcada por una trayectoria de &eacute;xito, seguimos brindando las mejores experiencias y los mejores servicios a nuestros clientes.</p>
            <p>&nbsp;</p>
            <p>Los Internacionales nos esmeramos en cada evento para crear el mejor ambiente de alta calidad, ya que contamos con la mejor tecnolog&iacute;a en audio, video, iluminaci&oacute;n, escenario y escenograf&iacute;a para cubrir perfectamente las especificaciones de los mejores eventos.</p>
            <p>&nbsp;</p>
            <p>Dise&ntilde;amos cada evento con m&uacute;sica, coreograf&iacute;a y entretenimiento de acuerdo a las necesidades de cada evento.</p>
            <p>&nbsp;</p>
            <p>Ofrecemos:</p>
            <div class="content">
            <ul>
            <li>Diversi&oacute;n, entretenimiento sin pausas.</li>
            <li>Servicio incomparable en la planeaci&oacute;n de eventos y ejecuci&oacute;n de nuestros servicios.</li>
            <li>El mejor repertorio disponible con grandes &eacute;xitos musicales.</li>
            <li>Producci&oacute;n, con el Staff m&aacute;s profesional del medio musical.</li>
            </ul>
            </div>';
        $seccionQuienesSomos->save();
    }
}
