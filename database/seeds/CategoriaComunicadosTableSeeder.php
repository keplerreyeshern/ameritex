<?php

use Illuminate\Database\Seeder;
use App\CategoriaComunicado;

class CategoriaComunicadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria = new CategoriaComunicado;
        $categoria->nombre = "noticias";
        $categoria->save();
    }
}
