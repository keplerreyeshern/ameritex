<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ServiciosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $urlImagen = asset("/base/img/servicios/fotografia.jpg");

        $filename = 'fotografia.jpg';
        \Storage::disk('servicios')->put($filename,file_get_contents($urlImagen));

        DB::table('servicios')->insert(
            [
                'titulo' => 'Fotografía',
                'imagen' => $filename,
                'descripcion' => '<h5>Una buena fotograf&iacute;a es esencial en la elecci&oacute;n de un producto, en&nbsp;<strong>FOTOGRAFIK</strong>&nbsp;hacemos que esa&nbsp;imagen sea brillante y que tu proyecto muestre todo su potencial.</h5>
                    <p>Creamos una &ldquo;Primera Gran Impresi&oacute;n&rdquo; en donde tu producto transmitir&aacute; mensajes y sensaciones a sus consumidores mediante un concepto s&oacute;lido. Cada producto, persona o lugar tiene ciertas caracter&iacute;sticas formales y superficiales que lo hacen &uacute;nico. Nos preocupamos de resaltar esas caracter&iacute;sticas y obtener el mejor resultado visual de cada una de ellas mediante complejos esquemas de iluminaci&oacute;n dise&ntilde;ados espec&iacute;ficamente para cada uno de ellos</p>'
            ]
        );
    }
}
