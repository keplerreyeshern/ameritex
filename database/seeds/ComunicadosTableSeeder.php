<?php

use Illuminate\Database\Seeder;
use App\CategoriaComunicado;
use App\Comunicado;

class ComunicadosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoriaNoticias = CategoriaComunicado::where('nombre', 'noticias')->first();
        if ($categoriaNoticias) {
            $comunicadoNoticia = new Comunicado;
            $comunicadoNoticia->titulo = "Título de mi noticia";
            $comunicadoNoticia->descripcion = "Descripción de mi noticia";
            $comunicadoNoticia->categoria_id = $categoriaNoticias->id;
            $comunicadoNoticia->fotolistado = "noticia1.jpeg";
            $comunicadoNoticia->save();
        }
    }
}
