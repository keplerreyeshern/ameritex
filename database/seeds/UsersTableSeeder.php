<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gerardo = new User;
        $gerardo->nombre    = "Gerardo";
        $gerardo->apellido  = "Trujillo";
        $gerardo->email     = "trujillo@excess.com.mx";
//        $gerardo->password  = \Hash::Make('xsadmin');
        $gerardo->password  = 'xsadmin';
        $gerardo->save();
        $gerardo->assignRole('Administrador');

        $erik = new User;
        $erik->nombre    = "Erik";
        $erik->apellido  = "Pérez";
        $erik->email     = "erik_al1@hotmail.com";
//        $erik->password  = \Hash::Make('secret_erik');
        $erik->password  = 'secret_erik';
        $erik->save();
        $erik->assignRole('Administrador');

    }
}
