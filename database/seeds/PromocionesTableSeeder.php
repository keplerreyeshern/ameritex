<?php

use Illuminate\Database\Seeder;

class PromocionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $urlImagen = asset("/base/img/promociones/promocion-verano.jpeg");

        $filename = 'promocion-verano.jpeg';
        \Storage::disk('promociones')->put($filename,file_get_contents($urlImagen));

        DB::table('promociones')->insert(
            [
                'nombre' => 'Promoción de verano',
                'slug' => 'promocion-de-verano',
                'imagen' => 'promocion-verano.jpeg',
                'contenido' => '<p style="font-weight: 400;">Parar todos aquellos que anhelan las mejores vacaciones en un lugar paradis&iacute;aco, Expedia te trae las mejores opciones. Aprovecha para viajar a precios &uacute;nicos con nuestras ofertas de verano. Para hacer realidad el descanso que necesitas con uno de esos viajes con familia que siempre so&ntilde;aste. Elige hoteles con servicios para cada uno de los integrantes de tu familia y disfruta de las vacaciones de verano.</p>
<p style="font-weight: 400;">&nbsp;</p>
<p style="font-weight: 400;">Encuentra hoteles en las mejores playas de M&eacute;xico con todos los servicios a excelentes precios. Ven a uno de los destinos m&aacute;s buscados por turistas de todo el mundo por su infraestructura moderna y playas de ensue&ntilde;o. Aguas de color turquesa, arenas blancas y los mejores centros comerciales te aguardan si eliges alguno de los&nbsp;<a href="https://www.expedia.mx/Cancun-Hoteles.d179995.Guia-Viajes-Hoteles">hoteles en Canc&uacute;n</a>. Ven a disfrutar este destino de Quintana Roo con los descuentos de Expedia. Tambi&eacute;n con nosotros puedes reservar los vuelos a cada destino desde nuestra p&aacute;gina. En M&eacute;xico llega al primer puerto internacional tur&iacute;stico, uno de esos destinos que a&ntilde;o tras a&ntilde;o siguen eligiendo miles y miles de turistas. Con los&nbsp;<a href="https://www.expedia.mx/Vuelos-Baratos-Acapulco.d179991.Guia-de-vuelos">vuelos a Acapulco</a>&nbsp;podr&aacute;s encontrarte con la experiencia de vacacionar a lo grande. Elige una promoci&oacute;n de Expedia y conoce cu&aacute;l es el motivo de que este destino sea conocido como la Perla del Pac&iacute;fico.</p>
<p style="font-weight: 400;">&nbsp;</p>
<p style="font-weight: 400;">Cuando viajamos en familia muchas veces intentamos simplificar el armado del viaje. Raz&oacute;n por la cual Expedia te ofrece paquetes a muchos destinos dentro y fuera del pa&iacute;s. Para disfrutar las vacaciones en una playa puedes elegir alguno de los&nbsp;<a href="https://www.expedia.mx/Puerto-Vallarta.d180016.Guia-de-vacaciones">paquetes a Puerto Vallarta</a>. Vuelo, estad&iacute;a, traslados, y m&aacute;s todo en una sola compra. Con hoteles que te ofrecen actividades para los ni&ntilde;os en un ambiente de playas incre&iacute;bles.</p>
<p style="font-weight: 400;">&nbsp;</p>
<p style="font-weight: 400;">Arma tu escapada de verano en familia ahora y disfruta de unas vacaciones memorables con nuestros descuentos. Viajar con Expedia es mucho m&aacute;s f&aacute;cil.</p>'
            ]
        );
    }
}
