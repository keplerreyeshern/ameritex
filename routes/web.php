<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// Language
Route::get('lang/{locale}', function ($locale) {
    // App::setLocale(session($locale));
    Session(['lang' => $locale]);
    return redirect()->back();
})->name('lang');

Route::get('/', 'HomeController@index')->name('home');
Route::get('home', function() {
    return redirect('admin');
});

/*
 *  Rutas Comunicados
 * */
Route::get('comunicados/{categoria}/{slug}', 'Cliente\ComunicadoController@detalle');
Route::resource('comunicados', 'Cliente\ComunicadoController', ['only' => ['index']]);

/*
 * Rutas estáticas (se pueden editar con tinymce)
 *
 * */

Route::get('quienes-somos', 'Cliente\EstaticoController@quienesSomos')->name('quienes-somos');

Route::get('productos', 'Cliente\EstaticoController@PRODUCTOS')->name('productos');

Route::get('calidad', 'Cliente\EstaticoController@calidad')->name('calidad');
Route::get('tecnologia', 'Cliente\EstaticoController@tecnologia')->name('tecnologia');
Route::get('diseno', 'Cliente\EstaticoController@diseno')->name('diseno');
Route::get('quienes-somos', 'Cliente\EstaticoController@quienesSomos')->name('quienes-somos');
Route::get('proveedores', 'Cliente\EstaticoController@proveedores')->name('proveedores');
Route::get('sustentabilidad', 'Cliente\EstaticoController@sustentabilidad')->name('sutentabilidad');


Route::get('aviso-de-privacidad', 'Cliente\EstaticoController@avisoPrivacidad')->name('aviso-de-privacidad');
//Route::view('/aviso-de-privacidad', 'cliente.aviso-de-privacidad');





Route::get('areas-de-practica', 'Cliente\EstaticoController@areasDePractica')->name('areas-de-practica');

Route::view('/ubicacion', 'cliente.ubicacion');


/*Rutas Galerias*/
Route::get('galeria', 'Cliente\GaleriaController@index')->name('galerias.index');
Route::get('galeria/{slug}', 'Cliente\GaleriaController@detalle')->name('galerias.detalle');

/* Rutas promociones */
Route::get('promociones/{slug}', 'Cliente\PromocionController@show')->name('promociones.show');

/* Rutas productos
Route::get('productos', 'Cliente\ProductoController@index')->name('productos.index');
Route::get('productos/{slug}', 'Cliente\ProductoController@show')->name('productos.show');
Route::get('productos/categoria/{slug}', 'Cliente\ProductoController@categoria')->name('productos.categoria');
*/


/* contacto */
Route::view('contacto', 'cliente.contacto')->name('contacto');
Route::post('mail/contacto', 'ContactoController@mailContacto')->name('mail.contacto');
Route::get('email', function() {
    return new \App\Mail\Contacto();
});

/*
 *  Rutas Protegidas
 * */
Route::middleware('auth')->group(function () {
    // Rutas con prefijo admin
    Route::group(['prefix'=>'admin', 'namespace' => '\Admin', 'as'=>'admin.'],function(){
        // User controller
        Route::get('', 'UserController@index');
        Route::post('users/desactivar/{id}', 'UserController@disable')->name('users.desactivar');
        Route::post('users/activar/{id}', 'UserController@enable')->name('users.activar');
        Route::resource('users','UserController');



        // Comunicado controller
        Route::get('comunicados/{categoria}/create', 'ComunicadoController@create')->name('comunicados.create');
        Route::get('comunicados/{categoria}', 'ComunicadoController@indexPorCategoria');

        Route::get('comunicados/{categoria}/create', 'ComunicadoController@create')->name('comunicados.create');
        Route::post('comunicados/activar/{id}', 'ComunicadoController@enable')->name('comunicados.activar');
        Route::post('comunicados/desactivar/{id}', 'ComunicadoController@disable')->name('comunicados.desactivar');

        Route::resource('comunicados','ComunicadoController', ['only' => ['edit', 'store', 'update', 'destroy']]);




        Route::resource('certificado/calidad','QualityCertificateController');
        Route::post('certificado/calidad/activar/{id}', 'QualityCertificateController@Enable')->name('qualities.enable');
        Route::post('certificado/calidad/desactivar/{id}', 'QualityCertificateController@Disable')->name('qualities.disable');

// parrafos controller


        //Route::get('parrafos/', 'ParrafoController@index')->name('parrafos.index');
        //Route::get('parrafos/{id}/edit', 'ParrafoController@edit')->name('parrafos.edit');
        //  Route::get('parrafos/{id}/update', 'ParrafoController@update')->name('parrafos.update');
        //  Route::get('parrafos/create', 'ParrafoController@create')->name('parrafos.create');
        Route::get('parrafos/destroy/{id}', 'ParrafoController@destroy')->name('parrafos.destroy');

        Route::resource('parrafos','ParrafoController', ['only' => ['edit', 'store', 'update', 'destroy', 'index', 'create']]);


        // Servicio controller
        Route::post('servicios/activar/{id}', 'ServicioController@enable')->name('servicios.activar');
        Route::post('servicios/desactivar/{id}', 'ServicioController@disable')->name('servicios.desactivar');
        Route::resource('servicios','ServicioController', ['only' => ['index', 'create', 'store', 'edit', 'destroy', 'update']]);

        // Secciones controller
        //
        Route::resource('secciones','SeccionController', ['only' => ['store']]);
        Route::get('secciones/{servicio}/create','SeccionController@createByModel')->name('secciones.model.create');

        // Fotografia controller
        Route::post('galerias/activar/{id}', 'FotografiaController@enable')->name('galerias.activar');
        Route::post('galerias/desactivar/{id}', 'FotografiaController@disable')->name('galerias.desactivar');
        Route::resource('galerias','FotografiaController', ['only' => ['index', 'create', 'store', 'edit', 'destroy']]);

        // Galería controller
        Route::post('galeria/image/remove/{id}','GaleriaController@removeImageFromGallery');
        Route::post('galeria/image/add/{id}','GaleriaController@addImageGallery')->name('gallery.image.add');
        Route::resource('galeria','GaleriaController', ['only' => ['update']]);

        // Promocion controller
        /*
         * En estas se repite admin en la url de la siguiente forma: /admin/admin/promociones/activar/{id}
         * verificar si no afecta el comportamiento y eliminar
         *
         * */
        Route::post('admin/promociones/activar/{id}', 'PromocionController@Enable')->name('promociones.activar');
        Route::post('admin/promociones/desactivar/{id}', 'PromocionController@Disable')->name('promociones.desactivar');


        Route::resource('promociones','PromocionController', ['except' =>
            ['show']
        ]);


        // Producto controller

        Route::post('/productos/activar/{id}', 'ProductoController@Enable')->name('productos.activar');
        Route::post('/productos/desactivar/{id}', 'ProductoController@Disable')->name('productos.desactivar');
        // Route::resource('productos','ProductoController', ['only' => ['index', 'create', 'store', 'edit', 'destroy', 'update']]);
        Route::post('/productos/{producto}', 'ProductoController@update')->name('productos.update');
        Route::resource('productos','ProductoController', ['only' => ['index', 'create', 'store', 'edit', 'destroy']]);

        Route::group(['prefix'=>'productos', 'as'=>'productos.'],function(){

            // CategoriaProductoController
            Route::get('categorias/obtener', 'CategoriaProductoController@categories')->name('categorias.obtener');
            Route::resource('categorias','CategoriaProductoController', ['only' => ['index', 'store', 'destroy', 'update']]);
        });
        /*// Producto controller
        Route::resource('productos','ProductoController', ['only' => ['index', 'create']]);*/

    }) ;
});



// Rutas únicas del proyecto
// Servicios
Route::group(['prefix'=>'servicios', 'as'=>'servicios.'],function(){
    Route::get('proyectos-y-construccion', function () {
        $servicios = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'Proyectos y construcción');
        })->get();
        return view('cliente.servicios.proyectos-y-construccion', compact('servicios'));
    });
    Route::get('mantenimiento-residencial', function () {
        $servicios = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'Mantenimiento residencial');
        })->get();
        return view('cliente.servicios.mantenimiento-residencial', compact('servicios'));
    });
    Route::get('diseno-mobiliario-y-decoracion', function () {
        $servicios = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'Diseño mobiliario y decoración');
        })->get();
        return view('cliente.servicios.diseno-mobiliario-y-decoracion', compact('servicios'));
    });
});
// Materiales


Route::get('materiales/{material}', 'Cliente\ProductoController@index');


/*
Route::get('materiales/{material}', function ($material = null) {

   $producto = App\Producto::where('slug', 'like', '%' . $material . '%')->first();

   //sacar la galeria del producto



   $productos = App\Producto::whereHas('categorias', function ($query) {
       $query->where('nombre', '$material');
   })->get();


    return view('cliente.materiales.material', compact('producto'));
});
*/


/*
Route::group(['prefix'=>'materiales', 'as'=>'materiales.'],function(){





    Route::get('lajas', function () {


        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'lajas');
        })->get();


        return view('cliente.materiales.lajas', compact('productos'));
    });





    Route::get('basalto', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'basalto');
        })->get();
        return view('cliente.materiales.basalto', compact('productos'));
    });
    Route::get('cantera', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'cantera');
        })->get();
        return view('cliente.materiales.cantera', compact('productos'));
    });
    Route::get('marmol', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'marmol');
        })->get();
        return view('cliente.materiales.marmol', compact('productos'));
    });

    Route::get('granito', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'granito');
        })->get();
        return view('cliente.materiales.granito', compact('productos'));
    });

    Route::get('onyx', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'onyx');
        })->get();
        return view('cliente.materiales.onyx', compact('productos'));
    });

    Route::get('recinto', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'recinto');
        })->get();
        return view('cliente.materiales.recinto', compact('productos'));
    });

    Route::get('silestone', function () {
        $productos = App\Producto::whereHas('categorias', function ($query) {
            $query->where('nombre', 'silestone');
        })->get();
        return view('cliente.materiales.silestone', compact('productos'));
    });
});

*/


Route::get('portafolio', function() {

    $portafolio = App\Galeria::where('slug', 'like', '%port%')->first();

    return view('cliente/portafolio', compact('portafolio'));

})->name('portafolio');