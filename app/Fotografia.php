<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fotografia extends Model
{
    public function galerias () {
        return $this->morphMany(Galeria::class,'galeriable');
    }
}
