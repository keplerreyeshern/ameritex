<?php


namespace App;


use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\DateFormater;
use Spatie\Translatable\HasTranslations;


class parrafo extends Model
{
    use HasTranslations;

    //para que la suba
    //
    public $translatable = ['titulo', 'contenido'];

    protected $table = "parrafos";
    protected $fillable = ['ref','titulo', 'contenido', 'categoria_id'];


    public function categoria()
    {
        return $this->belongsTo('App\CategoriaParrafo');
    }
}
