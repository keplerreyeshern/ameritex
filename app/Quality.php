<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Quality extends Model
{
    use Sluggable, HasTranslations;

    public $translatable = ['slug', 'title', 'content'];

    protected $table = "qualities";
    protected $fillable = [
        'slug'
        ,'title'
        ,'image'
        ,'content'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getUrlimageAttribute(){
        return asset('files/quality/'.$this->image);
    }

    public function gettitleAttribute($value)
    {
        $json = json_decode($value, true)[app()->getLocale()];
        if($json) {
            return $json;
        } else {
            return $value;
        }
    }

    public function getContentAttribute($value)
    {
        $json = json_decode($value, true)[app()->getLocale()];
        if($json) {
            return $json;
        } else {
            return $value;
        }
    }
}
