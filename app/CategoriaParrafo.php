<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CategoriaParrafo extends Model
{
    public function parrafos () {
        return $this->hasMany('App\Parrafo', 'categoria_id')->orderBy('nombre', 'asc');
    }
}
