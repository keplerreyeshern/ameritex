<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Spatie\Translatable\HasTranslations;

class Promocion extends Model
{
    use Sluggable, HasTranslations;

    public $translatable = ['slug', 'nombre', 'contenido'];

    protected $table = "promociones";
    protected $fillable = ['slug'
        ,'nombre'
        ,'imagen'
        ,'liga'
        ,'contenido'
    ];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function setImagenAttribute($imagen)
    {
        $name = Carbon::now()->month
            ."-".Carbon::now()->day
            ."-".Carbon::now()->hour
            ."-".Carbon::now()->minute
            ."-".Carbon::now()->second
            ."-".$imagen->getClientOriginalName();
        $this->attributes['imagen'] = $name;
        \Storage::disk('promociones')->put($name,\File::get($imagen));
    }


    public function getUrlimagenAttribute(){
        return asset('files/promociones/'.$this->imagen);
    }
    
    public function getNombreAttribute($value)
    {
        $json = json_decode($value, true)[app()->getLocale()];
        if($json) {
            return $json;
        } else {
            return $value;
        }
    }

    public function getContenidoAttribute($value)
    {
        $json = json_decode($value, true)[app()->getLocale()];
        if($json) {
            return $json;
        } else {
            return $value;
        }
    }

}
