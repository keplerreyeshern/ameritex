<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

use Spatie\Translatable\HasTranslations;


class Servicio extends Model
{
    use HasTranslations;

    public $translatable = ['titulo','descripcion'];

    protected $fillable = ['titulo', 'descripcion', 'imagen'];

    /**
     * @param $fotolistado
     */
    public function setImagenAttribute($imagen)
    {
        $name = Carbon::now()->month
            ."-".Carbon::now()->day
            ."-".Carbon::now()->hour
            ."-".Carbon::now()->minute
            ."-".Carbon::now()->second
            ."-".$imagen->getClientOriginalName();
        $this->attributes['imagen'] = $name;
        \Storage::disk('servicios')->put($name,\File::get($imagen));

    }
    public function getUrlImagenAttribute(){
        if($this->imagen != '') {
            return "/files/servicios/" . $this->imagen;
        }
        return null;
    }



}
