<?php

namespace App\Http\Controllers\Cliente;

use App\Galeria;
use App\Parrafo;
use App\Quality;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Seccion;
use App\Servicio;

class EstaticoController extends Controller
{
    public function quienesSomos () {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->whereIn('categoria_id', [1, 8])->get();
        $parrafos = $parrafos->keyBy('ref');


        return view('cliente.quienes-somos', compact(['parrafos']));
    }

    public function productos () {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 4)->get();
        $parrafos = $parrafos->keyBy('ref');

        $productos = Galeria::where('id', 26)->first();

        $deportivo = Galeria::where('id', 3)->first();

        $intima = Galeria::where('id', 4)->first();

        $tecnicos = Galeria::where('id', 25)->first();

        $automotriz = Galeria::where('id', 29)->first();


        return view('cliente.productos', compact(['parrafos', 'productos', 'deportivo', 'intima', 'tecnicos', 'automotriz']));
    }


    public function calidad () {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 9)->get();
        $parrafos = $parrafos->keyBy('ref');
        $quality = Quality::where('active', true)->first();

        $portafolio = Galeria::where('id', 32)->first();

        return view('cliente.calidad', compact(['parrafos', 'portafolio', 'quality']));
    }

    public function tecnologia () {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 10)->get();
        $parrafos = $parrafos->keyBy('ref');

        $portafolio = Galeria::where('id', 31)->first();


//        var_dump($parrafos);
//        echo $parrafos['urdido'];
        return view('cliente.tecnologia', compact(['parrafos', 'portafolio']));
    }


    public function diseno () {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 11)->get();
        $parrafos = $parrafos->keyBy('ref');


        $portafolio = Galeria::where('id', 27)->first();


        return view('cliente.diseno', compact(['parrafos', 'portafolio']));
    }


    public function proveedores() {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 12)->get();
        $parrafos = $parrafos->keyBy('ref');

        return view('cliente.proveedores', compact(['parrafos']));
    }


    public function sustentabilidad() {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 13)->get();
        $parrafos = $parrafos->keyBy('ref');

        return view('cliente.sustentabilidad', compact(['parrafos']));
    }

    public function avisoPrivacidad() {

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 7)->get();
        $parrafos = $parrafos->keyBy('ref');

        return view('cliente.aviso-de-privacidad', compact(['parrafos']));
    }


    public function laFirma () {
        $seccionQuienesSomos = Seccion::where('modelo', 'quienes-somos')->first();
        return view('cliente.la-firma', compact(['seccionQuienesSomos']));
    }


    public function servicios () {
        $seccionServicio = Seccion::where('modelo', 'servicios')->first();
        $servicios = Servicio::all();
        return view('cliente.servicios', compact(['seccionServicio', 'servicios']));
    }

    public function staff() {
        return view('cliente.staff');
    }

    public function sectores() {
        return view('cliente.sectores');
    }

    public function clientes() {
        return view('cliente.clientes');
    }

    public function areasDePractica () {
        //$seccionServicio = Seccion::where('modelo', 'servicios')->first();
        //$servicios = Servicio::all();
        //return view('cliente.servicios', compact(['seccionServicio', 'servicios']));
        return view('cliente.areas-de-practica');
    }
}
