<?php

namespace App\Http\Controllers\Cliente;

use App\Fotografia;
use App\Galeria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GaleriaController extends Controller
{
    public function index (){
        $fotografias = Fotografia::with('galerias')->get();

        return view('cliente.galerias.index', compact('fotografias'));
    }

    public function detalle($slug) {
        $galeria = Galeria::where('slug', $slug)->first();
        if (is_null($galeria)) {
            abort(404);
        }


        return view('cliente.galerias.detalle', compact('galeria'));
    }
}
