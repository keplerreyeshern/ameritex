<?php

namespace App\Http\Controllers\Cliente;

use App\CategoriaProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Producto;
use App\Galeria;


class ProductoController extends Controller
{
    public function index ($material) {

        //datos del material
        $producto = Producto::where('slug', 'like', '%' . $material . '%')->select('id','nombre','descripcion','fotolistado')->first();


        //$producto->fdo = trans('messages.lajas', [ 'name' => 'xyz' ], 'es');



    //dd($producto->fdo);


        $galeria = Galeria::where('galeriable_id', $producto->id)->first();

        //return view('cliente/portafolio', compact('portafolio'));


        //session()->forget('categoriaActual');
        //$categorias = CategoriaProducto::with('childrenRecursive')->whereNull('padre_id')->orderBy('nombre','asc')->get();
        //$productos = Producto::where('activo', '1')->get();




        return view('cliente.productos.index', compact(['producto', 'galeria', 'material']));
    }


    public function show ($slug) {
        session()->forget('categoriaActual');
        $categorias = CategoriaProducto::with('childrenRecursive')->whereNull('padre_id')->orderBy('nombre','asc')->get();
        $producto = Producto::where('slug', $slug)->with('galerias')->first();
        if(!$producto){
            abort(404);
        }
        return view('cliente.productos.show', compact(['producto', 'categorias']));
    }
    /* Muestra los productos por categoría*/
    public function categoria ($slug) {
        $categorias = CategoriaProducto::with('childrenRecursive')->whereNull('padre_id')->orderBy('nombre','asc')->get();
        $categoria = CategoriaProducto::with('productos')->where('slug', $slug)->first();
        session()->put('categoriaActual', $categoria);
        if(!$categoria){
            abort(404);
        }
        return view('cliente.productos.categoria', compact(['categoria', 'categorias']));
    }
}
