<?php

namespace App\Http\Controllers\Cliente;

use App\Promocion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PromocionController extends Controller
{
    public function show ($slug) {


        $promocion = Promocion::where('slug', 'like', '%' . $slug . '%')->first();



        if (!$promocion){
            abort(404);
        }





        return view('cliente.promociones.show', compact('promocion'));
    }
}
