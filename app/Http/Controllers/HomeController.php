<?php

namespace App\Http\Controllers;

use App\Parrafo;
use App\CategoriaComunicado;
use App\Fotografia;
use App\Galeria;
use App\Promocion;
use App\Comunicado;
use Illuminate\Http\Request;
use App\Seccion;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promociones = Promocion::where('active', '1')->inRandomOrder()->get();

        $parrafos = Parrafo::select('ref','titulo', 'contenido')->where('categoria_id', 2)->get();
        $parrafos = $parrafos->keyBy('ref');

        $portafolio = Galeria::where('id', 30)->first();

        //$categoriasComunicados = CategoriaComunicado::all();
        //$introServicios = Seccion::where('modelo', 'servicios')->first();
        //$categoriasComunicados = Comunicado::where('active',1)->where('categoria_id', 1)->orderBy('fecha','desc')->limit(3)->get();

        return view('home', compact([ 'promociones', 'parrafos', 'portafolio']));
    }
}
