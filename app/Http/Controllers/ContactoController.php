<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Mail\Contacto;
use Illuminate\Support\Facades\Mail;

class ContactoController extends Controller
{
    public function mailContacto (Request $request) {
        $this->validate($request, [
            'nombre' => 'required|min:3',
            'telefono' => 'required|min:10',
            'correo' => 'required|email',
            'mensaje' => 'required',
            'g-recaptcha-response' => 'required|captcha',
        ]);

        $mailto = env('MAIL_TO', 'gerardo.trujillo@gmail.com');


        Mail::to($mailto)->send(new Contacto($request->nombre, $request->telefono, $request->correo, $request->mensaje));



        session::flash('message', 'Su mensaje ha sido enviado');
        return redirect()->back();

    }
}
