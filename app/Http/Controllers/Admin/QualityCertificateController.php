<?php

namespace App\Http\Controllers\Admin;

use App\Quality;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class QualityCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qualities = Quality::all();

        return view('admin.modulos.qualities.index', compact('qualities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.modulos.qualities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $quality = new Quality($request->all());
        $quality->save();

        $mensaje = "El certificado se creó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.calidad.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quality = Quality::find($id);




        return view('admin.modulos.qualities.edit', compact('quality'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $quality = Quality::find($id);
        $quality->fill($request->all());
        $quality->slug = str_slug($quality->title);

        $quality->save();


        $mensaje = "El certificado fue actualizado exitósamente";
        $tipo = "success";

        return redirect()->route('admin.calidad.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Quality::destroy($id);
        $mensaje = "El certificado fue eliminado exitósamente";
        $tipo = "success";

        return redirect()->route('admin.calidad.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function Enable($id)
    {
        DB::table('qualities')
            ->where('id', $id)
            ->update(array('active' => 1));

        $mensaje = "El certificado se activó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.calidad.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }
    public function Disable($id)
    {
        DB::table('qualities')
            ->where('id', $id)
            ->update(array('active' => 0));

        $mensaje = "El certificado se desactivó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.calidad.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }
}
