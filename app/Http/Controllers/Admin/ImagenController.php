<?php

namespace App\Http\Controllers\Admin;

use App\Imagen;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Intervention\Image\ImageManager;


class ImagenController extends Controller
{
    protected $files;
    protected  $galeriaId;
    protected $fullDirectory;
    public function __construct($files, $galeriaId) {
        $this->files = $files;
        $this->galeriaId = $galeriaId;
        $this->fullDirectory =  "galerias/".$galeriaId."/";
    }
    public function saveGallery ()
    {
        $dir = $this->fullDirectory; // Directorio donde se va a guardar la imagen
        if (!File::exists($this->fullDirectory)) { //Creamos la carpeta
            File::makeDirectory($dir, $mode = 0777, true, true);
        }
        
        if (is_array($this->files)){ // Cuando se envian varias imágenes juntas
            foreach ($this->files as $file) {
                $this->storeImage($file);
            }
        }
        else { // Cuando se envia solo una imagen
            $this->storeImage($this->files);
        }

    }
    public function storeImage ($image) {
        var_dump($image->getClientOriginalName());

        $originalName = $image->getClientOriginalName();
        $allowed_filename = $originalName;
        $size = $image->getSize();
        $extension = $image->getClientOriginalExtension();
        $originalNameWithoutExt = substr($originalName, 0, strlen($originalName) - strlen($extension) - 1);

        $filename = $this->sanitize($originalNameWithoutExt);

        $allowed_filename = $this->createUniqueFilename($filename, $extension );


        $manager = new ImageManager();
        $manager->make( $image )->save($this->fullDirectory . $allowed_filename ); //Guardamos el archivo


        $sessionImage = new Imagen();
        $sessionImage->filename      = $allowed_filename;
        $sessionImage->original_name = $originalName;
        $sessionImage->galeria_id = $this->galeriaId;
        $sessionImage->save();

    }
    public function createUniqueFilename( $filename, $extension )
    {
        //$full_size_dir = $this->full_directory;
        $full_size_dir = $this->fullDirectory;
        $full_image_path = $full_size_dir . $filename . '.' . $extension;

        if ( File::exists( $full_image_path ) )
        {
            // Generate token for image
            $imageToken = substr(sha1(mt_rand()), 0, 5);
            return $filename . '-' . $imageToken . '.' . $extension;
        }

        return $filename . '.' . $extension;
    }
    function sanitize($string, $force_lowercase = true, $anal = false)
    {
        $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
            "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
            "â€”", "â€“", ",", "<", ".", ">", "/", "?");
        $clean = trim(str_replace($strip, "", strip_tags($string)));
        $clean = preg_replace('/\s+/', "-", $clean);
        $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean ;

        return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
                mb_strtolower($clean, 'UTF-8') :
                strtolower($clean) :
            $clean;
    }
    static function getImages($galeriaId)
    {

        // $url = asset("/galerias/".$galeriaId."/");
        $url = config('app.url')."/galerias/".$galeriaId;

        $images = Imagen::where('galeria_id',$galeriaId)->get();

        foreach ($images as $image) {
            $image['url'] = $url."/". $image->filename;
            $image['original_name'] = substr($image['original_name'], 0, strrpos($image['original_name'], "."));
            
        }
        return $images;
    }
}
