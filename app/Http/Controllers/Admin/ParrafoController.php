<?php

namespace App\Http\Controllers\Admin;

use App\Comunicado;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CategoriaParrafo;


use App\Parrafo;

use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;


class ParrafoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $parrafos = Parrafo::with('categoria')->orderBy('categoria_id','desc')->get();

        $categorias = CategoriaParrafo::select('id','nombre')->orderBy('nombre', 'asc')->get();



        return view('admin.modulos.parrafos.index', compact('parrafos','categorias'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $parrafo = new Parrafo();

        $parrafo->accionTit = 'Crear';
        $parrafo->method = "CREATE";
        $parrafo->link = "admin.parrafos.store";
        $parrafo->categoria = 'null';
        $parrafo->id = 0;

        $categorias = CategoriaParrafo::select('id','nombre')->orderBy('nombre', 'asc')->get();


        return view('admin.modulos.parrafos.forma', compact('parrafo','categorias'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parrafo = new Parrafo($request->except('_token'));
        $parrafo->save();

        $mensaje = "El parrafo se agregó exitósamente";
        $tipo = "success";

        return redirect('/admin/parrafos')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $parrafo = Parrafo::find($id);


        $parrafo->accionTit = "Editar";
        $parrafo->method = "PATCH";
        $parrafo->link = "/admin/parrafos/$parrafo->id";

        $categorias = CategoriaParrafo::select('id','nombre')->orderBy('nombre', 'asc')->get();



        return view('admin.modulos.parrafos.forma', compact('parrafo', 'categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $parrafo = Parrafo::find($id);
        $parrafo->fill($request->all());

        //dd($parrafo);

        $parrafo->save();

        $mensaje = "El comunicado se actualizó exitósamente";
        $tipo = "success";

        return redirect("/admin/parrafos/")->with(['mensaje' => $mensaje, 'tipo' => $tipo]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Parrafo::destroy($id);

        $mensaje = "El parrafo se eliminó exitósamente";
        $tipo = "success";

        return redirect("/admin/parrafos/")->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
}
