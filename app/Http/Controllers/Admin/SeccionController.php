<?php

namespace App\Http\Controllers\Admin;

use App\Seccion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SeccionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modulos.secciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $seccion = Seccion::where('modelo', $request->modelo)->first();
        if (!$seccion) {
            $seccion = new Seccion($request->all());
            $seccion->save();
        }
        else {
            $seccion->update($request->all());
        }

        $mensaje = "El contenido se guardó exitósamente";
        $tipo = "success";
        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function createByModel ($modelo) {
        $seccion = Seccion::where('modelo', $modelo)->first();
        if(!$seccion) {
            $seccion = new Seccion();
        }
        return view('admin.modulos.secciones.create', compact(['modelo', 'seccion']));
    }
}
