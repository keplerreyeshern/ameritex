<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Servicio;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ServicioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $servicios = Servicio::all();
        return view('admin.modulos.servicios.index', compact('servicios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modulos.servicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
            'imagen' => 'required'
        ]);
        $servicio = New Servicio($request->all());
        $servicio->save();

        $mensaje = "El servicio se agregó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.servicios.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $servicio = Servicio::find($id);
        return view('admin.modulos.servicios.edit', compact('servicio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
        ]);

        $servicio = Servicio::find($id);
        $servicio->fill($request->all());
        $servicio->save();

        $mensaje = "El servicio se actualizó exitósamente";
        $tipo = "success";


        return redirect()->route('admin.servicios.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $servicio = Servicio::find($id);
        $imagen = $servicio->imagen;

        DB::beginTransaction();
        try {
            Servicio::destroy($id);
            Storage::disk('servicios')->delete($imagen);
            $mensaje = "El servicio se eliminó exitósamente";
            $tipo = "success";
        } catch (\Exception $exception) {
            DB::rollBack();
            $mensaje = "Tuuvimos un problema, por favor inténtalo de nuevo";
            $tipo = "danger";
        }
        DB::commit();
        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function enable($id)
    {
        DB::table('servicios')->where('id', $id)->update(['active' => 1]);

        $mensaje = "El servicio se habilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    public function disable($id)
    {
        DB::table('servicios')->where('id', $id)->update(['active' => 0]);

        $mensaje = "El servicio se deshabilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
}
