<?php

namespace App\Http\Controllers\Admin;

use App\Galeria;
use App\Imagen;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class GaleriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        Galeria::find($id)->update($request->all());
        $galeria = Galeria::find($id);
        $galeria->fecha = Carbon::createFromFormat('d/m/Y', $request->fecha)->timestamp;
        $galeria->nombre = $request->nombre;
        $galeria->descripcion = $request->descripcion;

        $galeria->slug = str_slug($galeria->nombre);


        $galeria->save();



        Session::flash('mensaje', 'La Galeria fue actualizada exitosamente');
        Session::flash('tipo', 'success');

        return redirect()->route('admin.galerias.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function removeImageFromGallery($id) {
        Imagen::destroy($id);
    }
    public function addImageGallery(Request $request, $id) {
        $file = $request->file('file');
        $saveGallery = new ImagenController($file, $id);
        return $saveGallery->saveGallery();
//        return $request->all();
    }
}
