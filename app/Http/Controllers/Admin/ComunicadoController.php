<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CategoriaComunicado;
use App\Comunicado;
use Carbon\Carbon;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

class ComunicadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoria)
    {
        $categoria = CategoriaComunicado::where('nombre', $categoria)->first();
        if (!$categoria){
            abort(404);
        }
        $comunicado = new Comunicado();
        $comunicado->fecha = Carbon::now()->timestamp;
        $comunicado->categoria_id = $categoria->id;
        return view('admin.modulos.comunicados.categoria.create')->with(['categoria' => $categoria->nombre, 'comunicado' => $comunicado]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'titulo' => 'required',
            'descripcion' => 'required',
            'fecha' => 'required',
        ]);

        $comunicado = new Comunicado($request->except('_token'));
        $comunicado->fecha = Carbon::createFromFormat('d/m/Y', $request->fecha)->timestamp;
        $comunicado->destacado = (Input::has('destacado')) ? true : false;
        $comunicado->save();

        $categoria = CategoriaComunicado::where('id', $comunicado->categoria_id)->first();

        $mensaje = "El comunicado se agregó exitósamente";
        $tipo = "success";

        return redirect("/admin/comunicados/$categoria->nombre")->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comunicado = Comunicado::find($id);


        return view('admin.modulos.comunicados.edit')->with('comunicado', $comunicado);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comunicado = Comunicado::find($id);
        $comunicado->fill($request->all());
        $comunicado->destacado = (Input::has('destacado')) ? true : false;
        $comunicado->fecha = Carbon::createFromFormat('d/m/Y', $comunicado->fecha)->timestamp;

        $comunicado->slug = str_slug($comunicado->titulo);

        $comunicado->save();

        $categoria = CategoriaComunicado::where('id', $comunicado->categoria_id)->first();

        $mensaje = "El comunicado se actualizó exitósamente";
        $tipo = "success";

        return redirect("/admin/comunicados/$categoria->nombre")->with(['mensaje' => $mensaje, 'tipo' => $tipo]);


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Comunicado::destroy($id);

        $mensaje = "El comunicado se eliminó exitósamente";
        $tipo = "success";
        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }



    public  function indexPorCategoria (Request $request, $categoria) {


        $request->all();


        $categoria = CategoriaComunicado::where('nombre', $categoria)->first();
        if (!$categoria){
            abort(404);
        }

       //$categoria_comunicados = CategoriaComunicado::find($categoria->id)->with('comunicados');

        $categoria_comunicados = CategoriaComunicado::find($categoria->id);

        if($request['srchTxt'] == null) {
            $comunicados = $categoria_comunicados->comunicados()->paginate(6);
        }
        else
        {
            $comunicados = $categoria_comunicados->comunicados()->where('titulo', 'like', '%' . $request['srchTxt'] . '%')->paginate(6);
        }



        return view('admin.modulos.comunicados.categoria.index', compact(['categoria_comunicados', 'comunicados']));
    }


    public function enable($id)
    {
        DB::table('comunicados')->where('id', $id)->update(['active' => 1]);

        $mensaje = "El comunicado se habilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    public function disable($id)
    {
        DB::table('comunicados')->where('id', $id)->update(['active' => 0]);

        $mensaje = "El comunicado se deshabilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
}
