<?php

namespace App\Http\Controllers\Admin;

use App\CategoriaProducto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriaProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.modulos.productos.categorias.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $categoria = New CategoriaProducto();
        $categoria->nombre = $request->categoria;
        $categoria->padre_id = $request->parent_id;
        $categoria->save();

        return $categoria;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categoria = CategoriaProducto::find($id);
        $categoria->nombre = $request->nombre;
        $categoria->save();
        return $categoria;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CategoriaProducto::destroy($id);
    }

    public function categories () {
        $categorias = CategoriaProducto::with('childrenRecursive')->whereNull('padre_id')->orderBy('nombre','asc')->get();
        return $categorias;

    }
}
