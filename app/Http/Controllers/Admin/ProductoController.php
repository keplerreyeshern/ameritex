<?php

namespace App\Http\Controllers\Admin;

use App\Producto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
Use App\CategoriaProducto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $categoria = $request['categoria'];
        $cat = $categoria;




        $categorias = DB::table('categorias_productos')->where('padre_id', null)->get();


        $srchTxt = $request['srchTxt'];
   // Llámamos la funcion que hace los filtros
        $productos = $this->filter($categoria, $srchTxt);



        return view('admin.modulos.productos.index', compact('productos','categorias', 'cat') );
    }



    public function filter ($category, $search) {

        //$q = [];

        //$q[] = ['nombre', 'like', 'algo'];
        //$q[] = ['nombre1', 'like1', 'algo1'];

        //dd($q);



        $category = CategoriaProducto::find($category);

        $paginacion = 6;

        // Con dos variables de entrada (categoría y búsqueda) se obtienen 4 posibles combinaciones

        if($category && $search) {
            $products = $category->productos()->where('nombre', 'like', '%' . $search . '%')->paginate($paginacion);
        } else if($category && !$search) {
            $products = $category->productos()->paginate($paginacion);
        } else if(!$category && $search) {
            $products = Producto::where('nombre', 'like', '%' . $search . '%')->paginate($paginacion);
        } else {
            $products = Producto::orderBy('Nombre', 'asc')->paginate($paginacion);
        }

        return $products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modulos.productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->nombre) {
            $producto = new Producto($request->all());
            $producto->save();
            $producto->categorias()->sync($request->categories);
            Session::flash('mensaje', 'Se creó exitosamente el producto');
            Session::flash('tipo', 'success');
            return redirect()->route('admin.productos.index');
        }
        $producto = new Producto();
        $producto->nombre = $request['nameProduct'];
        $producto->precio = (float)$request['pricingProduct'];
        $producto->descripcion = $request['descriptionProduct'];
        $producto->detalles = $request['detailsProduct'];
        $producto->fotolistado = $request['fotolistado'];

        $files = $request->file('fotolistado');

       


        DB::beginTransaction();
        try {
            $producto->save();
            $galeria = $producto->galerias()->create([]);
            $saveGallery = new ImagenController($files, $galeria->id);
            $saveGallery->saveGallery();

            if($request->categories){
                $categories = array_map('intval', explode(',', $request->categories));
                $producto->categorias()->attach($categories);
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
        DB::commit();
        Session::flash('mensaje', 'Se creó exitosamente el producto');
        Session::flash('tipo', 'success');

        return;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::where('id', $id)->with('galerias', 'categorias')->first();

        return view('admin.modulos.productos.edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function oldUpdate(Request $request, $id)
    {

        $product = Producto::find($id);

        $product->fill($request->all());
        $product->slug = str_slug($product->nombre);

        $product->save();
        //Producto::find($id)->update($product);

        $product = Producto::find($id);
        if($request->categories){
            $product->categorias()->detach();
            $product->categorias()->attach($request->categories);
        }



        $mensaje = "El producto se actualizó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.productos.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    public function update(Request $request, $id) {
        $producto = Producto::find($id);
        if($request->nombre) {
            $producto->update($request->all());
            $producto->categorias()->sync($request->categories);
            Session::flash('mensaje', 'Se actualizó exitosamente el producto');
            Session::flash('tipo', 'success');
            return redirect()->back();
        }
        $producto->nombre = $request['nameProduct'];
        $producto->precio = (float)$request['pricingProduct'];
        $producto->descripcion = $request['descriptionProduct'];
        $producto->detalles = $request['detailsProduct'];
        $producto->fotolistado = $request['fotolistado'];

        $files = $request->file('file');
        DB::beginTransaction();
        try {
            $producto->save();
            if($files) {
                $galeria = $producto->galerias()->create([]);
                $saveGallery = new ImagenController($files, $galeria->id);
                $saveGallery->saveGallery();
            }
            if($request->categories){
                $categories = array_map('intval', explode(',', $request->categories));
                $producto->categorias()->attach($categories);
            }

        } catch (\Exception $exception) {
            DB::rollBack();
            return $exception;
        }
        DB::commit();
        Session::flash('mensaje', 'Se actualizó exitosamente el producto');
        Session::flash('tipo', 'success');

        return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $producto = Producto::find($id);
        $producto->categorias()->detach();
        Producto::destroy($id);

        $mensaje = "El producto se eliminó exitósamente";
        $tipo = "success";
        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function enable($id){
        DB::table('productos')->where('id', $id)->update(['activo' => 1]);

        $mensaje = "El producto se habilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function disable($id){
        DB::table('productos')->where('id', $id)->update(['activo' => 0]);

        $mensaje = "El producto se deshabilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
}
