<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('nombre', 'asc')->get();
        return view('admin.modulos.usuarios.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $roles = Role::all();
        return view('admin.modulos.usuarios.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {




        $request->validate([
            'nombre' => 'required',
            'apellido' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
        ]);

        $request['password'] = hash::make($request['password']);



        DB::beginTransaction();
            $user = new User($request->all());
            $user->save();
        try{
            $user->assignRole($request->rol);
        } catch (\Exception $exception) {
            DB::rollBack();
        }
        DB::commit();

        $mensaje = "Se ha agregado un nuevo Usuario";
        $tipo = "success";

        return redirect()->route('admin.users.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::all();
        return view('admin.modulos.usuarios.edit')->with(['user' => $user, 'roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $user = User::find($id);
        DB::beginTransaction();
            try {
                // $user->update($request->all());
                $user->update($request->except('password'));
                if($request->password) {
                    $user->password = bcrypt($request->password);
                    $user->save();
                }
                $user->syncRoles($request->rol);
            } catch (\Exception $exception) {

                DB::rollBack();

                $mensaje = "Tuvimos un problema, por favor inténtalo de nuevo.";
                $tipo = "Danger";
                return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
            }
        DB::commit();
        $mensaje = "El usuario fue actualizado";
        $tipo = "success";

        return redirect()->route('admin.users.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::destroy($id);

        $mensaje = "El usuario fue eliminado";
        $tipo = "success";

        return redirect()->route('admin.users.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function enable ($id) {
        $user = User::find($id);
        $user->active = 1;
        $user->save();

        $mensaje = "El usuario se habilitó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.users.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function disable($id) {
        $user = User::find($id);
        $user->active = 0;
        $user->save();

        $mensaje = "El usuario se deshabilitó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.users.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
}
