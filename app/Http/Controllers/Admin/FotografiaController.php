<?php

namespace App\Http\Controllers\Admin;

use App\Fotografia;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Galeria;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class FotografiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $fotografias = Fotografia::all();
        $fotografias = Fotografia::with('galerias')->get();


        return view('admin.modulos.galerias.index', compact('fotografias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.modulos.galerias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $files = $request->file('file');

        $fotografia = new Fotografia(); // el modelo al que se va a asignar la galería


        DB::beginTransaction();
            try {
                $fotografia->save();
                $galeria = $fotografia->galerias()->create(
                    [
                        'nombre' => $request->nameGallery,
                        'descripcion' => $request->descriptionGallery,
                        'fecha' => Carbon::createFromFormat('d/m/Y', $request->dateGallery)->timestamp
                    ]
                );
                $saveGallery = new ImagenController($files, $galeria->id);
                $saveGallery->saveGallery();
            } catch (\Exception $exception) {
                abort(500);
                DB::rollBack();
            }
        DB::commit();

        Session::flash('mensaje', 'Se creó exitosamente la galería');
        Session::flash('tipo', 'success');


        return;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $galeria = Galeria::findOrFail($id);
        return view('admin.modulos.galerias.edit', compact('galeria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Galeria::destroy($id);

        $mensaje = "La galería se eliminó exitósamente";
        $tipo = "success";
        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function enable($id)
    {
        DB::table('galerias')->where('id', $id)->update(['active' => 1]);

        $mensaje = "La galería se habilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    public function disable($id)
    {
        DB::table('galerias')->where('id', $id)->update(['active' => 0]);

        $mensaje = "La galería se deshabilitó exitósamente";
        $tipo = "success";

        return redirect()->back()->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
}
