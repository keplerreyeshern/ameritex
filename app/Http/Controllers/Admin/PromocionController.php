<?php

namespace App\Http\Controllers\Admin;

use App\Promocion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class PromocionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promociones = Promocion::all();

        return view('admin.modulos.promociones.index', compact('promociones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('admin.modulos.promociones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $promocion = new Promocion($request->all());
        $promocion->save();

        $mensaje = "La promoción se creó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.promociones.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promocion = Promocion::find($id);




        return view('admin.modulos.promociones.edit', compact('promocion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $promocion = Promocion::find($id);
        $promocion->fill($request->all());
        $promocion->slug = str_slug($promocion->nombre);

        $promocion->save();


        $mensaje = "La promoción fue actualizada exitósamente";
        $tipo = "success";

        return redirect()->route('admin.promociones.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Promocion::destroy($id);
        $mensaje = "La promoción fue eliminada exitósamente";
        $tipo = "success";

        return redirect()->route('admin.promociones.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);
    }
    public function Enable($id)
    {
        DB::table('promociones')
            ->where('id', $id)
            ->update(array('active' => 1));

        $mensaje = "La promoción se activó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.promociones.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }
    public function Disable($id)
    {
        DB::table('promociones')
            ->where('id', $id)
            ->update(array('active' => 0));

        $mensaje = "La promoción se desactivó exitósamente";
        $tipo = "success";

        return redirect()->route('admin.promociones.index')->with(['mensaje' => $mensaje, 'tipo' => $tipo]);

    }
}
