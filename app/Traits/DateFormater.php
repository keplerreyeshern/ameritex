<?php
/**
 * Created by PhpStorm.
 * User: erik_
 * Date: 06/06/2018
 * Time: 10:18 AM
 */

namespace App\Traits;

use Carbon\Carbon;


trait DateFormater
{
    public function getFechaformateadaAttribute () {
        $fecha = new Carbon();

        $fecha->timestamp = $this->fecha;
        $fecha->timezone = 'America/Mexico_City';

        $month = $this->returnMonthSpanish(ucfirst($fecha->formatLocalized('%B')));
        $format = $fecha->formatLocalized('%d/').$month.$fecha->formatLocalized('/%Y');
        return $format;
    }
    public function returnMonthSpanish($month) {
        switch ($month) {
            case "january":
                return "Ene";
                break;
            case "February":
                return "Feb";
                break;
            case "March":
                return "Mar";
                break;
            case "April":
                return "Abr";
                break;
            case "May":
                return "May";
                break;
            case "June":
                return "Jun";
                break;
            case "July":
                return "Jul";
                break;
            case "August":
                return "Ago";
                break;
            case "September":
                return "Sep";
                break;
            case "October":
                return "Oct";
                break;
            case "November":
                return "Nov";
                break;
            case "December":
                return "Dic";
                break;
            default:
                return $month;
        }
    }
}