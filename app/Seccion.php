<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Spatie\Translatable\HasTranslations;

class Seccion extends Model
{

    use HasTranslations;

    public $translatable = ['contenido'];

    protected $table = "secciones";
    protected $fillable = ['modelo', 'contenido'];

    public function getContenidoAttribute($value)
    {
        $json = json_decode($value, true)[app()->getLocale()];
        if($json) {
            return $json;
        } else {
            return $value;
        }
    }

}
