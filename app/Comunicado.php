<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Traits\DateFormater;
use Spatie\Translatable\HasTranslations;

class Comunicado extends Model
{
    use Sluggable, DateFormater, HasTranslations;

    public $translatable = ['slug', 'titulo', 'subtitulo', 'intro', 'descripcion'];

    protected $fillable = ['slug'
        ,'titulo'
        ,'subtitulo'
        ,'intro'
        ,'descripcion'
        ,'autor'
        ,'fecha'
        ,'fotocomunicado'
        ,'fotolistado'
        ,'pdf'
        ,'categoria_id'
        ,'destacado'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'titulo'
            ]
        ];
    }

    public function categoria () {
        return $this->belongsTo('App\CategoriaComunicado');
    }

    public function getFechaformateadaeditAttribute()
    {
        $fecha = date('d/m/Y', $this->fecha);
        return $fecha;
    }

    public function setThumbAttribute($thumb)
    {
        $name = Carbon::now()->month
            ."-".Carbon::now()->day
            ."-".Carbon::now()->hour
            ."-".Carbon::now()->minute
            ."-".Carbon::now()->second
            ."-".$thumb->getClientOriginalName();
        $this->attributes['thumb'] = $name;
        \Storage::disk('comunicados')->put($name,\File::get($thumb));

    }

    /**
     * @param $fotolistado
     */
    public function setFotolistadoAttribute($fotolistado)
    {
        $name = Carbon::now()->month
            ."-".Carbon::now()->day
            ."-".Carbon::now()->hour
            ."-".Carbon::now()->minute
            ."-".Carbon::now()->second
            ."-".$fotolistado->getClientOriginalName();
        $this->attributes['fotolistado'] = $name;
        \Storage::disk('comunicados')->put($name,\File::get($fotolistado));

    }

    public function setFotocomunicadoAttribute($fotocomunicado)
    {
        $name = Carbon::now()->month
            ."-".Carbon::now()->day
            ."-".Carbon::now()->hour
            ."-".Carbon::now()->minute
            ."-".Carbon::now()->second
            ."-".$fotocomunicado->getClientOriginalName();
        $this->attributes['fotocomunicado'] = $name;
        \Storage::disk('comunicados')->put($name,\File::get($fotocomunicado));

    }
    public function getUrlfotolistadoAttribute(){
        if($this->fotolistado != '') {
            return "/files/comunicados/" . $this->fotolistado;
        }
        return null;
    }

    public function getUrlfotocomunicadoAttribute(){
        if($this->fotocomunicado != ''){
            return "/files/comunicados/".$this->fotocomunicado;
        }
        return null;
    }

    public function getLinkdetalleAttribute(){
        $cat = CategoriaComunicado::find($this->categoria_id);
        $url = '/comunicados/'.$cat->nombre.'/'.$this->slug;
        return $url;
    }
}
