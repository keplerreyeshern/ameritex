<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class CategoriaComunicado extends Model
{
    public function comunicados () {
        return $this->hasMany('App\Comunicado', 'categoria_id')->orderBy('slug', 'asc');
    }
}
