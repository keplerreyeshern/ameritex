<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Admin\ImagenController;

use Spatie\Translatable\HasTranslations;

class Galeria extends Model
{
    use Sluggable, HasTranslations;

    public $translatable = ['slug','nombre', 'descripcion'];

    protected $appends =['imagenes_de_galeria'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    protected  $fillable = ['nombre', 'descripcion', 'fecha'];

    public function galeriable () {
        return $this->morphTo();
    }

    private function getImagenes(){
        return ImagenController::getImages($this->id);
    }

    public function getImagenesDeGaleriaAttribute(){
        return $this->getImagenes();
    }

    public function getFotoListadoAttribute(){
        $imgs = $this->getImagenes();
        if(count($imgs)){
            return $imgs[0]['url'];
        }else{
            return null;
        }
    }
    public function getFechaFormateadaEditAttribute()
    {
        $fecha = date('d/m/Y', $this->fecha);
        return $fecha;
    }
}
