<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class CategoriaProducto extends Model
{
    use Sluggable;

    protected $table = "categorias_productos";
    protected $fillable = ['nombre', 'slug'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function productos () {
        return $this->belongsToMany(Producto::class, 'cat_producto');
    }


    public function children()
    {
        return $this->hasMany('App\CategoriaProducto', 'padre_id');
    }

// recursive, loads all descendants
    public function childrenRecursive()
    {
        return $this->children()->with('childrenRecursive');
        // which is equivalent to:
        // return $this->hasMany('App\CategoriaProducto', 'parent')->with('childrenRecursive);
    }

// parent
    public function parent()
    {
        return $this->belongsTo('App\CategoriaProducto','padre_id');
    }

// all ascendants
    public function parentRecursive()
    {
        return $this->parent()->with('parentRecursive');
    }
}
