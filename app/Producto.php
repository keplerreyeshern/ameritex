<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Spatie\Translatable\HasTranslations;


class Producto extends Model
{
    use Sluggable, HasTranslations;

    public $translatable = ['slug', 'nombre', 'descripcion', 'detalles', 'descripcion'];

    protected $table = "productos";
    protected $fillable = ['nombre', 'precio', 'descripcion', 'detalles', 'slug', 'fotolistado'];

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'nombre'
            ]
        ];
    }

    public function galerias () {
        return $this->morphMany(Galeria::class,'galeriable');
    }

    public function categorias () {
        return $this->belongsToMany(CategoriaProducto::class, 'cat_producto');
    }


    /**
     * @param $fotolistado
     */
    public function setFotolistadoAttribute($fotolistado)
    {
        if(!$fotolistado){
            return null;
        }
        $name = Carbon::now()->month
            ."-".Carbon::now()->day
            ."-".Carbon::now()->hour
            ."-".Carbon::now()->minute
            ."-".Carbon::now()->second
            ."-".$fotolistado->getClientOriginalName();
        $this->attributes['fotolistado'] = $name;
        \Storage::disk('productos')->put($name,\File::get($fotolistado));

    }
    public function getUrlFotolistadoAttribute () {
        if($this->fotolistado) {
            return config('app.url')."/files/productos/" . $this->fotolistado;
        }
        return "/base/img/imagen-no-disponible.jpg";
    }
    public function getLinkdetalleAttribute(){
        $producto = Producto::find($this->id);
        $url = '/productos/'.$this->slug;
        return $url;
    }

    public function getNombreAttribute($value)
    {
        try {
            $json = json_decode($value, true)[app()->getLocale()];
            if($json) {
                return $json;
            } else {
                return $value;
            }
        } catch(\Exception $exception) {
            return '';
        }
    }

    public function getDescripcionAttribute($value)
    {

        // Buscamos el idioma en el string
        // $hasLang = strpos($value, '"'.app()->getLocale().'":');
        // if($hasLang) {
        //     $json = json_decode($value, true)[app()->getLocale()];
        //     return $json;
        // }
        // return '';
        try {
            $json = json_decode($value, true)[app()->getLocale()];
            if($json) {
                return $json;
            } else {
                return $value;
            }
        } catch(\Exception $exception) {
            return '';
        }
    }

    public function getDetallesAttribute($value)
    {
        // $hasLang = strpos($value, '"'.app()->getLocale().'":');
        // if($hasLang) {
        //     $json = json_decode($value, true)[app()->getLocale()];
        //     return $json;
        // }
        // return '';
        try {
            $json = json_decode($value, true)[app()->getLocale()];
            if($json) {
                return $json;
            } else {
                return $value;
            }
        } catch(\Exception $exception) {
            return '';
        }
    }
}
