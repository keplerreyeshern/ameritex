<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\CategoriaComunicado;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

class ComunicadosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function () {

            if (Auth::user()){
                $categoriasComunicados = CategoriaComunicado::all();
                View::share('categoriasComunicados', $categoriasComunicados);
            }
        });
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
