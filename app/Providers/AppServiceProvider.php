<?php

namespace App\Providers;

use App\Promocion;
use App\Quality;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use  Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {   
        Schema::defaultStringLength(191);

        /*
         * Sliders
         * */
        Blade::component('componentes.slider.basico', 'slider_basico');
        Blade::component('componentes.slider.multiple', 'slider_multiple');
        Blade::component('componentes.slider.multiple.slide', 'slider_multiple_slide');

        /*
         * Jumbotron
         * */
        Blade::component('componentes.jumbotron.basico', 'jumbotron_basico');
        Blade::component('componentes.jumbotron.titulo', 'jumbotron_titulo');

        /*
         *  Breadcrumb
         * */
        Blade::component('componentes.breadcrumb.basico', 'breadcrumb_basico');

        /*
         * Comunicados
         * */
        Blade::component('componentes.comunicado.basico', 'comunicado_basico');

        /*
         * Filas
         * */
        Blade::component('componentes.fila.basico', 'fila_basico');

        /*
         * Parallax
         * */
        Blade::component('componentes.parallax.basico', 'parallax_basico');
        Blade::component('componentes.parallax.big', 'parallax_big');


        /**
         * Tarjeta
         */
        Blade::component('componentes.general.tarjeta.basico', 'tarjeta_basico');


        view()->composer('cliente/parciales/home/iso9001', function($view) {
            $quality = Quality::where('active', true)->first();
            $view->with('quality', $quality);
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
