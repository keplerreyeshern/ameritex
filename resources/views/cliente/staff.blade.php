
@extends('cliente.layout.cliente')
@section('titulo', 'Staff')
@section('contenido')

    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Servicios legales<br>
                por mas de 20 años
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Nuestro Staff</a></li>
    @endslot
    @endbreadcrumb_basico

    <div class="container -staff">
        <div class="columns is-multiline ">


            <div class="column is-one-quarter has-text-centered">
                <h1 class="title">Nuestro Staff</h1>
            </div>
            <div class="column is-three-quarters box">

                    <ul class="-lista-margen">
                        <li><p>Nuestros profesionistas han prestado servicios legales por más de 20 años en las firmas de abogados más prestigiadas del país (vgr. Chevez, Ruiz, Zamarripa y Cía, S.C., Hogan Lovells BSTL, S.C, Sánchez DeVanny Eseverri, S.C., Basham, Ringe y Correa, S.C. y Baker & Mckenzie, S.C., etc.)
                        <li><p>Son especialistas en cada una de sus materias y
                    son bilingües.
                    <li><p>Tienen como política la actualización constante.
                    <li><p>Son catedráticos de Universidades de prestigio en el país.
                    <li><p>Participan activamente en diversos gremios y colegios de especialistas en su materia, tales como la Barra Mexicana, Colegio de Abogados, A.C., el Colegio de Contadores Públicos de México, A.C., la Asociación Nacional de Abogados de Empresa, A.C., la American Bar Association
                    (ABA), entre otras.
                    </ul>
            </div>

            <div class="column is-one-quarter d">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">Miguel Ángel Martinez Ochoa</h1>
                <p>Licenciado en Derecho por la Universidad Iberoamericana, A.C., en la cual se graduó con honores. En 2003 obtuvo su grado de Magister en Comercio Exterior por la Universidad Carlos III de Madrid y ha realizado diversos cursos de postgrado en diversas Universidades de prestigio tanto nacionales como
                    extranjeras.
                <p>Sus áreas de práctica, consisten en la consultoría y litigio en materia aduanera y de comercio exterior. Adicionalmente, es consultor y litigante en aspectos
                    de derecho Administrativo y regulatorio.
                <p>Ha escrito diversos artículos en temas de comercio exterior y fiscal en la revista “Defensa Fiscal”, así como en “Hélice” de la Asociación Sindical de Pilotos Aviadores de México (ASPA), así como coautor en “Puntos Finos” de Dofiscal Editores. Además ha impartido múltiples conferencias en materia de comercio exterior en la Barra Mexicana, Colegio de Abogados, la Cámara Nacional de la Industria Maquiladora de Exportación (CNIME), la Cámara Nacional de la Industria de Transformación (CANACINTRA), American Cham ber of Commerce (AMCHAM), entre otras.
                <p>Actualmente es catedrático en la Universidad Iberoamericana, A.C. y ha sido catedrático en otras Universidades de prestigio nacional.
                <p>Es miembro de la Barra Mexicana, Colegio de Abogados, la Asociación Nacional de Abogados de Empresa (ANADE), Instituto Mexicano de Especialistas en Comercio Exterior (IMECE), miembro de la Comisión de Comercio Exterior del Capítulo Mexicano de la Cámara de Comercio Internacional
                    (CAMECIC) y del Comité de Comercio Exterior de AMCHAM.
            </div>

            <div class="column is-one-quarter has-text-centered">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">José Luis Sánchez García</h1>
                <p>Licenciado en Derecho por la Universidad Tecnológica de México (UNITEC), de la cual se graduó en 2002. Cuenta con una especialidad
                    en derecho fiscal en la misma Universidad. <p>Sus áreas de práctica consisten en la consultoría y litigio en materia de Seguridad Social (IMSS e INFONAVIT), Impuestos Locales, y Procedimiento Sancionador iniciado por la Secretaría del Trabajo y Previsión Social (STPS) a nivel Federal, y a nivel local por la Secretarías Estatales
                    respectivas, mismas áreas que ha desarrollado por más de 10 años.
                <p>Ha escrito diversos artículos en temas relacionados con la Seguridad Social y procedimiento sancionador llevado por la STPS en revistas como Puntos Finos, Veritas, Practica Fiscal y Abogado Corporativo. Además de que ha impartido múltiples conferencias y cursos en las mismas materias en el Colegio de Contadores Públicos de México, en Colegios Afiliados al Instituto de Contadores Públicos en la República Mexicana
                    y en otras asociaciones profesionales y comerciales.

                    <p>Ha participado como expositor en diversos cursos y seminarios de la prestigiada organización Dofiscal Thomson Reuters.

                    <p>Es miembro activo del Colegio de Contadores Públicos de México, en dónde desde hace más de siete años ha sido integrante de la Comisión Representativa ante Organismos de Seguridad Social, colaborando en la publicación de libros como “Outsourcing” y “Prestación de Servicios de personal y sus Implicaciones en materia de Seguridad Social”, publicados por el Instituto Mexicano de Contadores Públicos.
            </div>

            <div class="column is-one-quarter has-text-centered">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">Fabián Toriz Díaz</h1>
                <p>Abogado por la Escuela Libre de Derecho. Su tesis intitulada “El gravamen sobre Marcas (Prenda y Fideicomiso de Garantía)” fue premiada en 1999 como la mejor Tesis en materia de Propiedad Industrial, premio que otorgó la Asociación Mexicana para la Protección de la Propiedad Industrial, A.C. Ha realizado estudios de postgrado en “Derecho de la Propiedad Intelectual e Industrial” en la Universidad Panamericana y la “Academy of American and International Law” en la Texas, además de que realizó la especialidad en Privacidad sobre Tecnologías de la Información ante la International
                    Association of Privacy Professionals.

                <p>Sus áreas de práctica consisten en la consultoría y litigio en materia de marcas, patentes, derechos de autor, nombres de dominio, piratería local y en frontera, trámites corporativos, administrativos ante diversas dependencias de Gobierno, así como de privacidad de datos, mismas áreas que ha
                    desarrollado por más de 20 años.

                <p>Se desempeñó como asociado del área de Propiedad Intelectual de Basham, Ringe y Correa, S.C. por más de 10 años. Ha escrito diversos artículos en temas de marcas y derechos de autor en la revista MarcaSur (ASIPPI). Además ha participado como expositor en diversos cursos y seminarios en Universidades de prestigio, así como en la Asociación Mexicana de Propie-
                    dad Intelectual (AMPPI), entre otras.

                <p>Es miembro activo de la Asociación Mexicana para la Protección de la Propiedad Intelectual (AMPPI), de la Asociación Interamericana de la Propiedad Intelectual (ASIPI), de la Asociación Nacional de Abogados de Empresa
                    (ANADE) y de la International Trademark Association (INTA), entre otras.

            </div>

            <div class="column is-one-quarter has-text-centered">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">Jorge Navarro Isla</h1>
                <p>Licenciado en Derecho por el Instituto Tecnológico Autónomo de México, A.C. (ITAM). En 2000 obtuvo su LLM en Tecnologías de la Información y de las Comunicaciones (TIC’s) en Queen Mary and Westfield College de la Universidad de Londres, Reino Unido y ha cursado diplomados en Telecomunicaciones y Seguridad de la Información en el ITAM.

                    <p>Sus áreas de práctica, consisten en la consultoría en seguridad de la información, protección de datos, telecomunicaciones y comercio electrónico. Se ha desarrollado como asociado en diversas firmas de prestigio nacional como SMPS Legal y González Calvillo, S.C.

                    <p>Asimismo, se desempeña como asesor de la Conferencia de las Naciones Unidas sobre Comercio y Desarrollo (UNCTAD) para América Latina en temas regulatorios vinculados con el comercio electrónico (2007 – a la fecha); participó en la Delegación Mexicana del Subgrupo de Privacidad del Foro de Cooperación Económica Asia Pacífico (APEC) (2008); Ha sido Consultor de la Asociación Mexicana de Internet – (AMIPCI), de la Procuraduría Federal del Consumidor (2007) y de diversas empresas líderes del sector de las TIC, las telecomunicaciones y del sector financiero. Participó en el Comité de Expertos de la Comisión de Comercio de la Cámara de Diputados de la LVIII Legislatura para las reformas al Código de Comercio en materia de Firma Electrónica (2003); Ex-Director de Proyectos Especiales de la Comisión Federal de Telecomunicaciones.

                    <p>Es autor de los Estudios sobre la Armonización de la Ciberlegislación en América Latina de UNCTAD (2009, 2011 y 2015); Coordinador y Co-autor del Libro “Tecnologías de la Información y de las Comunicaciones: Aspectos Legales”, ITAM Porrúa (2005).

                    <p>Es instructor de diversos cursos de aspectos legales del comercio electrónico de UNCTAD, de la Escuela del Sur de Gobernanza de Internet y ha sido catedrático de diversos posgrados en el ITAM, el Instituto Tecnológico de Estudios Superiores de Monterrey y el Centro de Estudios Superiores Navales.

            </div>

            <div class="column is-one-quarter has-text-centered">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">Fernando Ortiz Calvo</h1>
                <p>Licenciado en Derecho por la Universidad Iberoamericana.

                    <p>Sus áreas de práctica consisten en la consultoría, litigio, planeación y estrategia en materia Laboral nacional e internacional,
                    misma área que ha desarrollado por más de 10 años.

                    <p>Se ha desempeñado como Asociado en las firmas Basham, Ringe y Correa, S.C., Baker & Mckenzie, S.C. y Chevez, Ruiz, Zamarripa y Cía, S.C., específicamente en las áreas Laboral y de Seguri-
                    dad Social.

                    <p>Cuenta con experiencia en el ámbito jurisdiccional, al haber formado parte del Sexto Tribunal Colegiado en Materia del Trabajo
                    del Primer Circuito.

                    <p>Es miembro activo de la Barra Mexicana, Colegio de Abogados, A.C. y ha sido reconocido por la publicación “Legal 500” como
                    “Associate to note” en materia laboral y de seguridad social.

            </div>


            <div class="column is-one-quarter has-text-centered">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">Nicolás Guerrero Cuate</h1>
                <br>Oficina Qro.
                <p>Licenciado en Derecho graduado con mención honorífica por la Universidad Tecnológica de México.

                    <p>Durante el desarrollo de su carrera profesional, se ha desempe-̃ado como Asociado del área de Seguridad Social en Chevez, Ruiz, Zamarripa y Cía, S.C. y Basham, Ringe y Correa, S.C. así como en diversos cargos en el Tribunal Federal de Justicia Fiscal
                    y Administrativa (TFJFA).

                    <p>Ha sido reconocido por la Comisión Representativa ante Organismos de Seguridad Social del Instituto Mexicano de Contadores Públicos (IMPC), así como uno de los abogados líderes en
                    México en esta práctica.

                    <p>En la actualidad funge como Síndico del Contribuyente de Coparmex, Sección Querétaro. Ha participado cómo expositor en diversos foros en materia fiscal y de seguridad social y ha publicado importantes artículos en revistas como HR Magazine,
                    Práctica Fiscal y Puntos Finos.

                    <p>Igualmente, se ha desempeñado como profesor Titular de la asignatura de “Derecho de la Seguridad Social” en la Facultad
                    de Derecho de la Universidad Anahuac en Querétaro.

            </div>


            <div class="column is-one-quarter has-text-centered">
                <figure class="image">
                    <img src="/cliente/img/socios/julian.jpg">
                </figure>
            </div>
            <div class="column is-three-quarters">
                <h1 class="title">Fernando Meraz Rodríguez</h1>
                <br>Oficina Qro.
                <p>Estudió la carrera de derecho en la Escuela Libre de Derecho.

                    <p>Sus áreas de práctica consisten en la consultoría, planeación, estrategia e implementación de toda clase de esquemas de adquisición, reestructura, modificación y enajenación de todo tipo de bienes inmuebles, así como en la constitución, modificación, transformación, fusión y escisión de toda clase de personas morales, mismas que ha desarrollado por más de 19 años.

                    <p>Durante 17 años se desempeñó en la Notaría 84 del Distrito Federal, fungiendo los últimos 5 como Coordinador Jurídico de la misma, teniendo a su cargo la implementación y supervisión de todos los instrumentos públicos otorgados en ella, tanto a solicitud de particulares, como a solicitud de diversas Autoridades en tratándose de programas públicos.

                    <p>Posteriormente fundó la firma MSR Abogados en la que participó durante 4 años, como encargado del área de consultoría en Derecho Corporativo e Inmobiliario.

            </div>







        </div>
    </div>

    @include('cliente.parciales.home.llamanos')

@stop
