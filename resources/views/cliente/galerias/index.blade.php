@extends('cliente.layout.cliente')
@section('titulo', 'Galerías')
@section('contenido')
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Entérate de las noticias<br> de último momento
            </p>
            <p class="subtitle is-4">
                Te mantenemos informado
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Galerias</a></li>
    @endslot
    @endbreadcrumb_basico

    @fila_basico
    @slot('descripcion')
    <h4 class="title is-3"> Galerías</h4>
    @endslot
    @slot('contenido')
    <div class="columns is-multiline">
        @foreach($fotografias as $fotografia)
            @foreach( $fotografia->galerias as $galeria)
                <div class="column is-4">
                    @comunicado_basico
                    @slot('urlFotoListado') {{ $galeria->fotoListado }} @endslot
                    @slot('titulo') {{ $galeria->nombre }} @endslot
                    @slot('introduccion'){!!  substr(strip_tags($galeria->descripcion),0,100) !!} @endslot
                    @slot('urlDetalle') {{ route('galerias.detalle', ['slug' => $galeria->slug]) }} @endslot
                    @endcomunicado_basico
                </div>
            @endforeach
        @endforeach
    </div>
    @endslot
    @endfila_basico

@endsection