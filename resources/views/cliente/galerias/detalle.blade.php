@extends('cliente.layout.cliente')
@section('titulo', $galeria->nombre)
@section('contenido')
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Entérate de las noticias<br> de último momento
            </p>
            <p class="subtitle is-4">
                Te mantenemos informado
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li><a href="{{ route('galerias.index') }}" class="has-text-white" aria-current="page">Galerias</a></li>
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page"> {{ $galeria->nombre }} </a></li>
    @endslot
    @endbreadcrumb_basico

    @fila_basico
    @slot('descripcion')
        <h4 class="title is-4"> {{ $galeria->nombre }}</h4>
    @endslot
    @slot('contenido')
        <div id="gallery" class="">
            @foreach($galeria->imagenesDeGaleria as $imagen)
                <div class="-gallery-item">
                    <a href="#" @click.prevent="openModalImageGallery($event)">
                        <figure class="image">
                            <img src="{{ $imagen['url'] }}" alt="">
                        </figure>
                    </a>
                </div>
            @endforeach
        </div>
    @endslot
    @endfila_basico
    {{--  <div id="modal-image-gallery" class="modal">
        <div class="modal-background"></div>
        <div class="modal-content">
            <p class="image">
                <img :src="modalGalleryImage" alt="">
            </p>
        </div>
        <button class="modal-close is-large" aria-label="close" @click.prevent="closeModalImageGallery()"></button>
    </div>  --}}
@endsection