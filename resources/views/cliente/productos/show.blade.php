@extends('cliente.layout.cliente')
@section('titulo', $producto->nombre)
@section('contenido')
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ $producto->nombre }}
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        {{--  <li><a href="{{ route('productos.index') }}" class="has-text-white" aria-current="page">Productos</a></li>  --}}
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ $producto->nombre}}</a></li>
    @endslot
    @endbreadcrumb_basico
    
    @fila_basico
        @slot('descripcion')
            <h4 class="title is-3">{{ $producto->nombre }}</h4>
        @endslot
        @slot('contenido')

                <h4 class="title is-5"> {{ __('messages.descripcion') }}</h4>

                <p>{!! $producto->descripcion !!}</p>

                <br>
                <h4 class="title is-5"> {{ __('messages.detalles') }}</h4>

                <p>{!! $producto->detalles !!}</p>




            <div class="-is-spaced">
                @foreach($producto->galerias as $galeria)
                    <div id="gallery" class="">
                        @foreach($galeria->imagenesDeGaleria as $imagen)
                            <div class="-gallery-item">
                                <a href="#" @click.prevent="openModalImageGallery($event)">
                                    <figure class="image">
                                        <img src="{{ $imagen['url'] }}" alt="">
                                    </figure>
                                    <h4 class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>
        @endslot
        @endfila_basico
@endsection