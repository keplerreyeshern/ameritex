@extends('cliente.layout.cliente')
@section('titulo', $categoria->nombre)
@section('contenido')
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Los mejores productos <br> al mejor precio
            </p>
            <p class="subtitle is-4">
                Ahorra con nosotros
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li><a href="{{ route('productos.index') }}" class="has-text-white" aria-current="page">Productos</a></li>
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ $categoria->nombre }}</a></li>
    @endslot
    @endbreadcrumb_basico

    @fila_basico
    @slot('descripcion')
        <h4 class="title is-3">Categorías</h4>
        @include('/cliente/parciales/productos/categorias')
    @endslot
    @slot('contenido')
        <div class="columns is-multiline">
            @if(count($categoria->productos) == 0)
                <div class="column">
                    <h4 class="title is-4 has-text-centered">
                        No hay productos para mostrar
                    </h4>
                </div>
            @endif
                @foreach($categoria->productos as $producto)
                    <div class="column is-4">
                        @tarjeta_basico
                        @slot('urlDetalle') {{ route('productos.show', ['slug' => $producto->slug ]) }} @endslot
                        @slot('urlImagen') {{ $producto->urlFotolistado }}  @endslot
                        @slot('titulo') {{ $producto->nombre}} @endslot
                        @slot('introduccion')
                            {!! $producto->descripcion !!}
                        @endslot
                        @endtarjeta_basico
                    </div>
                @endforeach
        </div>
    @endslot
    @endfila_basico
@endsection