@extends('cliente.layout.cliente')
@section('titulo', 'Lajas')
@section('contenido')

    {{--  Parallax  --}}
    @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/titulos/lajas.jpg') }} @endslot
    @slot('texto')
    <div class="has-text-right">
        <p class="subtitle is-2 has-text-white">
            {{ $producto->nombre }}
        </p>
    </div>
    @endslot
    @endparallax_basico

    {{--  Breadcrumb  --}}
    @breadcrumb_basico
    @slot('links')
    <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('messages.lajas') }}</a></li>
    @endslot
    @endbreadcrumb_basico

    {{--  Descripción  --}}
    <div class="-fdo-area" style="background-image: url(/cliente/img/fdo-{{ $material }}.jpg);">

            <div class="section -pdcto-desc">
                <div class="container">

                    <figure class="image is-128x128 is-clipped">
                        <img src="/files/productos/{{ $producto->fotolistado }}">
                    </figure>

                    <p>
                        {!!  $producto->descripcion  !!}
                    </p>
                </div>
            </div>

            <section class="section -pdcto-galeria">
                <div class="container">

                    <h4 class="title is-3">{{ __('messages.galeria') }}</h4>

                    <div id="gallery-portfolio">

                        @if($galeria)
                            @foreach ($galeria->imagenes_de_galeria as $imagen)
                                <div class="-gallery-item">
                                    <a href="#" @click.prevent="openModalImageGallery($event)">
                                        <figure class="image">
                                            <img src="{{ $imagen->url }}" alt="">
                                        </figure>
                                        <h4 class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>
                                    </a>
                                </div>
                            @endforeach
                        @endif



                        {{-- Mostramos 3 fotos de la galería --}}
                        {{--
                @foreach( $productos as $producto)
                    <div class="column is-4">
                        @comunicado_basico
                        @slot('urlFotoListado') {{ $producto->Urlfotolistado }} @endslot
                        @slot('titulo') {{ $producto->nombre }} @endslot
                        @slot('introduccion'){!!  substr(strip_tags($producto->descripcion),0,100) !!} @endslot
                        @slot('urlDetalle') {{ $producto->linkdetalle }} @endslot
                        @endcomunicado_basico
                    </div>
                @endforeach
                       --}}
                    </div>
                </div>
            </section>
    </div>

@endsection
