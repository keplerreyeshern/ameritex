@extends('cliente.layout.cliente')
@section('titulo', 'Diseño')
@section('contenido')

    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.diseno') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
    @slot('fondo')
        images/titulo/tecnologia.jpg
    @endslot

    @slot('titulo')
        {{ __('messages.diseno') }}
    @endslot
    @endjumbotron_titulo


    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['diseno']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['diseno']->contenido  !!}

        <div id="gallery-portfolio">

            @foreach ($portafolio->imagenes_de_galeria as $imagen)
                <div class="-gallery-item is-one-quarter">
                    <a href="#" @click.prevent="openModalImageGallery($event)">
                        <figure class="image">
                            <img src="{{ $imagen->url }}" alt="">
                        </figure>
                    <!--<h4 class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>-->
                    </a>
                </div>
            @endforeach

        </div>
    @endslot
    @endfila_basico









@stop