@extends('cliente.layout.cliente')
@section('titulo', 'Portafolio')
@section('contenido')

    <!--
  <div  id="modal-image-gallery" class="modal">
    <div  class="modal-background"></div>
      <div class="modal-content">
        <p class="image">
            <img :src="modalGalleryImage" alt="">
        </p>
      </div>
    <button class="modal-close is-large" aria-label="close" @click.prevent="closeModalImageGallery()"></button>
  </div>

  -->

  @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/titulos/portafolio.jpg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ __('Portfolio') }}
            </p>
        </div>
    @endslot
  @endparallax_basico

  @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('Portfolio') }}</a></li>
    @endslot
  @endbreadcrumb_basico

    <section id="portfolio-section" class="section">
        <div id="gallery-portfolio">

            @foreach ($portafolio->imagenes_de_galeria as $imagen)
                <div class="-gallery-item">
                    <a href="#" @click.prevent="openModalImageGallery($event)">
                        <figure class="image">
                            <img src="{{ $imagen->url }}" alt="">
                        </figure>
                        <h4 class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>
                    </a>
                </div>
            @endforeach

        </div>
    </section>

@endsection