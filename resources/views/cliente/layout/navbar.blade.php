<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">



        <a class="" href="/">
            <img src="/cliente/img/logo-americantextil.jpg"  class="logo">
        </a>
        <span class="slogan subtitle">Diseñando el futuro y tejiendo el presente</span>


        <a role="button" class="navbar-burger burger is-block" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>

    </div>

    <div id="navbarBasicExample" class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item" href="/quienes-somos">
                {{ __('messages.quienes-somos') }}
            </a>

            <a class="navbar-item" href="/productos">
                {{ __('messages.productos') }}
            </a>

            <a class="navbar-item" href="/calidad">
                {{ __('messages.calidad') }}
            </a>

            <a class="navbar-item" href="/tecnologia">
                {{ __('messages.tecnologia') }}
            </a>

            <a class="navbar-item" href="/diseno">
                {{ __('messages.diseno') }}
            </a>

            <a class="navbar-item" href="/proveedores">
                {{ __('messages.proveedores') }}
            </a>

            <a class="navbar-item" href="{{ route('sutentabilidad') }}">
                {{ __('messages.sustentabilidad') }}
            </a>

            <a class="navbar-item" href="/contacto">
                {{ __('messages.contacto') }}
            </a>

            @if (App::isLocale('en'))
                <a class="navbar-item has-text-weight-semibold" href="{{ route('lang', ['locale' => 'es']) }}">
                    Versión español
                </a>
            @elseif (App::isLocale('es'))
                <a class="navbar-item has-text-weight-semibold" href="{{ route('lang', ['locale' => 'en']) }}">
                    English version
                </a>
            @endif
            <div class="-is-spaced"></div>
        </div>
    </div>
</nav>