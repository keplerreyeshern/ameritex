<!doctype html>
<html lang="es-MX">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ config('app.name') }} - @yield('titulo') </title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
    <link rel="stylesheet" href="{{ asset('cliente/css/cliente.css') }}">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"></script>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src='https://www.google.com/recaptcha/api.js'></script>
    @yield('head')
</head>
<body>
    <div id="app">
        <div id="modal-image-gallery" class="modal">
            <div @click.prevent="closeModalImageGallery()" class="modal-background"></div>
            <div class="modal-content">
                <p class="image">
                    <img :src="modalGalleryImage" alt="">
                </p>
            </div>
            <button class="modal-close is-large" aria-label="close" @click.prevent="closeModalImageGallery()"></button>
        </div>

        @include('cliente.layout.navbar')

        @yield('contenido')

        @include('cliente.layout.footer')
    </div>


    <script src="https://kit.fontawesome.com/231eaacd25.js" crossorigin="anonymous"></script>


    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '223828761534906',
                xfbml      : true,
                version    : 'v3.0'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/es_LA/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {

        // Get all "navbar-burger" elements
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {

            // Add a click event on each of them
            $navbarBurgers.forEach( el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
            });
        }

        });
    </script>
</body>
</html>