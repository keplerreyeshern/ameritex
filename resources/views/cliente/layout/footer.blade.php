<br><br>

@include('cliente.parciales.home.iso9001')

<br>
<footer class="footer is-paddingless">

        <div class="content">
            <div class="columns">

                <div class="column is-one-quarter">
                    <img id="logo" src="{{asset('cliente/img/logo-americantextil.jpg')}}"><br>
                    <a href="/quienes-somos" class=" has-text-weight-semibold is-size-5"> {{ __('messages.quienes-somos') }}</a><br>
                    <a href="/aviso-de-privacidad" class=" has-text-weight-semibold is-size-5">{{ __('messages.aviso') }}</a>
                </div>

                <div class="column is-half has-text-centered">
                    <a href="/ubicacion"><img src="cliente/img/mapa.jpg"></a>
                </div>

                <div class="column">

                    <a href="https://m.facebook.com/americanspecializedtextiles/" class="is-size-2"><i class="fab fa-facebook-square"></i></a>
                    <a href="https://instagram.com/americanspecializedtextiles?igshid=r6p6o0dxx1vf" class="is-size-2"><i class="fab fa-instagram"></i></a>
                    <a href="https://twitter.com/AmericanSpeTex?s=09" class="is-size-2" ><i class="fab fa-twitter-square"></i></a>

                    <p class="is-size-5"><i class="fas fa-phone"></i> (55)56 99 22 25</p>
                    <p>Vía Morelos No. 66
                        <br>Col Rústica Xalostoc.
                        <br>Ecatepec, Edo. de Méx.</p>
                    <a href="mailto:ventas@ameritex.com.mx" class="is-size-5">ventas@ameritex.com.mx</a>

                </div>
            </div>
        </div>

</footer>

<div class="is-radiusless -box-all-rights-reserved">
    <div class="has-text-centered is-size-7">
        {{ __('messages.copy') }}
    </div>
</div>


