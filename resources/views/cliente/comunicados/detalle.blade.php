@extends('cliente.layout.cliente')
@section('titulo', $comunicado->titulo)
@section('head')
    <meta property="og:url"           content="{{ Request::url() }}" />
    <meta property="og:type"          content="Comunicado" />
    <meta property="og:title"         content="{{ $comunicado->titulo }}" />
    <meta property="og:description"   content="{!! strip_tags($comunicado->intro) !!}" />
    <meta property="og:image"         content="{{ $comunicado->Urlfotolistado }}" />
@endsection
@section('contenido')
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {!!  __('messages.comunicados-detalle-titulo')  !!}

            </p>
            <p class="subtitle is-4">
                {!!  __('messages.comunicados-detalle-subtitulo')  !!}
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li><a href="{{ route('comunicados.index') }}" class="has-text-white" aria-current="page">Comunicados</a></li>
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ $comunicado->titulo }}</a></li>
    @endslot
    @endbreadcrumb_basico


    <div class="container -comunicados-detalle">

        <div class="columns">
            <div class="column is-one-quarter ">
                <h4 class="subtitle is-4 has-text-black "> {{ $comunicado->titulo }}</h4>
                <h3 class="subtitle is-6 is-marginless">{{ __('messages.publicado-por') }} {{ $comunicado->autor }}</h3>
                <h3 class="subtitle is-6">{{$comunicado->fechaFormateada}}</h3>
            </div>


            <div class="column is-three-quarters">
                <figure class="image">
                    <img src="@isset($comunicado->Urlfotolistado){{ $comunicado->Urlfotolistado }}@endisset" class="-object-fit-cover -is-fullheight" alt="">
                </figure>
                <div class="-descripcion">
                    {!!  $comunicado->descripcion !!}
                </div>


                <div class="has-text-right is-flex">
                    <div class="is-flex" style="margin-left: auto">
                        <div class="fb-share-button"
                             data-href="{{ Request::url() }}"
                             data-size="large"
                             data-action="share"
                             data-layout="button_count">
                        </div>
                        <a class="twitter-share-button"
                           href="https://twitter.com/intent/tweet?text="
                           data-size="large">
                            Tweet</a>
                        <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                </div>

            </div>




        </div>



    </div>



@endsection