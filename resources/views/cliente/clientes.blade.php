
@extends('cliente.layout.cliente')
@section('titulo', 'Clientes')
@section('contenido')

    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Nacionales e internacionales
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Clientes</a></li>
    @endslot
    @endbreadcrumb_basico

    <div class="container -sectores">
        <div class="columns is-multiline ">

            <div class="column is-one-quarter has-text-centered">
                <h1 class="title">Algunos de nuestros clientes</h1>
            </div>
            <div class="column is-three-quarters">
                <ul class="-lista-margen">
                    <li><p>Grupo Aeroméxico
                    <li><p>Mexicana de Aviación
                    <li><p>Samsung Electronics México
                    <li><p>Electrónica Steren
                    <li><p>Grupo Alpura
                    <li><p>Metco
                    <li><p>Enlaces Terrestres Na- cionales (ETN)
                    <li><p>Grupo Infra
                    <li><p>Sexy Jeans
                    <li><p>Grupo Techint
                    <li><p>Daymon Worldwide Mexico	•	Roché Bobois
                    <li><p>Bang & Olufsen
                    <li><p>Buckman Laborato- rios
                    <li><p>Química Sagitario
                    <li><p>Grupo Pegaso
                    <li><p>Grupo Techint
                    <li><p>Mayware
                    <li><p>Royce Corporation
                    <li><p>El Nuevo Mundo
                    <li><p>Gildan Activewear
                    <li><p>Akzo Nobel Chemi- cals


                </ul>
            </div>





        </div>
    </div>

    @include('cliente.parciales.home.llamanos')

@stop
