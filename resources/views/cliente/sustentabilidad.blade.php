@extends('cliente.layout.cliente')
@section('titulo', 'Sustentabilidad')
@section('contenido')

    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.sustentabilidad') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
    @slot('fondo')
        images/titulo/quienes-somos.jpg
    @endslot

    @slot('titulo')
        {{ __('messages.sustentabilidad') }}
    @endslot
    @endjumbotron_titulo


    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['sustentabilidad']->titulo or 'Por Definir' !!}</h4>

    @endslot
    @slot('contenido')
        {!! $parrafos['sustentabilidad']->contenido or 'Por Definir' !!}
        <br>
        <img src="cliente/img/susten.png">
    @endslot
    @endfila_basico

@stop