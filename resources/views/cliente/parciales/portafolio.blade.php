

<hr>

<section id="portfolio-section" class="section container">

    <div class="column is-full has-text-centered">
        <h1 class="subtitle">{{ __('messages.galeria') }}</h1>
    </div>


    <div id="gallery-portfolio">
        @foreach ($portafolio->imagenes_de_galeria as $imagen)
            <div class="-gallery-item is-one-quarter">
                <a href="#" @click.prevent="openModalImageGallery($event)">
                    <figure class="image">
                        <img src="{{ $imagen->url }}" alt="">
                    </figure>
                    <!--<h4 class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>-->
                </a>
            </div>
        @endforeach
    </div>


</section>