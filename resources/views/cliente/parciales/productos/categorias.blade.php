<div class="menu menu-categories">
    @foreach($categorias as $categoria)
        <ul class="menu-list">
            <li>
                <a class="{{ (session('categoriaActual') and session('categoriaActual')->id === $categoria->id) ? 'is-active': ''  }}"  href="{{ route('productos.categoria', ['slug' => $categoria->slug]) }}">
                    {{ $categoria->nombre }}
                </a>
            </li>
            @if(count($categoria->childrenRecursive) > 0)
                @component('cliente/parciales/productos/subcategoria',['categoria' => $categoria])
                @endcomponent
            @endif
        </ul>
    @endforeach
</div>