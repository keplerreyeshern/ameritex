{{-- Es usado como componente en las categorías --}}
<li>
    <ul>
        @foreach($categoria->childrenRecursive as $subcategoria)
            <li>
                <a class="{{ (session('categoriaActual') and session('categoriaActual')->id === $subcategoria->id) ? 'is-active': ''  }}" href="{{ route('productos.categoria', ['slug' => $subcategoria->slug]) }}">
                    {{ $subcategoria->nombre }}
                </a>
            </li>
            @component('cliente/parciales/productos/subcategoria',['categoria' => $subcategoria])
            @endcomponent
        @endforeach
    </ul>
</li>