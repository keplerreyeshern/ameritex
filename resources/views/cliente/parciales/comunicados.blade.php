<section class=" -lista-comunicados container">
@fila_basico


        @slot('descripcion')
            <h2 class="-subtitulo is-3">{{ __('News') }}</h2>
            <a href="/comunicados">Ver todos <i class="fas fa-arrow-right"></i></a>
        @endslot


        @slot('contenido')

            <div class="columns is-multiline">
                {{-- Mostramos 3 comunicados de la categoria --}}

                @foreach( $categoriasComunicados as $comunicado)

                    <div class="column is-one-third">

                        @comunicado_basico
                        @slot('linkFotoListado') {{ $comunicado->linkdetalle }} @endslot
                        @slot('urlFotoListado') {{ $comunicado->Urlfotolistado }} @endslot
                        @slot('titulo') {{ $comunicado->titulo }} @endslot
                        @slot('introduccion'){!!  substr(strip_tags($comunicado->intro),0,150) !!} @endslot
                        @slot('urlDetalle') {{ $comunicado->linkdetalle }} @endslot
                        @endcomunicado_basico

                    </div>

                @endforeach

            </div>


        @endslot

    @endfila_basico
</section>


