


@parallax_basico
@slot('urlImagen') {{ asset('base/img/llamanos.jpeg') }} @endslot
@slot('texto')
    <h2 class="title has-text-white">{{ __('messages.llamanos') }}</h2>
    <h2 class="title has-text-white"> CDMX <a href="tel:{{ env('CONTACT_NUMBER_UNFORMATED', '+52 5556992225') }}">{{ env('CONTACT_NUMBER', '+52 (55)5699 2225') }}</a></h2>

@endslot
@endparallax_basico