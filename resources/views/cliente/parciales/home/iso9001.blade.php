

@if($quality)
    @parallax_basico
    @slot('urlImagen') {{ asset('images/backiatf.jpg') }} @endslot
    @slot('texto')
        <div class="columns is-desktop">
            <div class="column">
                <div class="title text-center">
                    <h4 class="has-text-white"><span class="has-text-white">{{ $quality->title }}</h4>
                </div>
            </div>
            <div class="column">
                <img src="{{ asset( $quality->urlImage ) }}" class="img-fluid w-100" alt="IAFT">
            </div>
            <div class="column">
                <div class="title text-center">
                    <h4 class="has-text-white">{!!  $quality->content !!} </h4>
                </div>
            </div>
        </div>
    @endslot
    @endparallax_basico
@endif
