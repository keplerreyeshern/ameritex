
<section class="-contenido -areas container">

    <div class="column is-full has-text-centered">
        <h1 class="subtitle">{{ $parrafos['bienvenidos']->titulo }}</h1>
        <br>
        <p class="-definicion">{!! $parrafos['bienvenidos']->contenido  !!} </p>
    </div>

    <div class="columns is-multiline -home-">


        <div class="column is-one-quarter">

            <div class="card has-text-centered">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <img src="{{asset('/cliente/img/icono/deportivo.jpg')}}" class="-centered is-128x128 ">
                                </div>
                            </div>
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <h1 class="title">{{ $parrafos['deportivo']->titulo }}</h1>
{{--                                    <p class="-definicion">{!! $parrafos['deportivo']->contenido   !!}</p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <div class="">
                                <a href="/productos/#deportivo">
                                <img src="{{ asset('/cliente/images/products/background/deportivo.jpeg') }}" class="img-lineas w-100">
{{--                                <p class="-definicion">{!! $parrafos['deportivo']->contenido   !!}</p>--}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="column is-one-quarter">

            <div class="card has-text-centered">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <img src="{{asset('/cliente/img/icono/ropa-intima.jpg')}}" class="-centered is-128x128 ">
                                </div>
                            </div>
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <h1 class="title">{{ $parrafos['ropa-intima']->titulo }}</h1>
                                    {{--                                    <p class="-definicion">{!! $parrafos['ropa-intima']->contenido   !!}</p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <div class="">
                                <a href="/productos/#ropa-intima">
                                    <img src="{{ asset('/cliente/images/products/background/ropaintima.jpeg') }}" class="img-lineas w-100">
                                    {{--                                <p class="-definicion">{!! $parrafos['ropa-intima']->contenido   !!}</p>--}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="column is-one-quarter">

            <div class="card has-text-centered">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <img src="{{asset('/cliente/img/icono/textiles-tecnicos.jpg')}}" class="-centered is-128x128 ">
                                </div>
                            </div>
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <h1 class="title">{{ $parrafos['textiles-tecnicos']->titulo }}</h1>
                                    {{--                                    <p class="-definicion">{!! $parrafos['textiles-tecnicos']->contenido   !!}</p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <div class="">
                                <a href="/productos/#textiles-tecnicos">
                                    <img src="{{ asset('/cliente/images/products/background/textilestecnicos.jpeg') }}" class="img-lineas w-100">
                                    {{--                                <p class="-definicion">{!! $parrafos['textiles-tecnicos']->contenido   !!}</p>--}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>


        <div class="column is-one-quarter">

            <div class="card has-text-centered">
                <div class="flip-card">
                    <div class="flip-card-inner">
                        <div class="flip-card-front">
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <img src="{{asset('cliente/img/icono/automotriz.jpg')}}" class="-centered is-128x128 ">
                                </div>
                            </div>
                            <div class="columns is-mobile is-centered">
                                <div class="column is-half">
                                    <h1 class="title">{{ $parrafos['automotriz']->titulo }}</h1>
                                    {{--                                    <p class="-definicion">{!! $parrafos['automotriz']->contenido   !!}</p>--}}
                                </div>
                            </div>
                        </div>
                        <div class="flip-card-back">
                            <div class="">
                                <a href="/productos/#automotriz">
                                    <img src="{{ asset('/cliente/images/products/background/impermeable.jpeg') }}" class="img-lineas w-100">
                                    {{--                                <p class="-definicion">{!! $parrafos['automotriz']->contenido   !!}</p>--}}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>

</section>

