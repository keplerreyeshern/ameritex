
<section class="-servicio container">

    <div class="columns is-multiline">

        <div class="column is-one-quarter -servicio1">
            <div class="card">
                <h1 class="title has-text-white">{{ $parrafos['azul-servicio']->titulo }}</h1>
                <p class="-definicion">{!! str_limit($parrafos['azul-servicio']->contenido, 170 )  !!} </p>
                <a href="/proveedores"><button class="button is-primary is-inverted is-outlined">{{ __('messages.conocer-mas') }}</button></a>
            </div>
        </div>

        <div class="column is-one-quarter -servicio2">
            <div class="card ">
                    <h1 class="title has-text-white">{{ $parrafos['azul-calidad']->titulo }}</h1>
                    <p class="-definicion">{!! str_limit($parrafos['azul-calidad']->contenido, 170 )  !!} </p>
                <a href="/calidad"><button class="button is-primary is-inverted is-outlined">{{ __('messages.conocer-mas') }}</button></a>

            </div>

        </div>

        <div class="column is-one-quarter -servicio3">
            <div class="card ">
                    <h1 class="title has-text-white">{{ $parrafos['azul-tecnologia']->titulo }}</h1>
                    <p class="-definicion">{!! str_limit($parrafos['azul-tecnologia']->contenido, 170 )  !!} </p>
                <a href="/tecnologia"><button class="button is-primary is-inverted is-outlined">{{ __('messages.conocer-mas') }}</button></a>
            </div>

        </div>


        <div class="column is-one-quarter -servicio4">

            <div class="card ">

                    <h1 class="title has-text-white">{{ $parrafos['azul-diseno']->titulo }}</h1>
                    <p class="-definicion">{!! str_limit($parrafos['azul-diseno']->contenido, 170 )  !!}</p>
                <a href="/diseno"><button class="button is-primary is-inverted is-outlined">{{ __('messages.conocer-mas') }}</button></a>

            </div>

        </div>

    </div>


</section>

