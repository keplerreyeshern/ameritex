@extends('cliente.layout.cliente')
@section('titulo', 'Calidad')
@section('contenido')

    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.calidad') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
    @slot('fondo')
        images/titulo/calidad.jpg
    @endslot

    @slot('titulo')
        {{ __('messages.calidad') }}
    @endslot
    @endjumbotron_titulo


    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['calidad']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['calidad']->contenido  !!}

        <br>

        <section  class="section">
            <div id="gallery-portfolio">

                @foreach ($portafolio->imagenes_de_galeria as $imagen)
                    <div class="-gallery-item">
                        <a href="#" @click.prevent="openModalImageGallery($event)">
                            <figure class="image">
                                <img src="{{ $imagen->url }}" alt="">
                            </figure>
{{--                            <h4 class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>--}}
                        </a>
                    </div>
                @endforeach

            </div>
        </section>
        <br>
        <img src="{{ asset($quality->urlImage) }}" class="img-fluid" alt="Certificado de Calidad">
    @endslot
    @endfila_basico









@stop