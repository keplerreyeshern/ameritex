@extends('cliente.layout.cliente')
@section('titulo', 'Quienes somos')
@section('contenido')

    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.quienes-somos') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
        @slot('fondo')
            images/titulo/quienes-somos.jpg
        @endslot

        @slot('titulo')
            {{ __('messages.quienes-somos') }}
        @endslot
    @endjumbotron_titulo

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['mision']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['mision']->contenido  !!}
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['vision']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['vision']->contenido  !!}
    @endslot
    @endfila_basico


    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['bienvenidos']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['bienvenidos']->contenido  !!}
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['politica']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['politica']->contenido  !!}
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['seguridad']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['seguridad']->contenido  !!}
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['ambiente']->titulo  !!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['ambiente']->contenido  !!}
    @endslot
    @endfila_basico




@stop