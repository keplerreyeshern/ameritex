
@extends('cliente.layout.cliente')
@section('titulo', 'La Firma')
@section('contenido')

    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Fundada en 2008<br>
                generando relaciones estrechas y duraderas
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">La Firma</a></li>
    @endslot
    @endbreadcrumb_basico

   <div class="container -la-firma">
       <div class="columns is-multiline ">

           <div class="column is-one-quarter has-text-centered">
               <h1 class="title">Quiénes somos</h1>
           </div>
           <div class="column is-three-quarters">
                    <p>Somos una firma de abogados fundada en 2008, que tiene como propósito fundamental prestar servicios legales altamente especializados, personalizados, confiables, de calidad, con profesionalismo
                        y honestidad.

                        <p>De acuerdo a esos valores, deseamos generar una relación estrecha y duradera con nuestros clientes .</p>
           </div>

           <div class="column is-one-quarter has-text-centered">
               <h1 class="title">Misión</h1>
           </div>
           <div class="column is-three-quarters">
               <p>Nuestra misión consiste en lograr la “Satisfacción Total” de nuestros clientes, a partir de la prestación de servicios legales basados en la honestidad, excelencia y profesionalismo.</p>
           </div>


           <div class="column is-one-quarter has-text-centered">
               <h1 class="title">Qué nos hace diferentes</h1>
           </div>
           <div class="column is-three-quarters">
               <ul>
                   <li><p> Atención personalizada de nuestros socios.
                           <li><p> Creatividad en las operaciones cotidianas, ya
                           que buscamos la mejor solución al problema de nuestros clientes.
                           <li><p> Flexibilidad para ajustarnos a las necesidades y requerimientos de nuestros clientes.
                           <li><p> Honestidad y Compromiso constante.
                           <li><p> Proactivos, ya que estamos en constante comunicación con nuestros clientes para atender sus necesidades y detectar áreas de oportunidad.
                           <li><p> Alta especialización en las áreas del Derecho en que nos desempeñamos.
                           <li><p> Liderazgo en las negociaciones y operaciones que celebran nuestros clientes.
                           <li><p> Disponibilidad para estar cerca de nuestros clientes cuando así lo requieren.
                           <li><p> Experiencia en el desarrollo de nuestro trabajo.
                           <li><p> Trabajo en equipo para lograr las metas deseadas.
                           <li><p> Contamos con oficinas en la Cd. de México y Querétaro.
               </ul>

               <div class="box">
                   <p>"El autentico líder no es el que busca el concenso, sino el que lo produce"
                   <br><br>
                   Martin Luther King
               </div>
           </div>



       </div>
    </div>

    @include('cliente.parciales.home.llamanos')

@stop