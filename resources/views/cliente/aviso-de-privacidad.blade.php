@extends('cliente.layout.cliente')
@section('titulo', 'Aviso de privacidad')
@section('contenido')

    <!----parallax ------>
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Protección de datos
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
    <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('messages.aviso') }}</a></li>
    @endslot
    @endbreadcrumb_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4">{{ __('messages.aviso') }}</h4>  @endslot
    @slot('contenido')




        <h4 class="title is-4">{!! $parrafos['aviso-dirigido']->titulo  !!}</h4>
        {!! $parrafos['aviso-dirigido']->contenido  !!}


        <br>
        <h4 class="title is-4">{!! $parrafos['aviso-sobre']->titulo  !!}</h4>
        {!! $parrafos['aviso-sobre']->contenido  !!}</h4>



    @endslot
    @endfila_basico

@stop