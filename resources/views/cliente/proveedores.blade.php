@extends('cliente.layout.cliente')
@section('titulo', 'Proveedores')
@section('contenido')

    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.proveedores') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
    @slot('fondo')
        images/titulo/quienes-somos.jpg
    @endslot

    @slot('titulo')
        {{ __('messages.proveedores') }}
    @endslot
    @endjumbotron_titulo


    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['intro']->titulo or 'Intro->titulo' !!}</h4>

    @endslot
    @slot('contenido')
        {!! $parrafos['intro']->contenido or 'intro->contenido' !!}
        <br>
        <img src="cliente/img/proveedores/proveedores-principal.jpg">
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['manufactura']->titulo  or 'manufactura->titulo'!!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['manufactura']->contenido  or 'manufactura->contenido'!!}
        <br>
        <img src="cliente/img/proveedores/manufactura.jpg">
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['calidad']->titulo  or 'calidad->titulo'!!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['calidad']->contenido  or 'calidad->contenido'!!}
        <br>
        <img src="cliente/img/proveedores/calidad.jpg">
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['servicio']->titulo  or 'servicio->titulo'!!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['servicio']->contenido  or 'servicio->contenido'!!}
        <br>
        <img src="cliente/img/proveedores/servicio.jpg">
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['tecnologia']->titulo  or 'tecnologia->titulo'!!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['tecnologia']->contenido  or 'tecnologia->contenido'!!}
        <br>
        <img src="cliente/img/proveedores/tecnologia.jpg">
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{!! $parrafos['precio']->titulo  or 'precio->titulo'!!}</h4> @endslot
    @slot('contenido')

        {!! $parrafos['precio']->contenido  or 'precio->contenido'!!}
        <br>
        <img src="cliente/img/proveedores/precio.jpg">
    @endslot
    @endfila_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4 -azul">{{ __('messages.contacto') }}</h4> @endslot
    @slot('contenido')


        {{ __('messages.material-indirecto') }} <a href="mailto:mgalindo@ameritex.com.mx">mgalindo@ameritex.com.mx</a>
        <br>
        {{ __('messages.material-directo') }} <a href="mailto:gcompras@ameritex.com.mx">gcompras@ameritex.com.mx</a>
    @endslot
    @endfila_basico


@stop