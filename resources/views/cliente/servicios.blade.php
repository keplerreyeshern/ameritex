@extends('cliente.layout.cliente')
@section('titulo', 'Servicios')
@section('contenido')
    <!----- parallax ------>
    @parallax_basico
        @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
        @slot('texto')
            <div class="has-text-right">
                <p class="subtitle is-2 has-text-white">
                    El servicio de <br>
                    la más alta calidad
                </p>
            </div>
        @endslot
    @endparallax_basico

    <!----- breadcrumb ------>
    @breadcrumb_basico
        @slot('links')
            <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Servicios</a></li>
        @endslot
    @endbreadcrumb_basico

    <!----- sección ------>
    <section class="container -is-spaced">
        <div class="box -is-transparent -has-border">
            <p>
                {!!  $seccionServicio->contenido !!}
            </p>
        </div>
    </section>

    <!----- servicios ------>
    <section class="section">
        <div class="container">
            @foreach($servicios as $servicio)
                <div class="box -is-transparent -has-border">
                    <div class="columns">
                        <div class="column is-3">
                            <h4 class="title is-3">{{ $servicio->titulo }}</h4>
                        </div>
                        <div class="column is-3">
                            <figure class="image">
                                <img src="{{ $servicio->urlImagen }}" alt="">
                            </figure>
                        </div>
                        <div class="column">
                            <p>{!!  $servicio->descripcion !!}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@stop