@extends('cliente.layout.cliente')
@section('titulo', 'Comunicados')
@section('contenido')

    @parallax_basico

    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ __('messages.noticiasTit1') }} <br> {{ __('messages.noticiasTit2') }}
            </p>
        </div>
    @endslot

    @endparallax_basico



    @breadcrumb_basico
        @slot('links')
            <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('News') }}</a></li>
        @endslot
    @endbreadcrumb_basico
    {{-- Creamos una fila por cada categoria--}}


    <section class=" -lista-comunicados container">
        @fila_basico

            @slot('descripcion')
            <h2 class="-subtitulo is-3">{{ __('News') }}</h2>
            @endslot


            @slot('contenido')

                <div class="columns is-multiline">
                    {{-- Mostramos 3 comunicados de la categoria --}}

                    @foreach( $categoriasComunicados as $comunicado)

                        <div class="column is-one-third">

                            @comunicado_basico
                                @slot('linkFotoListado') {{ $comunicado->linkdetalle }} @endslot
                                @slot('urlFotoListado') {{ $comunicado->Urlfotolistado }} @endslot
                                @slot('titulo') {{ $comunicado->titulo }} @endslot
                                @slot('introduccion'){!!  substr(strip_tags($comunicado->intro),0,150) !!} @endslot
                                @slot('urlDetalle') {{ $comunicado->linkdetalle }} @endslot
                            @endcomunicado_basico

                        </div>

                    @endforeach

                </div>


            @endslot

        @endfila_basico
    </section>


    <div class="columns is-centered">
        <div class="column  is-half">{{ $categoriasComunicados->appends(Request::except('page'))->links()  }}</div>
    </div>
@endsection