@extends('cliente.layout.cliente')
@section('titulo', 'Contacto')
@section('contenido')
    @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/titulos/contacto.jpg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ __('messages.contacto') }}
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('Contact') }}</a></li>
    @endslot
    @endbreadcrumb_basico

    @fila_basico
    @slot('descripcion')
        <h4 class="title is-3">{{ __('Contacto') }}</h4>
        <span>
            <i class="far fa-envelope-open fa-10x"></i>
        </span>
    @endslot
    @slot('contenido')
        <div class="box -is-transparent">
            <h4 class="title">{{ __('messages.contactoEstamos') }}</h4>
            <p>
                {{ __('messages.contactoEstamosDesc') }}
            </p>
        </div>
        @if(Session::has('message'))
            <div class="message is-success">
                <div class="message-body">
                    {{ Session::get('message') }}
                </div>
            </div>
        @endif
        @if (count($errors) > 0)
            <article class="message is-danger">
                <div class="message-body">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="has-text-danger">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </article>
        @endif
        <form action="{{ route('mail.contacto') }}" method="post">
            {{ csrf_field() }}
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label ">{{ __('messages.nombre') }}</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input name="nombre" class="input is-radiusless" type="text" value="{{ old('nombre') }}">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label ">{{ __('messages.tel') }}</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input name="telefono" class="input is-radiusless" type="text" value="{{ old('telefono') }}">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label ">{{ __('messages.mail') }}</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <input name="correo" class="input is-radiusless" type="email" value="{{ old('correo') }}">
                        </p>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label ">{{ __('messages.mensaje') }}</label>
                </div>
                <div class="field-body">
                    <div class="field">
                        <p class="control">
                            <textarea name="mensaje" class="textarea is-radiusless">{{ old('mensaje') }}</textarea>
                        </p>
                    </div>
                </div>
            </div>
            <div class="field has-addons has-addons-right">
                <p class="control">
                    <div class="g-recaptcha" data-sitekey="{{env('NOCAPTCHA_SITEKEY')}}"></div>
                </p>
            </div>
            <hr>
            <p>
                {{ __('messages.contactoPrivacidad') }}
                <a class="has-text-link" href="aviso-de-privacidad">{{ __('messages.aviso') }}</a>


            </p>
            <div>
                <button class="button is-pulled-right has-text-danger  has-text-weight-bold">{{ __('messages.enviar') }}</button>
            </div>
        </form>
    @endslot
    @endfila_basico
@stop