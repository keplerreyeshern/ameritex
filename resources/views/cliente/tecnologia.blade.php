@extends('cliente.layout.cliente')
@section('titulo', 'Tecnología')
@section('contenido')

    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.tecnologia') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
    @slot('fondo')
        images/titulo/tecnologiaHilos.jpg
    @endslot

    @slot('titulo')
        {{ __('messages.tecnologia') }}
    @endslot
    @endjumbotron_titulo

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Urdido.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['urdido']){!!$parrafos['urdido']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['urdido']){!! $parrafos['urdido']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Tricot.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['tricot']){!!$parrafos['tricot']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['tricot']){!!$parrafos['tricot']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Circular.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['circular']){!!$parrafos['circular']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['circular']){!!$parrafos['circular']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Tintoreria.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['tintoreria']){!!$parrafos['tintoreria']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['tintoreria']){!!$parrafos['tintoreria']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Acabado.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['acabado']){!!$parrafos['acabado']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['acabado']){!!$parrafos['acabado']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Estampado.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['estampado']){!!$parrafos['estampado']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['estampado']){!!$parrafos['estampado']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Maquinas.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['maquinas']){!!$parrafos['maquinas']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['maquinas']){!!$parrafos['maquinas']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Laminado.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['laminado']){!!$parrafos['laminado']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['laminado']){!!$parrafos['laminado']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big

    @parallax_big
    @slot('urlImagen') {{ asset('cliente/images/products/background/Inspeccion.jpg') }} @endslot
    @slot('title')<h2 class="title-parallax has-text-white">@isset($parrafos['inspeccion']){!!$parrafos['inspeccion']->titulo !!} @else Por definir @endisset</h2>@endslot
    @slot('description')<h4 class="paragraph-parallax has-text-white">@isset($parrafos['inspeccion']){!!$parrafos['inspeccion']->contenido !!} @else Por definir @endisset</h4>@endslot
    @endparallax_big


{{--    @fila_basico--}}
{{--    @slot('descripcion') <h4 class="title is-4 -azul ">{!! $parrafos['tecnologia']->titulo  !!}</h4> @endslot--}}
{{--    @slot('contenido')--}}

{{--        {!! $parrafos['tecnologia']->contenido  !!}--}}

{{--        <div id="gallery-portfolio">--}}

{{--            @foreach ($portafolio->imagenes_de_galeria as $imagen)--}}
{{--                <div class="-gallery-item is-one-quarter">--}}
{{--                    <a href="#" @click.prevent="openModalImageGallery($event)">--}}
{{--                        <figure class="image">--}}
{{--                            <img src="{{ $imagen->url }}" alt="">--}}
{{--                        </figure>--}}
{{--                    <!--<h4 otro cambio class="subtitle is-5 has-text-centered">{{ $imagen['original_name'] }}</h4>-->--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--            @endforeach--}}

{{--        </div>--}}

{{--    @endslot--}}
{{--    @endfila_basico--}}
@stop