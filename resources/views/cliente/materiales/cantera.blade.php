@extends('cliente.layout.cliente')
@section('titulo', 'Cantera')
@section('contenido')

  {{--  Parallaz  --}}
  @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/titulos/cantera.jpg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ __('messages.cantera') }}
            </p>
        </div>
    @endslot
  @endparallax_basico

    {{--  Breadcrumb  --}}
    @breadcrumb_basico
        @slot('links')
            <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('messages.cantera') }}</a></li>
        @endslot
    @endbreadcrumb_basico

    {{--  Descripción  --}}
    <div class="section">
      <div class="container">
        <p>
            {{ __('messages.canteraDesc') }}
        </p>
      </div>
    </div>

    {{--  sws  --}}
    <section class="section">
      <div class="container">
        <div class="columns">
              {{-- Mostramos 3 comunicados de la categoria --}}
              @foreach( $productos as $producto)
                  <div class="column is-4">
                      @comunicado_basico
                      @slot('urlFotoListado') {{ $producto->Urlfotolistado }} @endslot
                      @slot('titulo') {{ $producto->nombre }} @endslot
                      @slot('introduccion'){!!  substr(strip_tags($producto->descripcion),0,100) !!} @endslot
                      @slot('urlDetalle') {{ $producto->linkdetalle }} @endslot
                      @endcomunicado_basico
                  </div>
              @endforeach
          </div>
      </div>
    </section>

  
@endsection
