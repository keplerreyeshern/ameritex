@extends('cliente.layout.cliente')
@section('titulo', 'Lajas')
@section('contenido')

  {{--  Parallaz  --}}
  @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/titulos/lajas.jpg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ __('messages.lajas') }}
            </p>
        </div>
    @endslot
  @endparallax_basico

    {{--  Breadcrumb  --}}
    @breadcrumb_basico
        @slot('links')
            <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('messages.lajas') }}</a></li>
        @endslot
    @endbreadcrumb_basico

    {{--  Descripción  --}}
    <div class="section">
      <div class="container">
        <p>
            {{ __('messages.lajasDesc') }}
        </p>
      </div>
    </div>

    {{--  sws  --}}
    <section class="section">
      <div class="container">

         <h4 class="title is-3">{{ __('messages.galeria') }}</h4>

        <div class="columns">
              {{-- Mostramos 3 comunicados de la categoria --}}
              @foreach( $productos as $producto)
                  <div class="column is-4">
                      @comunicado_basico
                      @slot('urlFotoListado') {{ $producto->Urlfotolistado }} @endslot
                      @slot('titulo') {{ $producto->nombre }} @endslot
                      @slot('introduccion'){!!  substr(strip_tags($producto->descripcion),0,100) !!} @endslot
                      @slot('urlDetalle') {{ $producto->linkdetalle }} @endslot
                      @endcomunicado_basico
                  </div>
              @endforeach
          </div>
      </div>
    </section>

  
@endsection
