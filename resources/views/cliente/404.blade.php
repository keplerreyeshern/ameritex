@extends('cliente.layout.cliente')
@section('titulo', 'Aviso de privacidad')
@section('contenido')

    <hr>
    <section class="hero is-medium is-bold">
        <div class="hero-body">
            <div class="container text-is-centered has-text-centered">
                <h1 class="title-404 is-size-3">
                    Lo sentimos! la pagina que estas buscando no existe.
                </h1>
            </div>
        </div>
    </section>

@endsection