@extends('cliente.layout.cliente')
@section('titulo', 'Áreas de práctica')
@section('contenido')


    @breadcrumb_basico
    @slot('links')
        <li>{{ __('messages.productos') }}</li>
    @endslot
    @endbreadcrumb_basico

    @jumbotron_titulo
    @slot('fondo')
        images/titulo/productos.jpg
    @endslot

    @slot('titulo')
        {{ __('messages.productos') }}
    @endslot
    @endjumbotron_titulo

    <!----sección ------>
    <div class="container -areas-de-practica">

        <div class="columns is-multiline ">

{{--            <div class="column is-one-quarter has-text-centered">--}}

{{--                <h1 class="title -azul">{{ __('messages.productos') }}</h1>--}}
{{--            </div>--}}
{{--            <div class="column is-three-quarters box">--}}
{{--                <p class="-definicion"> {!! $parrafos['productos-pdcto']->contenido  !!}--}}

{{--                    <section  class="section">--}}
{{--                        <div id="gallery-portfolio">--}}

{{--                            @foreach ($productos->imagenes_de_galeria as $imagen)--}}
{{--                                <div class="-gallery-item">--}}
{{--                                    <a href="#" @click.prevent="openModalImageGallery($event)">--}}
{{--                                        <figure class="image">--}}
{{--                                            <img src="{{ $imagen->url }}" alt="">--}}
{{--                                        </figure>--}}

{{--                                    </a>--}}
{{--                                </div>--}}
{{--                            @endforeach--}}

{{--                        </div>--}}
{{--                    </section>--}}
{{--            </div>--}}





{{--            <hr class="-producto-hr">--}}
            <a name="automotriz"></a>
            <div class="column is-one-quarter has-text-centered">
                <figure class="-centered image is-128x128 ">
                    <img src="/cliente/img/icono/automotriz.jpg">
                </figure>
                <h1 class="title -azul">{{ __('messages.automotriz') }}</h1>
            </div>
            <div class="column is-three-quarters">
                <p class="-definicion">{!! $parrafos['automotriz']->contenido  !!}</p>

                <section id="portfolio-section" class="section">
                    <div id="gallery-automotriz">

                        @foreach ($automotriz->imagenes_de_galeria as $imagen)
                            <div class="-gallery-item-a ">
                                <a href="#" @click.prevent="openModalImageGallery($event)">
                                    <figure class="image">
                                        <img src="{{ $imagen->url }}" alt="">
                                    </figure>

                                </a>
                            </div>
                        @endforeach

                    </div>
                </section>
                <div class="columns is-mobile is-centered">
                    <div class="column is-4 has-text-centered">
                        <img src="{{ asset('cliente/images/products/volkswagen.jpg') }}" class="image" alt="VolksWagen">
                    </div>
                    <div class="column is-4 has-text-centered">
                        <img src="{{ asset('cliente/images/products/generalmotors.jpg') }}" class="image" alt="General Motors">
                    </div>
                    <div class="column is-4 has-text-centered">
                        <img src="{{ asset('cliente/images/products/nissan.jpg') }}" class="image" alt="Nissan">
                    </div>
                </div>
            </div>


            <hr class="-producto-hr">
            <a name="deportivo"></a>

            <div class="column is-one-quarter has-text-centered">
                <figure class="-centered image is-128x128 ">
                    <img src="/cliente/img/icono/deportivo.jpg">
                </figure>
                <h1 class="title -azul">{{ __('messages.deportivo') }}</h1>
            </div>
            <div class="column is-three-quarters">
                <p class="-definicion">{!! $parrafos['deportivo']->contenido  !!}



                    <section  class="section" id="deportivo">
                        <div id="gallery-deportivo">

                            @foreach ($deportivo->imagenes_de_galeria as $imagen)
                                <div class="-gallery-item-d">
                                    <a href="#" @click.prevent="openModalImageGallery($event)">
                                        <figure class="image">
                                            <img src="{{ $imagen->url }}" alt="deportivo">
                                        </figure>

                                    </a>
                                </div>
                            @endforeach

                        </div>
                    </section>






            </div>

            <hr class="-producto-hr">

            <a name="ropa-intima"></a>
            <div class="column is-one-quarter has-text-centered">
                <figure class="-centered image is-128x128 ">
                    <img src="/cliente/img/icono/ropa-intima.jpg">
                </figure>
                <h1 class="title -azul">{{ __('messages.ropa-intima') }}</h1>
            </div>
            <div class="column is-three-quarters">
                <p class="-definicion">{!! $parrafos['ropa-intima']->contenido  !!}</p>

                <section id="portfolio-section" class="section">
                    <div id="gallery-intima">

                        @foreach ($intima->imagenes_de_galeria as $imagen)

                            <div class="-gallery-item-i">
                                <a href="#" @click.prevent="openModalImageGallery($event)">
                                    <figure class="image">
                                        <img src="{{ $imagen->url }}" alt="íntima">
                                    </figure>

                                </a>
                            </div>

                        @endforeach

                    </div>
                </section>






            </div>

            <hr class="-producto-hr">
            <a name="textiles-tecnicos"></a>
            <div class="column is-one-quarter has-text-centered">
                <figure class="-centered image is-128x128 ">
                    <img src="/cliente/img/icono/textiles-tecnicos.jpg">
                </figure>
                <h1 class="title -azul">{{ __('messages.textiles-tecnicos') }}</h1>
            </div>
            <div class="column is-three-quarters">
                <p class="-definicion">{!! $parrafos['deportivo']->contenido  !!}</p>


                <section id="portfolio-section" class="section">
                    <div id="gallery-tecnicos">

                        @foreach ($tecnicos->imagenes_de_galeria as $imagen)
                            <div class="-gallery-item-t ">
                                <a href="#" @click.prevent="openModalImageGallery($event)">
                                    <figure class="image">
                                        <img src="{{ $imagen->url }}" alt="">
                                    </figure>

                                </a>
                            </div>
                        @endforeach

                    </div>
                </section>

            </div>



            <hr class="-producto-hr">
        </div>
    </div>


    @include('cliente.parciales.llamanos')
@stop