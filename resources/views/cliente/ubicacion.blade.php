@extends('cliente.layout.cliente')
@section('titulo', 'Ubicación')
@section('contenido')



    @breadcrumb_basico
    @slot('links')
    <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('messages.ubicacion') }}</a></li>
    @endslot
    @endbreadcrumb_basico

    @fila_basico
    @slot('descripcion') <h4 class="title is-4">{{ __('messages.ubicacion') }}</h4>  @endslot
    @slot('contenido')

    <div class="content">
        <div class="column is-full">
            <img src="/images/ubicacion.jpg">

            <p><strong>Planta</strong> <br>Vía Morelos #66
                <br>Colonia Rústica Xalostoc
                <br>Ecatepec, Estado de México C.P. 55340

            <p><a href="http://maps.google.com.mx/maps/ms?ie=UTF8&hl=es&msa=0&msid=108279174445782060618.00048439f609e07108cd7&ll=19.477274,-99.119682&spn=0.204886,0.271568&z=12">Rutas en Google Maps</a>

                <p>E-mail: ventas@ameritex.com.mx
                <br>Teléfono: (55) 56 99 22 25
            </p>
        </div>
    </div>


    @endslot
    @endfila_basico

@stop