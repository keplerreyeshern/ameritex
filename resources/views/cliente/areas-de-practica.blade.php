@extends('cliente.layout.cliente')
@section('titulo', 'Áreas de práctica')
@section('contenido')
    <!----parallax ------>
    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                El servicio de <br>
                la más alta calidad
            </p>
        </div>
    @endslot
    @endparallax_basico

    <!----breadcrumb ------>
    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Áreas de práctica</a></li>
    @endslot
    @endbreadcrumb_basico

    <!----sección ------>
    <div class="container -areas-de-practica">

        <div class="columns is-multiline ">


            <div class="column is-one-quarter has-text-centered">
                <h1 class="title">Honorarios</h1>
            </div>
            <div class="column is-three-quarters box">
                <p>Nuestros honorarios los fijamos con flexibilidad, a fin de que se ajusten a las necesidades de nuestros clientes, por lo que éstos pueden ser fijados conforme a los siguientes esquemas:
                    <br><br>
                <ul class="-lista-margen">
                    <li><p>Tiempo invertido
                    <li><p>Iguala mensual
                    <li><p>Por asunto o trámite
                </ul>
                    <span id="corporativo">&nbsp;</span>
            </div>




            <div class="column is-one-quarter has-text-centered">
                <i class="fas fa-building icon is-large fa-3x -azul"></i>
                <h1 class="title">Corporativo</h1>
            </div>
            <div class="column is-three-quarters">
                <ul>
                    <li><p>Asesoría estratégica en la <strong>constitución o formación</strong> de sociedades mercantiles y civiles, asociaciones religiosas, instituciones de asistencia privada y otras.</p>

                    <li><p>Estudio, análisis y elaboración de <strong>estatutos sociales</strong> “ad-hoc” para cada caso particular, según las necesidades de nuestros clientes.

                    <li><p>Asesoría e implementación estratégica de <strong>movimientos corporativos</strong>, tales como transformaciones, fusiones, escisiones, reestructuras y adquisiciones de empresas.

                    <li><p>Asesoría estratégica en la celebración de <strong>Asambleas de socios y sesiones</strong> de los órganos de administración, así como en la elaboración de las
                            actas respectivas.

                    <li><p>Asesoría en el <strong>nombramiento</strong> de representantes legales, tales como Administradores, Gerentes, Apoderados y dependientes en general.

                    <li><p>Mantenimiento de <strong>libros corporativos</strong> de las empresas.

                    <li><p>Asesoría en la <strong>emisión de obligaciones</strong> y otros
                            títulos valor.

                    <li><p>Inscripción de sociedades y accionistas extranjeros ante el <strong>Registro Nacional de Inversiones Extranjeras</strong> y el cumplimiento de sus obligaciooiknes.

                    <li><p>Elaboración de <strong>contratos</strong> mercantiles, civiles y administrativos.


                    <li><p><strong>Ratificación y reconocimientos de firmas</strong> en contratos mercantiles.

                    <li><p>Elaboración de <strong>auditorías (diagnósticos)</strong> a las empresas para evaluar el grado de cumplimiento de sus obligaciones corporativas.

                    <li><p>Asesoría, coordinación y supervisión de <strong>servicios de fedatarios públicos</strong> para todos los fines anteriores, para lo cual contamos con apoyo de Notarios y Corredores Públicos.
                </ul>

                <span id="inmobiliario">&nbsp;</span>
            </div>
            
            
            <div class="column is-one-quarter has-text-centered">
                <i class="fas fa-home icon is-large fa-3x -azul"></i>
                <h1 class="title">Inmobiliario</h1>
            </div>
            <div class="column is-three-quarters">
                <ul>
                    <li><p>Asesoría y estrategia <strong>inmobiliaria</strong>.

                    <li><p>	Asesoría en la <strong>adquisición, enajenación y modificación</strong> de toda clase de inmuebles, independientemente de la vía.

                    <li><p>	Asesoría en la <strong>selección, diseño y constitución</strong> de garantías para el cumplimiento de obligaciones, tales como hipotecas, prendas y fianzas.

                    <li><p>	Asesoría en materia de <strong>fideicomisos</strong>.

                    <li><p>	Búsquedas de <strong>títulos y antecedentes registrales</strong> de bienes inmuebles.

                    <li><p>	Asesoría y elaboración de <strong>contratos</strong> para la adquisición de propiedades inmobiliarias en México.


                    <li><p>	Consultoría y asesoría en el <strong>cumplimiento de regulaciones</strong> relacionadas con terrenos, planificación urbanística y zonificación.

                    <li><p>	Realización de trámites para obtención de <strong>permisos y licencias</strong> de infraestructura.

                    <li><p>	Asesoría, coordinación y supervisión de servicios de <strong>fedatarios públicos</strong> para todos los fines anteriores, para lo cual contamos con el apoyo de
                            diversos Notarios Públicos.
                </ul>
                <span id="fiscal">&nbsp;</span>
            </div>


             <div class="column is-one-quarter has-text-centered">
                 <i class="fas fa-landmark  icon is-large fa-3x -azul"></i>
                 <h1 class="title">Fiscal</h1>
             </div>
             <div class="column is-three-quarters">
                 <ul>
                     <li><p>Emisión de <strong>opiniones</strong> respecto a la interpretación de las normas fiscales, o bien respecto a la descripción del régimen fiscal que resulta aplicable
                     a una operación en particular.

                     <li><p>Elaboramos <strong>diagnósticos</strong> sobre el nivel de cumplimiento de las obligaciones fiscales a que está sujeto un contribuyente.

                     <li><p>Contamos con una amplia experiencia en la recuperación de <strong>saldos a favor</strong> de impuestos federales y locales.

                     <li><p>Apoyamos a las empresas en la realización de <strong>trámites</strong> ante las autoridades fiscales como son la presentación de avisos de compensación, la autorización para disminuir pagos provisionales y la
                     obtención de oficios de confirmación de criterios.

                     <li><p>	Participamos activamente en los procesos de <strong>reestructura de Grupos empresariales</strong> que involucran fusiones, escisiones, traspaso de acciones
                     a costo fiscal, o bien liquidaciones de sociedades.

                     <li><p>Adicionalmente, programamos <strong>cursos y pláticas</strong> de actualización fiscal sobre los temas que nos son requeridos, así como respecto a las reformas
                     fiscales que se dan a conocer cada ejercicio.

                     <li><p><strong>Litigio</strong> en materia fiscal en contra de actos de autoridad como pueden ser la liquidación de créditos fiscales que derivan de visitas domiciliarias, la negativa a la devolución de saldos a favor, o bien el
                     embargo de cuentas bancarias, entre otros.

                 </ul>
                 <span id="seguridad-social">&nbsp;</span>
             </div>




             <div class="column is-one-quarter has-text-centered">
                <i class="fas fa-file-medical  icon is-large fa-3x -azul"></i>
                <h1 class="title">Seguridad Social</h1>
            </div>
            <div class="column is-three-quarters">
                <ul>
                    <li><p><strong>Asesoría</strong> en materia de seguro social.

                    <li><p>Elaboración de <strong>consultas</strong> relacionadas con el debido cumplimiento de obligaciones patronales ante autoridades como el IMSS e INFONAVIT, en aspectos relacionados con la debida integración del salario base de cotización para el pago de cuotas y aportaciones, procedencia de capitales constitutivos, sustitución patronal, debida clasificación y determinación anual de la y prima en el seguro de riesgos de trabajo, rectificaciones de clase y prima en este seguro.

                    <li><p>Diagnósticos sobre el área de nóminas, con el fin de identificar el nivel de cumplimiento de las disposiciones en esta materia, particularmente respecto a la integración salarial y presentación de
                            avisos al IMSS.


                    <li><p><strong>Diagnósticos</strong> en materia de riesgos de trabajo para determinar la debida clasificación de las empresas, elaboración y estudio de procedimientos para limitar los riesgos inherentes a su actividad y
                            disminuir la prima en este seguro.

                    <li><p>Análisis de los esquemas de <strong>subcontratación laboral</strong> y su legal aplicación.

                    <li><p>Atención y apoyo para el desahogo de cualquier <strong>acto de fiscalización</strong> notificado por autoridades como el IMSS e INFONAVIT. (visita domiciliaria, revisión de gabinete, revisión de dictamen, corrección.

                    <li><p>Elaboración, revisión y modificación de <strong>contratos individuales de trabajo o de servicios</strong>> dentro del marco legal que encierran las obligaciones en
                                materia de seguridad social.


                    <li><p>Elaboración, revisión y modificación de <strong>contratos de prestación de servicios profesionales o de servicios</strong> a la luz de las actuales reformas a
                            las Ley Federal del Trabajo.

                    <li><p><strong>Litigio</strong> en contra de actos emitidos por el Instituto Mexicano del Seguro Social (IMSS), Instituto del Fondo Nacional de la Vivienda para los Trabajadores (INFONAVIT), tales como la presentación de escritos de aclaración administrativa, escritos de desacuerdo, recurso de inconformidad y trámites de suspensión al procedimiento administrativo de ejecución (EMBARGOS), demanda de nulidad y el
                            juicio de amparo.

                </ul>
                <span id="laboral">&nbsp;</span>
            </div>


             <div class="column is-one-quarter has-text-centered">
                 <i class="icon is-large fab fa-3x -azul fas fa-briefcase"></i>
                 <h1 class="title">Laboral</h1>
             </div>
            <div class="column is-three-quarters">
                 <ul>
                     <li><p><strong>Asesoría y consultoría</strong> en materia laboral, en relación con cuestiones administrativas y de relaciones individuales de trabajo, incluyendo la elaboración y revisión de contratos individuales de trabajo, asesoría para la correcta elaboración de controles de asistencia, y en general la revisión y/o elaboración de toda la documentación de carácter
                             administrativo-laboral.

                     <li><p>Atención de toda clase de <strong>citatorios</strong>, en relación a quejas iniciadas en su contra por alguno de sus trabajadores o ex trabajadores ante la Procuraduría de la Defensa del Trabajo y/o cualquier otra
                             autoridad laboral.

                     <li><p><strong>Asesoría y apoyo legal</strong> en el área jurídico laboral en caso de que se requiera dar por terminada una relación laboral en forma individual o colectiva.

                     <li><p>Atención de los <strong>conflictos económicos, colectivos y de huelga</strong>.

                     <li><p>Asesoría y consultoría respecto a las <strong>políticas</strong> que se deban seguir en las relaciones colectivas de trabajo, en caso que las mismas ya existan o llegaran a existir en la empresa.

                     <li><p>Atención de toda clase de citas ante la Secretaría de Trabajo y Previsión Social, y ante cualquier otra autoridad del trabajo, respecto de <strong>emplazamientos a huelga</strong> que presente cualquier sindicato por firma o titularidad del Contrato Colectivo de Trabajo.

                     <li><p>Gestionar con la celebración de un <strong>Contrato Colectivo de Trabajo</strong> con algún sindicato.


                     <li><p>Atención de toda clase de <strong>emplazamientos</strong> por parte de la Secretaría de Trabajo y Previsión Social, Dirección de Inspección Federal del Trabajo y Dirección de Trabajo y Previsión Social, con el
                             propósito de evitar sanciones.

                     <li><p>Atención de toda clase de <strong>juicios</strong> ante las diferentes Juntas de Conciliación y Arbitraje, o cualquier otra autoridad del trabajo y el juicio de amparo.

                 </ul>
                <span id="administrativo-regulatorio">&nbsp;</span>
             </div>




             <div class="column is-one-quarter has-text-centered">
                 <i class="fas fa-donate  icon is-large fa-3x -azul"></i>
                 <h1 class="title">Administrativo (Regulatorio)</h1>
             </div>
             <div class="column is-three-quarters">
                 <ul>
                     <li><p><strong>Asesoría</strong> sobre el correcto cumplimiento de normas administrativas.

                     <li><p>Consultoría y participación en <strong>licitaciones públicas</strong>.

                     <li><p><strong>Contratación</strong> con entes del Gobierno Federal, Local o Municipal.

                     <li><p>Obtención de <strong>licencias, permisos, autorizaciones o concesiones</strong> ante la autoridad gubernamental que resulte competente (COFEPRIS, SEMARNAT,
                                etc.)

                     <li><p>Desarrollo, seguimiento y desahogo de <strong>visitas de verificación</strong> por parte de autoridades administrativas federales, estatales y/o municipales.



                     <li><p>Trámites y atención de procedimientos <strong>conciliatorios, de infracciones a la ley, de publicidad comparativa y competencia desleal, incluyendo el registro de contratos de adhesión</strong> ante la Procuraduría Federal del Consumidor
                                (PROFECO).

                     <li><p><strong>Trámites</strong> ante las distintas dependencias de gobierno, ya sea federal, local o municipal.

                     <li><p><strong>Responsabilidad</strong> de servidores públicos.

                     <li><p><strong>Litigio en materia administrativa</strong>, tales como la interposición del recurso de revisión, juicio de nulidad ante el Tribunal Federal de Justicia Fiscal y Administrativa o ante los Tribunales de lo Contencioso Administrativo estatales y el juicio de amparo.

                 </ul>
                 <span id="proteccion-de-datos">&nbsp;</span>
             </div>




             <div class="column is-one-quarter has-text-centered">

                 <i class="fas fa-hdd icon is-large fa-3x -azul"></i>
                 <h1 class="title">Protección de datos</h1>
             </div>
             <div class="column is-three-quarters">
                 <ul>
                     <li><p><strong>Asesoría</strong> sobre los alcances y aplicación de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y su Reglamento, en el contexto de la regulación sectorial aplicable, así como de la legislación en materia de protección al consumidor y en materia laboral.

                     <li><p>Elaboración, modificación y actualización de <strong>avisos de privacidad</strong> en sus diferentes modalidades.

                     <li><p>Elaboración de <strong>estrategias</strong> para la recolección del consentimiento de los titulares de los datos sobre los avisos de privacidad.
                     <li><p>Instrumentación de <strong>medidas compensatorias</strong> para dar a conocer los avisos de privacidad.

                     <li><p>Elaboración y revisión de <strong>lineamientos, políticas y controles</strong> en materia de protección de datos personales y asesoría para la aplicación de medidas sancionatorias, tanto laborales, civiles, como de orden penal, incluyendo el manejo de evidencias digitales.

                     <li><p>Elaboración de estrategias y de documentación para atender solicitudes del Instituto Nacional de Transparencia, Acceso a la Información y Protección de Datos Personales (INAI) para la atención y reporte de <strong>vulneraciones en materia de seguridad a datos personales</strong>.

                     <li><p>Asesoría sobre la integración y operación del <strong>Departamento de Protección de Datos, el Comité de Protección de Datos Personales</strong>.

                     <li><p>Elaboración, revisión y modificación de <strong>solicitudes de acceso, rectificación, cancelación y oposición</strong>, así como formulación de respuesta a las mismas.

                     <li><p>Elaboración de estrategia y de documentos legales para atender y solventar procedimientos de <strong>protección de derechos y de verificación</strong> por parte del INAI.

                     <li><p>Preparación de <strong>documentos de seguridad</strong>, inventarios de datos, análisis de riesgos y análisis de brecha.

                     <li><p>Revisión y/o elaboración de <strong>instrumentos de autorregulación</strong>.

                     <li><p>Negociación, elaboración, revisión y actualización de <strong>contratos</strong> en múltiples jurisdicciones para la gestión, transferencia y remisión de datos personales.

                     <li><p>Elaboración, revisión y actualización de <strong>cláusulas en materia de privacidad, protección de datos y seguridad de la información</strong> para contratos de prestación de servicios, outsourcing, administración de bases de datos, servicios de cómputo en la nube.

                     <li><p><strong>Revisiones y/o Auditorías externas</strong> en materia de protección de datos personales– avisos de privacidad, recolección del consentimiento y medidas compensatorias, contratos, políticas y lineamientos de seguridad.

                     <li><p> Co-participación en <strong>proyectos interdisciplinarios</strong> de la LFPDPPP implementación de un Sistema de Gestión de Seguridad de Datos Personales medidas de seguridad física, técnicas administrativas para la protección de


                     <li><p><strong>Capacitación y sensibilización</strong> del personal en torno a la LFPDPPP y su Reglamento.

                     <li><p><strong>Litigio</strong> administrativo y constitucional en contra de resoluciones del INAI respecto de procedimientos sancionatorios.

                 </ul>
             </div>

          </div>
    </div>


    @include('cliente.parciales.home.llamanos')
@stop