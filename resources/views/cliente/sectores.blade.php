
@extends('cliente.layout.cliente')
@section('titulo', 'Sectores')
@section('contenido')

    @parallax_basico
    @slot('urlImagen') {{ asset('base/img/jumbotron/comunicados.jpeg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Amplia experiencia<br>
                nacional e internacional
            </p>
        </div>
    @endslot
    @endparallax_basico

    @breadcrumb_basico
    @slot('links')
        <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Sectores</a></li>
    @endslot
    @endbreadcrumb_basico

    <div class="container -sectores">
        <div class="columns is-multiline ">

            <div class="column is-one-quarter has-text-centered">
                <h1 class="title">Sectores</h1>
            </div>
            <div class="column is-three-quarters">
                <p>Gozamos de amplia experiencia en la prestación de servicios legales a empresas nacionales e internacionales ubicadas en los siguientes sectores:

                <ul class="-lista-margen">
                    <li><p>Aviación
                    <li><p>Financiero
                    <li><p>Automotriz
                    <li><p>Alimentos
                    <li><p>Eléctrico y Electrónico
                    <li><p>Salud
                    <li><p>Tiendas departamentales
                    <li><p>Saborizantes y fragancias
                    <li><p>Autotransporte
                    <li><p>Manufactura
                    <li><p>Logística
                    <li><p>Educación
                    <li><p>Aseguradoras
                 </ul>
            </div>





        </div>
    </div>

    @include('cliente.parciales.home.llamanos')

@stop
