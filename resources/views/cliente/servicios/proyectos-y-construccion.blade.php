@extends('cliente.layout.cliente')
@section('titulo', 'Proyectos y construcción')
@section('contenido')

  {{--  Parallax  --}}
  @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/servicios/proyectos-y-construccion.jpg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                {{ __('messages.proyectos') }}
            </p>
        </div>
    @endslot
  @endparallax_basico

    {{--  Breadcrumb  --}}
    @breadcrumb_basico
        @slot('links')
            <li class="is-active"><a href="#" class="has-text-white" aria-current="page">{{ __('messages.proyectos') }}</a></li>
        @endslot
    @endbreadcrumb_basico

    {{--  Descripción  --}}
    <div class="section -pdcto-desc">
        <div class="container">
        <p>
            {{ __('messages.proyectosDesc') }}
        </p>
      </div>
    </div>



    {{--  sws  --}}
    <section class="section -pdcto-galeria">
      <div class="container">

          <h4 class="title is-3">{{ __('messages.galeria') }}</h4>

        <div class="columns">
              {{-- Mostramos 3 comunicados de la categoria --}}
              @foreach( $servicios as $servicio)
                  <div class="column is-4">
                      @comunicado_basico
                      @slot('urlFotoListado') {{ $servicio->Urlfotolistado }} @endslot
                      @slot('titulo') {{ $servicio->nombre }} @endslot
                      @slot('introduccion'){!!  substr(strip_tags($servicio->descripcion),0,100) !!} @endslot
                      @slot('urlDetalle') {{ $servicio->linkdetalle }} @endslot
                      @endcomunicado_basico
                  </div>
              @endforeach
          </div>
      </div>
    </section>

  
@endsection
