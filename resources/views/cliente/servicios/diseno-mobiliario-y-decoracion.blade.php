@extends('cliente.layout.cliente')
@section('titulo', 'Diseño mobiliario y decoración')
@section('contenido')

  {{--  Parallaz  --}}
  @parallax_basico
    @slot('urlImagen') {{ asset('cliente/img/servicios/diseno-mobiliario.jpg') }} @endslot
    @slot('texto')
        <div class="has-text-right">
            <p class="subtitle is-2 has-text-white">
                Diseño mobiliario y decoración
            </p>
        </div>
    @endslot
  @endparallax_basico

    {{--  Breadcrumb  --}}
    @breadcrumb_basico
        @slot('links')
            <li class="is-active"><a href="#" class="has-text-white" aria-current="page">Diseño mobiliario y decoración</a></li>
        @endslot
    @endbreadcrumb_basico

    {{--  Descripción  --}}
    <div class="section -pdcto-desc">
      <div class="container">
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit nostrum ducimus, earum ullam nulla provident magnam dolore nobis atque cupiditate nesciunt aliquid repudiandae eveniet perspiciatis aperiam est quis?
        </p>
      </div>
    </div>

    {{--  sws  --}}
    <section class="section -pdcto-galeria">
      <div class="container">
        <div class="columns">
              {{-- Mostramos 3 comunicados de la categoria --}}
              @foreach( $servicios as $servicio)
                  <div class="column is-4">
                      @comunicado_basico
                      @slot('urlFotoListado') {{ $servicio->Urlfotolistado }} @endslot
                      @slot('titulo') {{ $servicio->nombre }} @endslot
                      @slot('introduccion'){!!  substr(strip_tags($servicio->descripcion),0,100) !!} @endslot
                      @slot('urlDetalle') {{ $servicio->linkdetalle }} @endslot
                      @endcomunicado_basico
                  </div>
              @endforeach
          </div>
      </div>
    </section>

  
@endsection
