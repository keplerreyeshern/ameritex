@if (session('mensaje'))
    <article class="message is-{{ session('tipo') }}">
        <div class="message-body">
            {{ session('mensaje') }}
        </div>
    </article>
@endif