<!doctype html>
<html lang="es-Mx" class="has-navbar-fixed-top">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin powered by Excess</title>

    <link rel="stylesheet" href="{{ asset('adminn/css/admin.css') }}">
    <link rel="stylesheet" href="{{ asset('adminn/css/dropzone.css') }}">
    <link rel="stylesheet" href="{{ asset('adminn/css/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/dropzone.css">
</head>
<body>
    <div id="app">
        <notifications group="foo"></notifications>
        @include('admin.layout.navbar')
        @include('admin.layout.sidebar')
        <main>
            <section class="section">
                @include('admin.parciales.mensaje')
                @yield('contenido')
            </section>
        </main>
    </div>
{{--    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js"></script>--}}

    <script src="{{ asset('adminn/js/admin.js') }}"></script>
{{--    <script src="{{ asset('adminn/js/dropzone.js') }}"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js"></script>
    {{--<script src="{{ asset('adminn/js/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('adminn/js/tinymce/tinymce_editor.js') }}"></script>--}}
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.8.15/jquery-ui.min.js" integrity="sha256-UNE48xeZLFKg87YKcTtP3zHY2+N7J9JvTEssXDgYcKQ=" crossorigin="anonymous"></script>
    <script src="{{ asset('adminn/js/custom.js') }}"></script>
    <script src="https://cdn.tiny.cloud/1/xdkgem0ygosq716ckqlt2h0ou6k6ecy4ijm0829y71jn06nl/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
    <script>
        var editor_config = {
            path_absolute : "/",
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code fullscreen",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons template paste textcolor colorpicker textpattern"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media | forecolor backcolor",
            relative_urls: false,
            file_browser_callback : function(field_name, url, type, win) {
                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                // var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                if (type == 'image') {
                    cmsURL = cmsURL + "&type=Images";
                } else {
                    cmsURL = cmsURL + "&type=Files";
                }

                tinyMCE.activeEditor.windowManager.open({
                    file : cmsURL,
                    title : 'Filemanager',
                    width : x * 0.8,
                    height : y * 0.8,
                    resizable : "yes",
                    close_previous : "no"
                });
            }
        };

        tinymce.init(editor_config);
    </script>
</body>
</html>