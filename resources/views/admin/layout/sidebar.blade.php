<div id="admin-sidebar" class="box is-hidden-touch -fdo-sidebar">
    <aside class="menu">

        <div class="-btn-idioma">

            @if (App::isLocale('en'))
                <a class="button " href="{{ route('lang', ['locale' => 'es']) }}">
                    Administrar en Español
                </a>
            @elseif (App::isLocale('es'))

                <a class="button " href="{{ route('lang', ['locale' => 'en']) }}">
                    Administrar en Inglés
                </a>
            @endif
        </div>




        @if (Auth::user()->hasRole('Administrador'))
            <ul class="menu-list">
                <li class="list">
                    <a href="#" class="parent-menu" @click.prevent="showMenu"> Admin <span class="icon is-pulled-right"><i class="fas fa-caret-down"></i></span></a>
                    <ul class="is-marginless is-paddingless">
                        <li><a href="{{ route('admin.users.index') }}"> &nbsp; <span class="icon"><i class="far fa-user"></i></span> Usuarios</a></li>
                    </ul>
                </li>
            </ul>
        @endif





        <ul class="menu-list">
            <li class="list">
                <a href="#" class="parent-menu" @click.prevent="showMenu">Secciones <span class="icon is-pulled-right"><i class="fas fa-caret-down"></i></span></a>
                <ul class="is-marginless is-paddingless">
                    <li><a href="{{ route('admin.parrafos.index') }}"> <span class="icon"><i class="fas fa-clipboard-check"></i></span> Diccionario</a></li>
                <!-- <li><a href="{{ route('admin.secciones.model.create', ['modelo' => 'servicios'] ) }}"> &nbsp; <span class="icon"><i class="fas fa-clipboard-check"></i></span> Servicios</a></li>
                    <li> <a href="{{ route('admin.secciones.model.create', ['modelo' => 'quienes-somos'] ) }}"> &nbsp; <span class="icon"><i class="fas fa-user-tie"></i></span> Quiénes somos</a></li>-->
                </ul>
            </li>
        </ul>


        <ul class="menu-list">
            <li class="list">
                <a href="#" class="parent-menu" @click.prevent="showMenu">Misc <span class="icon is-pulled-right"><i class="fas fa-caret-down"></i></span></a>
                <ul class="is-marginless is-paddingless">
                    <li><a href={{ route('admin.promociones.index') }}> &nbsp; <span class="icon"><i class="far fa-star"></i></span> Promociones</a></li>
                    <li><a href={{ route('admin.calidad.index') }}> &nbsp; <span class="icon"><i class="fas fa-medal"></i></span> Certificado Calidad</a></li>
                </ul>
            </li>
        </ul>



        <ul class="menu-list">
            <li class="list">
                <a href="#" class="parent-menu" @click.prevent="showMenu">Galerías <span class="icon is-pulled-right"><i class="fas fa-caret-down"></i></span></a>
                <ul class="is-marginless is-paddingless">
                    <li><a href="{{ route('admin.galerias.index') }}"> &nbsp; <span class="icon"><i class="fas fa-camera-retro"></i></span>Imágenes</a></li>
                </ul>
            </li>
        </ul>
<!--



        <ul class="menu-list">
            <li class="list">
                <a href="#" class="parent-menu" @click.prevent="showMenu">Productos <span class="icon is-pulled-right"><i class="fas fa-caret-down"></i></span></a>
                <ul class="is-marginless is-paddingless">
                    <li><a href="{{ route('admin.productos.index') }}"> &nbsp; <span class="icon"><i class="fas fa-store"></i></span> Productos</a></li>
                    <li><a href="{{ route('admin.productos.categorias.index') }}"> &nbsp; <span class="icon"><i class="fas fa-tag"></i></span> Categorías</a></li>
                </ul>
            </li>
        </ul>
        -->
    </aside>
</div>