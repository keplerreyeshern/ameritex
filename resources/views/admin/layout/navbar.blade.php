<nav id="admin-navbar" class="navbar is-black is-fixed-top">
    <div class="navbar-brand">
        <button class="button navbar-burger is-marginless -is-transparent -is-borderless is-shadowless" @click="toggleSidebarMobile">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <a class="navbar-item" href="/admin">
            <img src="{{asset('adminn/img/logo.png')}}" alt="Bulma: a modern CSS framework based on Flexbox" width="auto" height="100%" style="max-height: 100%">
        </a>
    </div>

    <div id="" class="navbar-menu">

        <div class="navbar-end">
            <div class="navbar-item">
                <span>Hola {{ Auth::user()->nombre }} {{ Auth::user()->apellido }}</span>
            </div>
            <div class="navbar-item">
                <form action="{{ route('logout') }}" method="post">
                    {{ csrf_field() }}
                    <button class="button is-danger is-small is-rounded">
                        <span>Cerrar sesión</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</nav>
<div class="hero -barra-idioma @if (App::isLocale('en')) -azul @else -vde @endif  has-text-right ">
    @if (App::isLocale('en'))
        <h6 class="subtitle is-size-7 has-text-white">Estas editando la información en Idioma -> Inglés</h6>
    @elseif (App::isLocale('es'))
        <h6 class="subtitle is-size-6 has-text-white">Estas editando la información en Idioma -> Español</h6>
    @endif
</div>