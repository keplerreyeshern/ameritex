@extends('admin.layout.admin')
@section('contenido')
    <card>
        <p slot="title">Galerías</p>
        <div slot="content">
            <form id="create-new-gallery" action="{{ route('admin.galerias.store') }}" method="post" enctype="multipart/form-data">
                @csrf
                 <div class="columns is-multiline">
                     <div class="column is-6">
                         <form-input>
                             <label slot="label" class="label">Nombre</label>
                             <input id="gallery-name" slot="input" type="text" class="input" required>
                             <p slot="help" class="help is-danger is-invisible">El nombre es requerido</p>
                         </form-input>
                     </div>
                     <div class="column is-6">
                         <form-input>
                             <label slot="label" class="label">Fecha</label>
                             <input id="gallery-date" slot="input" type="text" class="input datepicker" required>
                             <p slot="help" class="help is-danger is-invisible">La fecha es requerida</p>
                         </form-input>
                     </div>
                     <div class="column is-6">
                         <form-input>
                             <label slot="label" class="label">Descripción</label>
                             <input id="gallery-description" slot="input" type="text" class="input" required>
                             <p slot="help" class="help is-danger is-invisible">La descripción es requerida</p>
                         </form-input>
                     </div>
                 </div>
                <hr>




                <div slot="footer" class="box is-radiusless card-footer-item has-text-right">
                    <button id="btn-submit-gallery" class="button is-link is-rounded">Crear</button>
                </div>
            </form>
        </div>

    </card>
@stop