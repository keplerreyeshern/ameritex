@extends('admin.layout.admin')
@section('contenido')
    <a href="{{ route('admin.galerias.create') }}" class="button is-info is-small is-rounded btn-create-model">Crear
        galería</a>
    <card class="-has-content-paddingless">
        <p slot="title">Galerías</p>
        <div slot="content">
            <table class="table is-fullwidth table is-bordered is-marginless -listado">
                <thead>
                <tr>
                    <th class="title is-6 -colFecha">Imagen</th>
                    <th class="title is-6">Nombre</th>
                    <th class="title is-6">Descripción</th>
                    <th class="title is-6 -actions">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($fotografias as $fotografia)
                    @foreach( $fotografia->galerias as $galeria)
                        <tr>
                            <td>
                                <a href="{{ route('admin.galerias.edit', ['id'=> $galeria->id]) }}"
                                   title="Editar">
                                    <figure class="image is-128x128 -overflow-hidden -has-margin-auto">
                                        <img src="{{ $galeria->fotoListado }}" class="-object-fit-cover -is-fullheight"
                                             alt="">
                                    </figure>
                                </a>
                            </td>
                            <td>
                                <a href="{{ route('admin.galerias.edit', ['id'=> $galeria->id]) }}"> {{ $galeria->nombre }}</a>
                            </td>
                            <td>{!! substr(strip_tags ($galeria->descripcion), 0, 100)  !!}</td>
                            <td class="has-text-centered -actions">
                                <options active="{{ $galeria->active }}"
                                         route_disable="{{ route('admin.galerias.desactivar', ['id' => $galeria->id]) }}"
                                         route_enable="{{ route('admin.galerias.activar', ['id' => $galeria->id]) }}"
                                         route_destroy="{{ route('admin.galerias.destroy', ['id' => $galeria->id]) }}"
                                         csrf_token="{{ csrf_token() }}"></options>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </card>
@stop