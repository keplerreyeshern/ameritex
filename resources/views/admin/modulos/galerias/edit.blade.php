@extends('admin.layout.admin')
@section('contenido')
    <card>
        <p slot="title">Galerías</p>
        <div slot="content">
            <form id="gallery-update" action="{{ route('admin.galeria.update', ['id' => $galeria->id] ) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @method('put')
                <div class="columns is-multiline">
                    <div class="column is-6">
                        <form-input>
                            <label slot="label" class="label">Nombre</label>
                            <input id="gallery-name" name="nombre" slot="input" type="text" class="input"
                                   value="{{ $galeria->nombre }}" required>
                            <p slot="help" class="help is-danger is-invisible">El nombre es requerido</p>
                        </form-input>
                    </div>
                    <div class="column is-6">
                        <form-input>
                            <label slot="label" class="label">Fecha</label>
                            <input id="gallery-date" name="fecha" slot="input" type="text" class="input datepicker"
                                   value="{{ $galeria->fechaFormateadaEdit }}" required>
                            <p slot="help" class="help is-danger is-invisible">La fecha es requerida</p>
                        </form-input>
                    </div>
                    <div class="column is-6">
                        <form-input>
                            <label slot="label" class="label">Descripción</label>
                            <input id="gallery-description" slot="input" name="descripcion" type="text" class="input"
                                   value="{{ $galeria->descripcion }}" required>
                            <p slot="help" class="help is-danger is-invisible">La descripción es requerida</p>
                        </form-input>
                    </div>
                </div>
            </form>
            <hr>

            <h4 class="title is-5">Galería</h4>
            <div class="box">
                @include('admin/parciales/mensaje-imagenes-galeria')
                <div class="columns is-multiline">
                    @foreach($galeria->imagenesDeGaleria as $imagen)
                        <div class="column is-2">
                            <figure class="image" style="height: 200px; overflow: hidden">
                                <img class="-object-fit-cover -is-fullheight" src="{{ $imagen['url'] }}" alt="">
                            </figure>
                            <div class="has-text-centered">
                                <a href="javascript:void(0)" @click.prevent="removeImageFromGallery($event, {{ $imagen['id'] }})">
                                    <span class="has-text-danger is-size-7">Eliminar</span>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <form id="my-awesome-dropzone" class="dropzone" method="post" action="{{ route('admin.gallery.image.add', ['id' => $galeria->id]) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
{{--                    <div class="fallback">--}}
{{--                        <input name="file" type="file"/>--}}
{{--                    </div>--}}
                    <div class="dz-message">Arrastra tus archivos para cargarlos</div>
                </form>
            </div>
        </div>
        <div slot="footer" class="box is-radiusless card-footer-item has-text-right">
            <button class="button is-link is-rounded" onclick="document.getElementById('gallery-update').submit()">Actualizar</button>
        </div>
    </card>
@stop