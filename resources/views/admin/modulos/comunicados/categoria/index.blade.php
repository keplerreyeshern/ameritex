@extends('admin.layout.admin')
@section('contenido')

    <a href="{{route('admin.comunicados.create',['cat_id'=>$categoria_comunicados->nombre])}}" class="button is-info is-small is-rounded btn-create-model">Crear comunicado</a>


    <form action="/admin/comunicados/noticias/" method="GET">
        <div class="field has-addons">
            <div class="control">
                <input class="input" type="text" name="srchTxt" placeholder="Buscar">
            </div>
            <div class="control">
                <button class="button is-info">
                    Buscar
                </button>
            </div>
        </div>
    </form>



    <card class="-has-content-paddingless -is-spaced">
        <p slot="title">{{ ucfirst($categoria_comunicados->nombre) }}</p>
        <div slot="content">



            <table class="table is-fullwidth table is-bordered is-marginless -listado">
                <thead>
                <tr>
                    <th class="title is-6 -colFecha">Imagen</th>
                    <th class="title is-6">Título</th>
                    <th class="title is-6">Introducción</th>
                    <th class="title is-6 -colFecha">Fecha</th>
                    <th class="title is-6 -actions">Acciones</th>

                </tr>
                </thead>

                <tbody>
                @foreach($comunicados as $comunicado)
                    <tr class="table ">
                        <td>
                            <a href="{{URL::route('admin.comunicados.edit', $params = array('id' => $comunicado->id))}}"
                               title="Editar">
                                <figure class="image is-128x128 -overflow-hidden -has-margin-auto">
                                    <img src="{{$comunicado->Urlfotolistado}}" class="-object-fit-cover -is-fullheight" alt="">
                                </figure>
                            </a>
                        </td>
                        <td>
                            <a href="{{URL::route('admin.comunicados.edit', $params = array('id' => $comunicado->id))}}"
                               title="Editar">{{$comunicado->titulo}}</a>
                            <br>
                            {{$comunicado->subtitulo}}
                        </td>
                        <td>{!! strip_tags ($comunicado->intro) !!}</td>

                        <td>{{$comunicado->fechaformateada}}</td>

                        <td class="has-text-centered ">
                            <options active="{{ $comunicado->active }}"
                                     route_disable="{{ route('admin.comunicados.desactivar', ['id' => $comunicado->id]) }}"
                                     route_enable="{{ route('admin.comunicados.activar', ['id' => $comunicado->id]) }}"
                                     route_destroy="{{ route('admin.comunicados.destroy', ['id' => $comunicado->id]) }}"
                                     csrf_token="{{ csrf_token() }}"></options>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </card>
    <br>
    <div class="columns is-centered">
        <div class="column box is-half">{{ $comunicados->appends(Request::except('page'))->links()  }}</div>
    </div>

@stop