@extends('admin.layout.admin')
@section('contenido')
    <card>
        <p slot="title">Crear comunicado ({{ $categoria }})</p>
        <div slot="content">
            @include('admin.parciales.errores')
            <form id="form-create-new-comunicado" action="{{ route('admin.comunicados.store') }}" method="post" enctype="multipart/form-data">
                    @include('admin.modulos.comunicados.categoria.form')
            </form>
        </div>
        <div slot="footer" class="box is-radiusless card-footer-item has-text-right">
            <button class="button is-link is-rounded"  onclick="document.getElementById('form-create-new-comunicado').submit()">Crear</button>
        </div>
    </card>
@stop