{{ csrf_field() }}
<input type="hidden" name="categoria_id" value="{{ $comunicado->categoria_id }}">
<div class="columns is-multiline">
    <div class="column is-6">
        <div class="field">
            <label class="label">Autor</label>
            <div class="control">
                <input class="input" name="autor" type="text" value="{{ $comunicado->autor }}">
            </div>
        </div>
    </div>
    <div class="column is-6">
        <div class="field">
            <label class="label" for="">Destacado</label>
            <div class="control">
                <label class="checkbox">
                    @if($comunicado->destacado)
                        <input name="destacado" type="checkbox" value="{{ $comunicado->destacado }}" checked>
                    @else
                        <input name="destacado" type="checkbox" value="{{ $comunicado->destacado }}">
                    @endif
                </label>
            </div>
        </div>
    </div>
    <div class="column is-6">
        <div class="field">
            <label class="label">Título</label>
            <div class="control">
                <input class="input" name="titulo" type="text" value="{{ $comunicado->titulo }}">
            </div>
        </div>
    </div>
    <div class="column is-6">
        <div class="field">
            <label class="label">Subtítulo</label>
            <div class="control">
                <input class="input" name="subtitulo" type="text" value="{{ $comunicado->subtitulo }}">
            </div>
        </div>
    </div>
    <div class="column is-6">
        <div class="field">
            <label class="label">Fecha</label>
            <div class="control">
                <input class="input datepicker" name="fecha" type="text" value="{{ $comunicado->fechaformateadaedit }}">
            </div>
        </div>
    </div>
</div>
<hr>
<div class="field">
    <label class="label">Introducción</label>
    <div class="control">
        <textarea name="intro" class="textarea" placeholder="Textarea">{!! $comunicado->intro !!}</textarea>
    </div>
</div>
<div class="field">
    <label class="label">Descripción</label>
    <div class="control">
        <textarea name="descripcion" class="textarea" placeholder="Textarea">{!! $comunicado->descripcion !!}</textarea>
    </div>
</div>
<hr>
@include('admin/parciales/mensaje-imagenes')
<div class="field">
    <label class="label"><i class="fas fa-eye"></i> Fotografía del listado @include('admin/parciales/tamano-imagenes')</label>
    <div class="file is-info is-small has-name">
        <label class="file-label">
            <input class="file-input" type="file" name="fotolistado" @change="uploadImageListado($event)">
            <span class="file-cta">
                <span class="file-icon">
                  <i class="fas fa-upload"></i>
                </span>
            </span>
            @if ($comunicado->fotolistado)
                <span v-if="!imageListado" class="file-name">
                    @{{ fileImageListadoName = 'Cambiar Imagen' }}
                </span>
                <span v-else="" class="file-name">
                    @{{ fileImageListadoName }}
                </span>
            @else
                <span class="file-name">
                    @{{ fileImageListadoName }}
                </span>
            @endif
        </label>
    </div>
</div>


<div class="columns">
    <div class="column is-6">
        <figure class="image">
            <img v-if="!imageListado" src="{{ asset('files/comunicados/'.$comunicado->fotolistado) }}" alt="">
            <img v-else :src="imageListado" alt="">
        </figure>
    </div>
</div>
<div class="field">
    <label class="label"><i class="fas fa-eye"></i> Fotografía del comunicado @include('admin/parciales/tamano-imagenes')</label>
    <div class="file is-info is-small has-name">
        <label class="file-label">
            <input class="file-input" type="file" name="fotocomunicado" @change="uploadImageComunicado($event)">
            <span class="file-cta">
                <span class="file-icon">
                  <i class="fas fa-upload"></i>
                </span>
            </span>
            @if ($comunicado->fotocomunicado)
                <span v-if="!imageListado" class="file-name">
                    @{{ fileImageComunicadoName = 'Cambiar Imagen' }}
                </span>
                <span v-else="" class="file-name">
                    @{{ fileImageComunicadoName }}
                </span>
            @else
                <span class="file-name">
                    @{{ fileImageComunicadoName }}
                </span>
            @endif
        </label>
    </div>
</div>






<div class="columns">
    <div class="column is-6">
        <figure class="image">
            <img v-if="!imageComunicado" src="{{ asset('files/comunicados/'.$comunicado->fotocomunicado) }}" alt="">
            <img v-else :src="imageComunicado" alt="">
        </figure>
    </div>
</div>