@extends('admin.layout.admin')
@section('contenido')
    <div class="card">
        <header class="card-header">
            <div class="card-header-title is-block">
                <span>Editar comunicado ( {{ $comunicado->categoria->nombre }} )</span>
            </div>
        </header>

        <div class="card-content">
            <div class="content">
                @if (count($errors) > 0)
                    <article class="message is-danger">
                        <div class="message-body">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </article>
                @endif
                <form id="formEditComunicado" action="{{ route('admin.comunicados.update', $comunicado->id) }}" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="_method" value="put">
                    @include('admin.modulos.comunicados.categoria.form')
                </form>
            </div>
        </div>

        <footer class="card-footer">
            <div class="box is-radiusless card-footer-item has-text-right">
                <button class="button is-link is-rounded"  onclick="document.getElementById('formEditComunicado').submit()">Actualizar</button>
            </div>
        </footer>

    </div>
@stop