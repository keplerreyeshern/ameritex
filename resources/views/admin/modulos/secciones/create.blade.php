@extends('admin.layout.admin')
@section('contenido')
    <card>
        <p slot="title">Sección {{ str_replace('-',' ', $modelo) }}</p>
        <div slot="content">
            @include('admin.parciales.errores')
            <form id="form-create-seccion" action="{{ route('admin.secciones.store') }}" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="modelo" value="{{ $modelo }}">
                <form-input>
                    <label slot="label" class="label">Contenido</label>
                    <textarea slot="input" name="contenido" class="textarea"> {{ $seccion->contenido }} </textarea>
                </form-input>
            </form>
        </div>
        <div slot="footer" class="box is-radiusless card-footer-item has-text-right">
            <button class="button is-link is-rounded"  onclick="document.getElementById('form-create-seccion').submit()">Guardar</button>
        </div>
    </card>
@stop