@extends('admin.layout.admin')
@section('contenido')
    <a href="{{route('admin.promociones.create')}}" class="button is-info is-small is-rounded btn-create-model">Crear
        Promoción</a>
    <card class="-has-content-paddingless">
        <p slot="title">Promociones</p>
        <div slot="content">
            <table class="table is-fullwidth table is-bordered is-marginless -listado">
                <thead>
                <tr>
                    <th class="title is-6 -colFecha">Imagen</th>
                    <th class="title is-6">Nombre</th>
                    <th class="title is-6 -colLiga">Liga</th>
                    <th class="title is-6">Contenido</th>
                    <th class="title is-6">Acciones</th>

                </tr>
                </thead>
                <tbody>
                @foreach($promociones as $promotion)
                    <tr>
                        <td>

                            <figure class="image is-128x128 -overflow-hidden -has-margin-auto">
                                @if($promotion->imagen)
                                    <img src="{{ asset($promotion->urlImagen) }}" class="-object-fit-cover -is-fullheight" alt="">
                                @else
                                    <img src="{{ asset('base/img/imagen-no-disponible.jpg') }}" class="-object-fit-cover -is-fullheight" alt="">
                                @endif
                            </figure>
                        </td>
                        <td>
                            <a href="{{ route('admin.promociones.edit', $promotion->id) }}">{{ $promotion->nombre }}</a>
                        </td>
                        <td>{{ $promotion->liga }}</td>
                        <td>{!! \Illuminate\Support\Str::words($promotion->contenido, 25) !!}</td>
                        <td class="has-text-centered -actions">
                            <options active="{{ $promotion->active }}"
                                     route_disable="{{ route('admin.promociones.desactivar', ['id' => $promotion->id]) }}"
                                     route_enable="{{ route('admin.promociones.activar', ['id' => $promotion->id]) }}"
                                     route_destroy="{{ route('admin.promociones.destroy', ['id' => $promotion->id]) }}"
                                     csrf_token="{{ csrf_token() }}"></options>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </card>
@stop