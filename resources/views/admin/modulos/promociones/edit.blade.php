@extends('admin.layout.admin')
@section('contenido')

    <form-promotion action="{{ route('admin.promociones.update', ['id' => $promocion->id]) }}"
                      csrf_token="{{ csrf_token() }}" promotion="{{ $promocion }}" method="put" >
    </form-promotion>

@endsection