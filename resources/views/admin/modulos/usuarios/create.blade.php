@extends('admin.layout.admin')
@section('contenido')
    <div class="columns is-centered">
        <div class="column is-6">
            <card>
                <p slot="title">Crear usuario</p>
                <div slot="content">
                    @include('admin.parciales.errores')
                    <form action="{{ route('admin.users.store') }}" method="post">
                        {{ csrf_field() }}
                        <form-input>
                            <label slot="label" class="label">Email</label>
                            <input slot="input" class="input" name="email" type="text" value="">
                        </form-input>
                        <form-input>
                            <label slot="label" class="label">Nombre</label>
                            <input slot="input" class="input" name="nombre" type="text" value="">
                        </form-input>
                        <form-input>
                            <label slot="label" class="label">Apellido</label>
                            <input slot="input" class="input" name="apellido" type="text" value="">
                        </form-input>
                        <div class="field">
                            <label class="label">Rol</label>
                            <div class="control">
                                <div class="select is-small">
                                    <select name="rol">
                                        @foreach($roles as $rol)
                                            <option value="{{ $rol->name }}">{{ $rol->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <form-input>
                            <label slot="label" class="label">Contraseña</label>
                            <input slot="input" class="input" name="password" type="password" value="">
                        </form-input>
                        <form-input>
                            <label slot="label" class="label">Repetir contraseña</label>
                            <input slot="input" class="input" name="password_confirmation" type="password" value="">
                        </form-input>
                        <hr>
                        <div class="has-text-right">
                            <button class="button is-link is-rounded is-small">Crear usuario</button>
                        </div>
                    </form>
                </div>
            </card>
        </div>
    </div>
@stop