@extends('admin.layout.admin')
@section('contenido')
    <a href="{{route('admin.users.create')}}" class="button is-info is-small is-rounded btn-create-model">Crear Usuario</a>
    <div class="card">
        <header class="card-header">
            <p class="card-header-title">
                Usuarios
            </p>
        </header>
        <div class="card-content is-paddingless">
            <table class="table table is-bordered is-marginless is-fullwidth">
                <thead>
                <tr>
                    <th class="title is-6">Nombre</th>
                    <th class="title is-6">Correo</th>
                    <th class="title is-6">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>
                            <a href="{{URL::route('admin.users.edit', $params = array('id' => $user->id))}}"
                               title="Editar">{{$user->nombre_completo}}</a>
                        </td>
                        <td>
                            {{$user->email}}
                        </td>
                        <td class="has-text-centered -actions">
                            <options active="{{ $user->active }}"
                                     route_disable="{{ route('admin.users.desactivar', ['id' => $user->id]) }}"
                                     route_enable="{{ route('admin.users.activar', ['id' => $user->id]) }}"
                                     route_destroy="{{ route('admin.users.destroy', ['id' => $user->id]) }}"
                                     csrf_token="{{ csrf_token() }}"></options>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@stop