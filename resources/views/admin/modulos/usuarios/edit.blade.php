@extends('admin.layout.admin')
@section('contenido')
    <div class="columns is-centered">
        <div class="column is-6">
            <card>
                <p slot="title">Crear usuario</p>
                <div slot="content">
                    @include('admin.parciales.errores')
                    <form action="{{ route('admin.users.update', ['id' => $user->id]) }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <form-input>
                            <label slot="label" class="label">Email</label>
                            <input slot="input" class="input" name="email" type="text" value="{{ $user->email }}">
                        </form-input>
                        <form-input>
                            <label slot="label" class="label">Nombre</label>
                            <input slot="input" class="input" name="nombre" type="text" value="{{ $user->nombre }}">
                        </form-input>
                        <form-input>
                            <label slot="label" class="label">Apellido</label>
                            <input slot="input" class="input" name="apellido" type="text" value="{{ $user->apellido }}">
                        </form-input>
                        <form-input>
                            <label slot="label" class="label">Contraseña</label>
                            <input slot="input" class="input" name="password" type="text">
                            <p slot="help" class="is-info help">Dejar en blanco si no desea cambiar la contraseña</p>
                        </form-input>
                        <div class="field">
                            <label class="label">Rol</label>
                            <div class="control">
                                <div class="select is-small">
                                    <select name="rol">
                                        @foreach($roles as $rol)
                                            <option value="{{ $rol->name }}" @if($user->hasRole($rol->name)) selected @endif>{{ $rol->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="has-text-right">
                            <button class="button is-link is-rounded is-small">Actualizar usuario</button>
                        </div>
                    </form>
                </div>
            </card>
        </div>
    </div>
@stop