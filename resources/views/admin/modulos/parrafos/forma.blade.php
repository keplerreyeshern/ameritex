@extends('admin.layout.admin')
@section('contenido')

    <template>
        <card>
            <p slot="title">Parrafo {{ $parrafo->accionTit }}</p>



            <div slot="content">



                @if($parrafo->method == 'PATCH')
                    <form  action="{{ $parrafo->link }}" method="post" enctype="multipart/form-data">
                        @else
                            <form  action="{{ route($parrafo->link) }}" method="post" enctype="multipart/form-data">
                                @endif

                                {{ csrf_field() }}
                                @if( $parrafo->method  == 'PATCH' )
                                    <input type="hidden" name="_method" value = "PATCH">
                                @endif
                                <input type="hidden" name="id" value="{{ $parrafo->id }}">


                                <!-- Categoria -->
                                <div class="select is-smal">

                                    <select  name="categoria_id">
                                        @foreach($categorias as $categoria)
                                            <option value="{{$categoria->id}}"  @if($categoria->id == $parrafo->categoria_id)  selected  @endif>{{$categoria->nombre}}</option>
                                        @endforeach
                                    </select>
                                    <br>
                                </div>


                               <!--referencia -->
                                <div class="columns">
                                    <div class="column is-6">
                                        <input-form>
                                            <label slot="label" class="label">Referencia única</label>
                                            <input  name="ref"  type="text" class="input" value="{{ $parrafo->ref }}">
                                        </input-form>
                                    </div>
                                </div>

                                <!-- Nombre  -->
                                <div class="columns">
                                    <div class="column is-6">
                                        <input-form>
                                            <label slot="label" class="label">Nombre</label>
                                            <input  name="titulo"  type="text" class="input" value="{{ $parrafo->titulo }}">


                                        </input-form>
                                    </div>
                                </div>
                                <!-- Descripción -->
                                <input-form >
                                    <label slot="label" class="label">Descripción</label>
                                    <textarea  name="contenido" class="textarea"> {{ $parrafo->contenido }} </textarea>

                                </input-form>


                                <div slot="footer" class="box is-radiusless card-footer-item has-text-right">
                                    <button class="button is-link is-rounded" >{{ $parrafo->accionTit }}</button>
                                </div>

                            </form>
            </div>

        </card>
    </template>


@endsection