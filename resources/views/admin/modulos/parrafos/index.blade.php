@extends('admin.layout.admin')
@section('contenido')
    <a href="{{ route('admin.parrafos.create') }}" class="button is-info is-small is-rounded btn-create-model">Crear Parrafo</a>




    <card class="-has-content-paddingless">
        <p slot="title">Parrafos </p>
        <div slot="content">
            <table class="table is-fullwidth table is-bordered is-marginless -listado">
                <thead>
                <tr>
                    <th class="title is-6" width="170">Categoría</th>
                    <th class="title is-6" width="250">Referencia</th>
                    <th class="title is-6">Titulo</th>
                    <th class="title is-6">Contenido</th>
                    <th class="title is-6">Acciones</th>

                </tr>
                </thead>


                <tbody>
                @foreach($parrafos as $parrafo)
                    <tr>
                        <td>{{ $parrafo->categoria_id  }} - {{ $parrafo->categoria->nombre }}</td>

                        <td>
                            <a href="{{ route('admin.parrafos.edit', $parrafo->id) }}">@if($parrafo->ref){{ $parrafo->ref }}@else editar @endif </a>
                        </td>

                        <td>
                            <a href="{{ route('admin.parrafos.edit', $parrafo->id) }}">@if($parrafo->titulo){{ $parrafo->titulo }}@else editar @endif </a>
                        </td>

                        <td>{!! \Illuminate\Support\Str::words($parrafo->contenido, 50) !!}</td>

                        <td class="has-text-centered -actions">

                            <a href="/admin/parrafos/destroy/{{ $parrafo->id }}" class=""><i class="fas fa-trash-alt is-size-3"></i></a>

                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </card>
@stop