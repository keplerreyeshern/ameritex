@extends('admin.layout.admin')
@section('contenido')
    <products-create action="{{ route('admin.productos.update', ['id' => $producto->id]) }}"
                     product="{{ $producto }}"
                     actiongallery="{{ route('admin.gallery.image.add', ['id' => $producto->id]) }}" token="{{ csrf_token() }}"></products-create>
@stop