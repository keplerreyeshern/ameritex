@extends('admin.layout.admin')
@section('contenido')
    <a href="{{ route('admin.productos.create') }}" class="button is-info is-small is-rounded btn-create-model">Crear producto</a>

    <form action="/admin/productos/" method="GET">
        <div class="field has-addons">
            <div class="control">
                <input class="input" type="text" name="srchTxt" placeholder="Buscar">
            </div>

            <div class="select">
                <select name="categoria">
                        <option value="0">Categoría</option>
                    @foreach($categorias as $categoria)
                        <option value="{{$categoria->id}}"  @if($cat == $categoria->id) selected="selected" @endif>{{$categoria->nombre}}</option>
                    @endforeach

                </select>
            </div>

            <div class="control">
                <button class="button is-info">
                    Buscar
                </button>
            </div>
        </div>
    </form>


    <card class="-has-content-paddingless -is-spaced">
        <p slot="title">Productos</p>
        <div slot="content">
            <table class="table is-fullwidth table is-bordered is-marginless -listado">
                <thead>
                <tr>
                    <th class="title is-6 -colFecha">Imagen</th>
                    <th class="title is-6">Nombre</th>
                    <th class="title is-6">Descripción</th>
                    <th class="title is-6 -actions">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach($productos as $producto)
                    <tr>
                        <td>
                            <a href="{{ route('admin.productos.edit', ['id'=> $producto->id]) }}"
                               title="Editar">
                                <figure class="image is-128x128 -overflow-hidden -has-margin-auto">
                                    <img src="{{ $producto->urlFotolistado }}" class="-object-fit-cover -is-fullheight" alt="">
                                </figure>
                            </a>
                        </td>
                        <td><a href="{{ route('admin.productos.edit', ['id'=> $producto->id]) }}"> {{ $producto->nombre }}</a></td>
                        <td>{!! substr(strip_tags ($producto->descripcion), 0, 100)  !!}</td>

                        <td>
                            <options active="{{ $producto->activo }}"
                                     route_disable="{{ route('admin.productos.desactivar', ['id' => $producto->id]) }}"
                                     route_enable="{{ route('admin.productos.activar', ['id' => $producto->id]) }}"
                                     route_destroy="{{ route('admin.productos.destroy', ['id' => $producto->id]) }}"
                                     csrf_token="{{ csrf_token() }}"></options>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </card>
    <br>
    <div class="columns is-centered">
        <div class="column box is-half">{{ $productos->appends(Request::except('page'))->links()  }}</div>
    </div>


@stop