@extends('admin.layout.admin')
@section('contenido')
    <card>
        <p slot="title">Categorías</p>
        <div slot="content">
            <div>
                <div class="tags has-addons">
                    <span class="tag is-info">
                        <a href="#" @click.prevent="addNewProductCategory">
                            <span class="icon has-text-white"><i class="fas fa-plus"></i></span>
                        </a>
                    </span>
                    <span class="tag">
                        <a href="#" class="has-text-grey-dark" @click.prevent="addNewProductCategory">
                            Agregar nueva categoría
                        </a>
                    </span>
                </div>

                <!-- Categorias -->
                <div id="container-products-categories" class="-is-spaced">
                    <div v-for="category in productsCategories">
                        <!-- Categoría padre -->
                        <div class="-level">
                            <span class="tags has-addons is-marginless">
                                <span class="tag is-info">
                                    <a href="#" title="Agregar subcategoría" @click.prevent="addNewProductSubcategory">
                                        <span class="icon has-text-white"><i class="fas fa-plus"></i></span>
                                    </a>
                                </span>
                                <span class="tag is-warning">
                                    <a href="#" title="Editar" @click.prevent="openFormEditCategory">
                                        <span class="icon has-text-white"><i class="fas fa-pencil-alt"></i></span>
                                    </a>
                                </span>
                                <span class="tag is-danger">
                                    <a href="#" title="Eliminar Categoría" @click.prevent="deleteNewProductSubcategory(category)">
                                        <span class="icon has-text-white"><i class="fas fa-trash"></i></span>
                                    </a>
                                </span>
                                <span class="tag">@{{ category.nombre }}</span>
                            </span>
                            <!-- input para crear nueva subcategoría-->
                            <div class="field has-addons is-hidden -field-new-subcategory">
                                <div class="control">
                                    <input name="input-new-subcategory" class="input is-small" type="text"
                                           placeholder="Subcategoría">
                                </div>
                                <div class="control">
                                    <a class="button is-success is-small"
                                       @click.prevent="saveNewProductSubcategory($event, category)">
                                        Guardar
                                    </a>
                                </div>
                                <div class="control">
                                    <a class="button is-danger is-small"
                                       @click.prevent="cancelAddNewProductSubcategory">
                                        Cancelar
                                    </a>
                                </div>
                            </div>
                            <!-- input para editar categoría-->
                            <div class="field has-addons is-hidden -field-edit-category">
                                <div class="control">
                                    <input name="input-new-subcategory" class="input is-small" type="text"
                                           v-model="category.nombre">
                                </div>
                                <div class="control">
                                    <a class="button is-success is-small"
                                       @click.prevent="updateCategory($event, category)">
                                        Guardar
                                    </a>
                                </div>
                                <div class="control">
                                    <a class="button is-danger is-small"
                                       @click.prevent="cancelEditCategory">
                                        Cancelar
                                    </a>
                                </div>
                            </div>
                            <subcategory-row :category="category"></subcategory-row>
                        </div> <!-- Termina categoría padre -->

                    </div>

                    <!-- input para crear nueva categoría-->
                    <div id="fieldAddNewProductCategory" class="field has-addons is-invisible">
                        <div class="control">
                            <input id="input-new-product-category" class="input is-small" type="text"
                                   placeholder="Categoría" v-model="newProductCategory">
                        </div>
                        <div class="control">
                            <a class="button is-success is-small" @click.prevent="saveNewProductCategory">
                                Guardar
                            </a>
                        </div>
                        <div class="control">
                            <a class="button is-danger is-small" @click.prevent="cancelAddNewProductCategory">
                                Cancelar
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </card>
@stop