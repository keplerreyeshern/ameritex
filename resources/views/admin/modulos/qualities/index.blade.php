@extends('admin.layout.admin')
@section('contenido')
    <a href="{{route('admin.calidad.create')}}" class="button is-info is-small is-rounded btn-create-model">Crear
        Certificado</a>
    <card class="-has-content-paddingless">
        <p slot="title">Certificado de Calidad</p>
        <div slot="content">
            <table class="table is-fullwidth table is-bordered is-marginless -listado">
                <thead>
                <tr>
                    <th class="title is-6 -colFecha">Imagen</th>
                    <th class="title is-6">Titulo</th>
                    <th class="title is-6">Contenido</th>
                    <th class="title is-6">Acciones</th>

                </tr>
                </thead>
                <tbody>
                @foreach($qualities as $quality)
                    <tr>
                        <td>

                            <figure class="image is-128x128 -overflow-hidden -has-margin-auto">
                                @if($quality->image)
                                    <img src="{{ asset($quality->urlImage) }}" class="-object-fit-cover -is-fullheight" alt="{{$quality->image}}">
                                @else
                                    <img src="{{ asset('base/img/imagen-no-disponible.jpg') }}" class="-object-fit-cover -is-fullheight" alt="No Disponible">
                                @endif
                            </figure>
                        </td>
                        <td width="150px">
                            <a href="{{ route('admin.calidad.edit', $quality->id) }}">{{ $quality->title }}</a>
                        </td>
                        <td>{!! \Illuminate\Support\Str::words($quality->content, 15) !!}</td>
                        <td class="has-text-centered -actions">
                            <options active="{{ $quality->active }}"
                                     route_disable="{{ route('admin.qualities.disable', ['id' => $quality->id]) }}"
                                     route_enable="{{ route('admin.qualities.enable', ['id' => $quality->id]) }}"
                                     route_destroy="{{ route('admin.calidad.destroy', ['id' => $quality->id]) }}"
                                     csrf_token="{{ csrf_token() }}"></options>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </card>
@stop