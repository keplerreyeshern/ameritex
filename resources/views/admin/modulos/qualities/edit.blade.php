@extends('admin.layout.admin')
@section('contenido')

    <form-quality action="{{ route('admin.calidad.update', ['id' => $quality->id]) }}"
                      csrf_token="{{ csrf_token() }}" quality="{{ $quality }}" method="put" >
    </form-quality>

@endsection