@extends('admin.layout.admin')
@section('contenido')
    <a href="{{ route('admin.servicios.create') }}" class="button is-info is-small is-rounded btn-create-model">Crear servicio</a>
    <card class="-has-content-paddingless">
        <p slot="title">Servicios</p>
        <div slot="content">
            <table class="table is-fullwidth table is-bordered is-marginless">
                <thead>
                <tr>
                    <th class="title is-6">Imagen</th>
                    <th class="title is-6">Título</th>
                    <th class="title is-6">Descripción</th>
                    <th class="title is-6">Acciones</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($servicios as $servicio)
                        <tr>
                            <td>
                                <a href="{{ route('admin.servicios.edit', ['id'=> $servicio->id]) }}"
                                   title="Editar">
                                    <figure class="image is-128x128 -overflow-hidden -has-margin-auto">
                                        <img src="{{ $servicio->urlImagen }}" class="-object-fit-cover -is-fullheight" alt="">
                                    </figure>
                                </a>
                            </td>
                            <td><a href="{{ route('admin.servicios.edit', ['id'=> $servicio->id]) }}"> {{ $servicio->titulo }}</a></td>
                            <td>{!! substr(strip_tags ($servicio->descripcion), 0, 100)  !!}</td>
                            <td class="has-text-centered -actions">
                                <options active="{{ $servicio->active }}"
                                         route_disable="{{ route('admin.servicios.desactivar', ['id' => $servicio->id]) }}"
                                         route_enable="{{ route('admin.servicios.activar', ['id' => $servicio->id]) }}"
                                         route_destroy="{{ route('admin.servicios.destroy', ['id' => $servicio->id]) }}"
                                         csrf_token="{{ csrf_token() }}"></options>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </card>
@stop