@extends('admin.layout.admin')
@section('contenido')
    <card>
        <p slot="title">Crear servicio</p>
        <div slot="content">
            @include('admin.parciales.errores')
            <form id="form-create-new-servicio" action="{{ route('admin.servicios.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="columns">
                    <div class="column is-6">
                        <form-input>
                            <label slot="label" class="label">Título</label>
                            <input name="titulo" slot="input" type="text" class="input">
                        </form-input>
                    </div>
                </div>
                <hr>
                <form-input>
                    <label slot="label" class="label">Descripción</label>
                    <textarea slot="input" name="descripcion" class="textarea" placeholder="Textarea"></textarea>
                </form-input>
                @include('admin/parciales/mensaje-imagenes')
                <label class="label">Fotografía del servicio</label>
                <div class="file is-info is-small has-name">
                    <label class="file-label">
                        <input class="file-input" type="file" name="imagen" @change="uploadImageServicio($event)">
                        <span class="file-cta">
                            <span class="file-icon">
                              <i class="fas fa-upload"></i>
                            </span>
                        </span>
                        <span class="file-name">
                            @{{ fileImageServicioName }}
                        </span>
                    </label>
                </div>
                <div class="columns">
                    <div class="column is-6">
                        <figure class="image">
                            <img :src="imageServicio" alt="">
                        </figure>
                    </div>
                </div>
            </form>
        </div>
        <div slot="footer" class="box is-radiusless card-footer-item has-text-right">
            <button class="button is-link is-rounded"  onclick="document.getElementById('form-create-new-servicio').submit()">Crear</button>
        </div>
    </card>
@stop