@component('mail::message')
# Mensaje de contacto

<ul>
    <li> <strong>Nombre: </strong>{{ $nombre }}</li>
    <li> <strong>Teléfono: </strong>{{ $numero }}</li>
    <li> <strong>Correo: </strong>{{ $email }}</li>
    <li> <strong>Mensaje: </strong>{{ $mensaje }}</li>
</ul>

{{ config('app.name') }}
@endcomponent
