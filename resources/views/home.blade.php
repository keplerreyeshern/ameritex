@extends('cliente.layout.cliente')
@section('titulo', 'Inicio')
@section('contenido')



        
    <!-- Inicia slider -->
    @slider_basico
        @slot('items')
            @foreach($promociones as $promocion)
                  <a href="{{ route('promociones.show', ['slug' => $promocion->slug]) }}">
                @if($promocion->liga && $promocion->liga != '')
                    <a href={{ $promocion->liga }}>
                @else
                    <a href="{{ route('promociones.show', ['slug' => $promocion->slug]) }}">
                @endif
                  <a href="@if($promocion->liga && $promocion->liga != '')  {{ $promocion->liga }} @else {{ route('promociones.show', ['slug' => $promocion->slug]) }} @endif ">
                    <div class="-carousel-item @if ($loop->first) -is-active @endif">
                        <img src="{{ $promocion->urlimagen }}" alt="">
                        <div class="box -is-transparent -text-slider -is-absolute is-radiusless is-shadowless animated slideInUp">
                                {{ $promocion->nombre }}
                        </div>
                    </div>
                </a>
            @endforeach
        @endslot
    @endslider_basico
    <!-- Termina slider otro cambio  -->


    @include('cliente.parciales.home.bienvenidos')

    @include('cliente.parciales.portafolio')

    @include('cliente.parciales.home.servicio')





@stop