<div class="card -is-transparent is-shadowless">
    @isset($urlDetalle)<a href="{{ $urlDetalle }}"> @endisset
        <div class="card-image">
            <figure class="image -is-200px-height -overflow-hidden">
                <img src="@isset($urlImagen){{ $urlImagen }}@endisset" class="-object-fit-cover -is-fullheight" alt="">
            </figure>
        </div>
    @isset($urlDetalle) </a> @endisset
    <div class="card-content is-paddingless -is-spaced">
        @isset($titulo)
        <div class="media">
            <div class="media-content">
                <p class="title is-4">
                    {{ $titulo }}
                </p>
            </div>
        </div>
        @endisset
        <div class="content">
            <p>
                @isset($introduccion)
                    {{ $introduccion }}
                @endisset
            </p>
        </div>
        @isset($urlDetalle)
        <div>
            <a href="{{ $urlDetalle }}" class="title is-5 is-pulled-right"> Ver más</a>
        </div>
        @endisset
    </div>
</div>