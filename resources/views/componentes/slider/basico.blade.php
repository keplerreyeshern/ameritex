<div class="-carousel">
    <a @click.prevent="previousSlide" href="#">
        <div class="-navigation">
            <span class="icon is-large has-text-white">
                <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </span>
        </div>
    </a>
    <div class="-carousel-container -is-relative">
      <!--<div class="is-overlay -is-overlay-carousel"></div>-->
        {{ $items }}
    </div>
    <a @click.prevent="nextSlide" href="#">
        <div class="-navigation -is-right">
            <span class="icon is-large has-text-white">
                <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </span>
        </div>
    </a>
</div>