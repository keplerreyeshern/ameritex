<div class="column -slide is-4">
    <a href="@isset($liga){{ $liga }}@endisset">
        <figure class="image -is-200px-height -overflow-hidden">
            <img class="-object-fit-cover -is-fullheight" src="@isset($urlImagen) {{ $urlImagen }}@endisset" alt="">
        </figure>
        <h5 class="title is-5 -is-spaced">
            @isset($titulo)
                {{ $titulo }}
            @endisset
        </h5>
    </a>
</div>