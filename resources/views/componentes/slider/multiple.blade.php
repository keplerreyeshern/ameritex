<div class="columns -is-relative -multiple-slider -overflow-hidden">
    <a @click.prevent="showPrevMutipleSlide" href="#"
       class="-is-absolute -has-bg-primary -navigation -is-left is-hidden-mobile -prev">
        <span class="icon is-large has-text-white">
            <i class="fa fa-chevron-left" aria-hidden="true"></i>
        </span>
    </a>
    @isset($slot)
        {{ $slot }}
    @endisset
    <a @click.prevent="showNextMutipleSlide" href="#"
       class="-is-absolute -has-bg-primary -navigation -is-right is-hidden-mobile -next">
        <span class="icon is-large has-text-white">
            <i class="fa fa-chevron-right" aria-hidden="true"></i>
        </span>
    </a>
</div>