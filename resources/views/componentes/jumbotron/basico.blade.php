

<section class="hero "  style='background-image: url( "{{$fondo}}" ); background-size: 100%;'>
    <div class="hero-body">
        <div class="container">
        </div>
    </div>
</section>

<section class="hero -titulo -base-azul">
    <div class="container has-text-right">
        @isset($titulo)
            <h2 class="is-size-3 -azul">
                {{ $titulo }}
            </h2>
        @endisset

    </div>

</section>
