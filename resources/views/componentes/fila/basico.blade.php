<section class="section -base-azul">
    <div class="container ">
        <div class="columns ">
            <div class="column is-3">
                @isset($descripcion)
                    {{ $descripcion }}
                @endisset
            </div>
            <div class="column">
                @isset($contenido)
                    {{ $contenido }}
                @endisset
            </div>
        </div>
    </div>
</section>