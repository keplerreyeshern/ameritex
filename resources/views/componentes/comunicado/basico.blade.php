

<div class="card">

        <div class="-is-fullheight">
            <figure class="image -is-200px-height -overflow-hidden">
                <a href="@isset($linkFotoListado){{ $linkFotoListado }} @endisset" ><img src="@isset($urlFotoListado){{ $urlFotoListado }}@endisset" class="-object-fit-cover "></a>
            </figure>
        </div>

    <div class="card-content is-paddingless">

        @isset($titulo)
            <div class="media">
                <div class="media-content">
                    <a href="@isset($linkFotoListado){{ $linkFotoListado }} @endisset" >
                        <p class="subtitle is-5 has-text-left">
                            {{ str_limit($titulo, 45, $end = '...') }}
                        </p>
                    </a>
                </div>
            </div>
        @endisset


        @isset($introduccion)
        <div class="content">
            <p>
                {{ str_limit($introduccion, 70, $end = '...') }}
            </p>
        </div>
        @endisset

        @isset($urlDetalle)
                <footer class="card-footer">

                        <a href="{{ $urlDetalle }}" class="subtitle is-6 has-text-right"> {{ __('messages.verMas') }} <i class="fas fa-arrow-right"></i></a>

                </footer>
        @endisset



    </div>





</div>