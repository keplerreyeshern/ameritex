<div class="-breadcrumb-box -has-bg-primary is-radiusless">
    <nav class="breadcrumb" aria-label="breadcrumbs">
        <ul>
            <li><a href="/" class="has-text-white">{{ __('Home') }}</a></li>
            @isset($links)
                {{ $links }}
            @endisset
        </ul>
    </nav>
</div>