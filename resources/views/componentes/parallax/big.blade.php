<section class="hero -parallax" style="background-image: url('@isset($urlImagen) {{ $urlImagen }} @endisset')">
    <div class="is-overlay"></div>
    <section class="hero is-large">
        <div class="hero-body">
            <div class="container has-text-centered">
                @isset($title)
                    {{ $title }}
                @endisset
            </div>
            <div class="container has-text-left">
                @isset($description)
                    <span class="has-text-white">{{ $description }}</span>
                @endisset
            </div>
        </div>
    </section>
</section>