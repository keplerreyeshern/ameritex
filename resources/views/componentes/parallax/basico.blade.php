<section class="hero -parallax" style="background-image: url('@isset($urlImagen) {{ $urlImagen }} @endisset')">
    <div class="is-overlay"></div>
    <div class="hero-body">
        <div class="container has-text-centered">
            @isset($texto)
                {{ $texto }}
            @endisset
        </div>
    </div>
</section>