<?php


return [

    'hello' => 'bienvenido a la aplicación',


    /*
     |--------------------------------------------------------------------------
     | titulos
     |--------------------------------------------------------------------------
     |
     | textos de títulos
     |
     */

    /* Menú */
    'quienes-somos' => 'Quienes somos',
    'productos' => 'Productos',
    'calidad' => 'Calidad',
    'tecnologia' => 'Tecnología',
    'diseno' => 'Diseño / R&D',
    'comunicados' => 'Comunicados',
    'proveedores' => 'Proveedores',
    'sustentabilidad' => 'Sustentabilidad',
    'contacto' => 'Contacto',

    'servicio' => 'Servicio',




    /*
     |--------------------------------------------------------------------------
     | Sub titulos
     |--------------------------------------------------------------------------
     |
     | textos de sub títulos
     |
     */

    'comunicados-detalle-titulo' => 'Entérate de las noticias<br> de último momento',
    'comunicados-detalle-subtitulo' => 'Te mantenemos informado',


    /*
       |--------------------------------------------------------------------------
       | home /  Bienvenidos a American Innovation Industry
       |--------------------------------------------------------------------------
       |
       | textos de home
       |
       */

    'bienvenidos-a-American' => 'Bienvenidos a American Innovation Industry',
    'fundada' => 'Fundada en 1941, American Specialized Textiles, S.A. de C.V. es una empresa mexicana.<br> Ubicada en el área metropolitana en la ciudad de México, ocupa una extensión de 36,000 m2.',


    'deportivo' => 'Deportivo',
    'deportivo-intro' => 'Telas especialmente diseñadas para la producción de prendas de alto rendimineto.',
    'ropa-intima' => 'Ropa Intima',
    'ropa-intima-intro' => 'Atendemos exitosamente a las principales marcas de brassier, faja y pantaletas para dama',
    'textiles-tecnicos' => 'Textiles técnicos',
    'textiles-tecnicos-intro' => 'Nuestras telas tienen aplicación en la producción de colchas, salas, zapatos deportivos y de vestir',
    'automotriz' => 'Automotriz',
    'automotriz-intro' => 'Somos una empresa integrada para diseñar, desarrollar y proveer textiles y compuestos laminados',


    /*
          |--------------------------------------------------------------------------
          | Galería
          |--------------------------------------------------------------------------
          |
          | textos de galeria
          |
          */


    'galeria' => 'Galería',

    /*
           |--------------------------------------------------------------------------
           | Componente /  Llamanos
           |--------------------------------------------------------------------------
           |
           | textos de home
           |
           */

    'llamanos'   => 'Llámanos',


    /*
              |--------------------------------------------------------------------------
              | comunicados
              |--------------------------------------------------------------------------
              |
              | textos del area de comunicados y detalle
              |
              */

    'publicado-por' => 'Publicado por',





    'terminos' => 'Términos y condiciones',

    'ubicacion' => 'Ubicación',


    /*
    |--------------------------------------------------------------------------
    | generales
    |--------------------------------------------------------------------------
    |
    | textos generales
    |
    */


    'telefonos' => 'Teléfonos',

    'galeria' => 'Galería',
    'verMas' => 'Ver más',
    'descripcion' => 'Descripción',
    'detalles' => 'Detalles',
    'enviar' => 'Enviar',

    'nombre' => 'Nombre',
    'tel' => 'Teléfono',
    'mail' => 'Correo',
    'mensaje' => 'Mensaje',


    'conocer-mas' => 'Conocer mas',



    'lorem' => 'orem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los .',

    /*
    |--------------------------------------------------------------------------
    | provvedores
    |--------------------------------------------------------------------------
    |
    | textos generales
    |
    */


    'material-directo' => 'Material Directo',
    'material-indirecto' => 'Material indirecto',

    /*
     |--------------------------------------------------------------------------
     | forma de contacto
     |--------------------------------------------------------------------------
     |
     | textos de fomra de contacto
     |
     */

    'contactoEstamos' => 'Estamos para servirle',
    'contactoEstamosDesc' => 'En American Specialized Textiles, S.A. de C.V. estamos para servirle, envíenos sus comentarios y en breve uno de nuestros representantes se pondrá en contacto con usted.',

    'contactoPrivacidad' => 'Puede revisar nuestro ',

    /*
    |--------------------------------------------------------------------------
    | footer
    |--------------------------------------------------------------------------
    |
    | textos del pie de pagina
    |
    */




    'ubicacion' => 'Ubicación',

    'siguenos' => 'Síguenos',


    'dir'       => ' Carretera San Miguel Qro. Km 2.5 <br>San Miguel de Allende, Guanajuato',

    'aviso'     => 'Aviso de privacidad',

    'copy'      =>  'American Specialized Textiles 2019 todos los derechos reservados'
];