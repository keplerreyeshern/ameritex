<?php


return [
  'hello' => 'welcome to the app',

    /*
    |--------------------------------------------------------------------------
    | titulos
    |--------------------------------------------------------------------------
    |
    | textos de títulos
    |
    */
    /* Menú*/
    'quienes-somos' => 'About us',
    'productos' => 'Products',
    'calidad' => 'Quality',
    'tecnologia' => 'Tecnology',
    'diseno' => 'Design / R&D',
    'comunicados' => 'en - Comunicados',
    'proveedores' => 'Suppliers',
    'sustentabilidad' => 'Sustainability',
    'contacto' => 'Contact',

    'servicio' => 'Service',


    /*
     |--------------------------------------------------------------------------
     | Sub titulos
     |--------------------------------------------------------------------------
     |
     | textos de sub títulos
     |
     */

    'comunicados-detalle-titulo' => '-En-Entérate de las noticias<br> de último momento',
    'comunicados-detalle-subtitulo' => '-en-Te mantenemos informado',


    /*
      |--------------------------------------------------------------------------
      | home /  Bienvenidos a American Innovation Industry
      |--------------------------------------------------------------------------
      |
      | textos de home
      |
      */

    'bienvenidos-a-American' => 'en-Bienvenidos a American Innovation Industry',
    'fundada' => 'en-Fundada en 1941, American Specialized Textiles, S.A. de C.V. es una empresa mexicana.<br> Ubicada en el área metropolitana en la ciudad de México, ocupa una extensión de 36,000 m2.',


    'deportivo' => 'Sportwear',
    'deportivo-intro' => '-en-Telas especialmente diseñadas para la producción de prendas de alto rendimineto.',
    'ropa-intima' => 'Intimate apparel',
    'ropa-intima-intro' => '-en-Atendemos exitosamente a las principales marcas de brassier, faja y pantaletas para dama',
    'textiles-tecnicos' => 'Technical textiles',
    'textiles-tecnicos-intro' => '-en-Nuestras telas tienen aplicación en la producción de colchas, salas, zapatos deportivos y de vestir',
    'automotriz' => 'Automotive',
    'automotriz-intro' => '-en-Somos una empresa integrada para diseñar, desarrollar y proveer textiles y compuestos laminados',

    /*
              |--------------------------------------------------------------------------
              | Galería
              |--------------------------------------------------------------------------
              |
              | textos de galeria
              |
              */


    'galeria' => 'Gallery',

    /*
             |--------------------------------------------------------------------------
             | comunicados
             |--------------------------------------------------------------------------
             |
             | textos del area de comunicados y detalle
             |
             */

    'publicado-por' => '-en-Publicado por',

    'terminos' => 'Términos y condiciones',

    'ubicacion' => 'Ubicación',


    /*
   |--------------------------------------------------------------------------
   | provvedores
   |--------------------------------------------------------------------------
   |
   | textos generales
   |
   */


    'material-directo' => 'Direct Material',
    'material-indirecto' => 'Indirect material',


    'telefonos' => 'Phone',

    'galeria' => 'Gallery',
    'verMas' => 'More',
    'descripcion' => 'Description',
    'detalles' => 'Details',
    'enviar' => 'Send',

    'nombre' => 'Name',
    'tel' => 'Phone',
    'mail' => 'Mail',
    'mensaje' => 'Mensage',


    'conocer-mas' => 'More info',



    'lorem' => 'orem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los .',


    /*
           |--------------------------------------------------------------------------
           | Componente /  Llamanos
           |--------------------------------------------------------------------------
           |
           | textos de home
           |
           */

    'llamanos'   => 'Contact us',





    'mejorOp'   => 'Mejor opción',
    'paraNuestros' => 'para nuestros clientes',
    'solidez' => 'solidez y calidad',

    'proyectos' => 'Proyectos y construcción',
    'mantenimiento' => 'Mantenimiento, residencial',



    'noticiasTit1' => 'eng: Entérate de las noticias',
    'noticiasTit2' => 'de último momento',

    'contactoTit' => 'eng: Te mantenemos informado',
    'contactoEstamos' => 'eng: Estamos para servirte',
    'contactoEstamosDesc' => 'eng: descripcion',

    'contactoPrivacidad' => 'eng: Al enviar este formulario acepto que MYSA Abogados pueda procesar mis datos de la forma indicada anteriormente y tal como se describe en la declaración de',



    'terminos' => 'Legal terms.',

    'ubicacion' => 'Location',


    'proyectosDesc' => 'Descripción y principales valores de nuestros proyectos de construcción',
    'mantenimientoDesc' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit nostrum ducimus, earum ullam nulla provident magnam dolore nobis atque cupiditate nesciunt aliquid repudiandae eveniet perspiciatis aperiam est quis?',

    /*
    |--------------------------------------------------------------------------
    | generales
    |--------------------------------------------------------------------------
    |
    | textos generales
    |
    */

    'telefonos' => '-en-Teléfonos',

    'galeria' => 'Galery',
    'verMas' => 'See more',
    'descripcion' => 'Description',
    'detalles' => 'Details',
    'enviar' => 'Send',

    'nombre' => 'Name',
    'tel' => 'Phone',
    'mail' => 'Email',
    'mensaje' => 'Message',

    'lorem' => 'orem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una galería de textos y los mezcló .',

    /*
    |--------------------------------------------------------------------------
    | materiales
    |--------------------------------------------------------------------------
    |
    | textos generales
    |
    */


    'lajas' => 'Slabs',
    'lajasDesc' => 'en: Lajas introducción Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit nostrum ducimus, earum ullam nulla provident magnam dolore nobis atque cupiditate nesciunt aliquid repudiandae eveniet perspiciatis aperiam est quis?',

    'basalto' => 'Basalt',
    'basaltoDesc' => 'en: Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',

    'cantera' => 'Quarry',
    'canteraDesc' => 'en: Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',

    'marmol' => 'Marble',
    'marmolDesc' => 'en: Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',

    'granito' => 'Granite',
    'granitoDesc' => 'en: Granite Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',

    'onyx' => 'Onyx',
    'onyxDesc' => 'en: Onyx Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',

    'recinto' => 'Enclosure',
    'recintoDesc' => 'en: Enclosure Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',

    'silestone' => 'Silestone',
    'silestoneDesc' => 'en: Silestone Lorem ipsum dolor sit amet consectetur adipisicing elit. Quod, dolorem sit n',


    /*
     |--------------------------------------------------------------------------
     | forma de contacto
     |--------------------------------------------------------------------------
     |
     | textos de fomra de contacto
     |
     */

    'contactoEstamos' => 'We are here for you',
    'contactoEstamosDesc' => 'In American Specialized Textiles we are your service, please send your comments and one representative of our team will be in touch with you as soon as posible..',

    'contactoPrivacidad' => 'Please check our ',

    /*
     |--------------------------------------------------------------------------
     | footer
     |--------------------------------------------------------------------------
     |
     | textos del pie de pagina
     |
     */



    'ubicacion' => 'Location',


    'siguenos' => 'Follow us',

    'dir'       => 'Carretera San Miguel Qro. Km 2.5 <br>San Miguel de Allende, Guanajuato',

    'aviso'     => 'Privacy Statement',

    'copy'      =>  'American Specialized Textiles  2019 todos los derechos reservados'
];