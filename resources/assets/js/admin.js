
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Notifications from 'vue-notification'
Vue.use(Notifications)

import VeeValidate from 'vee-validate';
// Vue.use(VeeValidate);
import es from 'vee-validate/dist/locale/es';
Vue.use(VeeValidate, {
    locale: 'es',
    dictionary: {
        es
    }
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('form-input', require('./components/Input.vue'));
Vue.component('card', require('./components/Card.vue'));

Vue.component('subcategory-row', require('./components/SubcategoryRow'));
// Vue.component('products-categories', require('./components/admin/modulos/productos/Categories'));
Vue.component('products-create', require('./components/admin/modulos/productos/Create'));

Vue.component('form-promotion', require('./components/admin/modulos/promociones/FormPromotion'));
Vue.component('form-quality', require('./components/admin/modulos/qualities/FormQuality'));

Vue.component('options', require('./components/admin/general/Options'));

const app = new Vue({
    el: '#app',
    created() {
        this.getProductsCategories();
    },
    data: {
        // Promociones
        //Create
        /*promotionType: 0, // Tipo de promoción (url o contenido) al agregar una nueva promoción
        fileImageName: 'No se ha seleccionado imagen', // Nombre de la imagen
        imagePromotion: false,*/

        //Blog
        fileImageListadoName: 'No se ha seleccionado imagen',
        imageListado: false,
        fileImageComunicadoName: 'No se ha seleccionado imagen',
        imageComunicado: false,

        // Servicios
        fileImageServicioName: 'No se ha seleccionado imagen',
        imageServicio: false,

        // Productos
        //Categorías de productos
        productsCategories: [
            /*{nombre: 'Categoría 1'},
            {nombre: 'Categoría 2'},*/
        ],
        newProductCategory: '',
        // inputNewProductCategory: false
    },
    methods: {
        showMenu(event) {  /*Función que activa/desactiva/oculta/muestra los menús del sideNav */
            let menuParent = event.target; //El elemento que fue clickeado
            let menu = menuParent.closest(".list"); //El li padre del elemento clickeado
            if (menu.classList.contains('is-active') && !menu.classList.contains('hide-menu')) {
                menu.classList.add('hide-menu');
            }
            else if (menu.classList.contains('hide-menu')) {
                menu.classList.remove('hide-menu');
            }
            else {
                if (menu.classList.contains('show-menu')) {
                    menu.classList.remove('show-menu');
                }
                else {
                    menu.classList.add('show-menu');
                }
            }
        },
        uploadImage (e) {
            const files = e.target.files || e.dataTransfer.files;

            if (!files.length)
                return;

            const image = files[0];
            this.fileImageName = image.name;
            this.createImage(files[0]);
        },
        createImage(file) {
            const image = new Image();
            const reader = new FileReader();

            reader.onload = (e) => {
                this.imagePromotion = e.target.result;
            };
            reader.readAsDataURL(file);
        },

        // Blog
        uploadImageListado (e) {
            const files = e.target.files || e.dataTransfer.files;

            if (!files.length)
                return;

            const image = files[0];
            this.fileImageListadoName = image.name;
            this.createImageListado(files[0]);
        },
        createImageListado(file) {
            const image = new Image();
            const reader = new FileReader();

            reader.onload = (e) => {
                this.imageListado = e.target.result;
            };
            reader.readAsDataURL(file);
        },



        uploadImageComunicado (e) {
            const files = e.target.files || e.dataTransfer.files;

            if (!files.length)
                return;

            const image = files[0];
            this.fileImageComunicadoName = image.name;
            this.createImageComunicado(files[0]);
        },
        createImageComunicado(file) {
            const image = new Image();
            const reader = new FileReader();

            reader.onload = (e) => {
                this.imageComunicado = e.target.result;
            };
            reader.readAsDataURL(file);
        },





        // Servicios
        uploadImageServicio (e) {
            const files = e.target.files || e.dataTransfer.files;

            if (!files.length)
                return;

            const image = files[0];
            this.fileImageServicioName = image.name;
            this.createImageServicio(files[0]);
        },
        createImageServicio(file) {
            const image = new Image();
            const reader = new FileReader();

            reader.onload = (e) => {
                this.imageServicio = e.target.result;
            };
            reader.readAsDataURL(file);
        },
        // Navbar
        toggleSidebarMobile (e) { //Se llama dando clic al burguer button en la navegación
            const button = event.target.closest('.navbar-burger');
            button.classList.toggle('is-active');
            const sidebar = document.getElementById('admin-sidebar');
            sidebar.classList.toggle('is-hidden-touch');
        },
        //Validation
        cleanDanger (e) {
            const element = e.target;
            const input = element.closest("input");
            input.classList.remove("is-danger");
        },
        // Gallery
        removeImageFromGallery(event, id) {
            const url = "/admin/galeria/image/remove/"+id;
            axios.post(url)
                .then(function (response) {
                    const element = event.target;
                    const image = element.closest(".column");
                    image.style.display = "none";
                })
                .catch(function (error) {
                    alert("Tuvimos un problema, por favor inténtalo más tarde");
                    console.log(error.response);
                });
        },

        // Products

        // Producto categories
        getProductsCategories () {
            const url = "/admin/productos/categorias/obtener";
            axios.get(url)
                .then(response => {
                    this.productsCategories = response.data;
                })
        },
        addNewProductCategory() {
            // this.inputNewProductCategory = true
            document.getElementById('fieldAddNewProductCategory').classList.remove('is-invisible');
            document.getElementById('input-new-product-category').focus();
        },
        cancelAddNewProductCategory() {
            this.newProductCategory = '';
            // this.inputNewProductCategory = false
            document.getElementById('fieldAddNewProductCategory').classList.add('is-invisible');
        },
        saveNewProductCategory() {
            const newCategory = this.newProductCategory;
            if(newCategory === '') {
                alert("Tienes que escribir la categoría");
                return
            }
            const url = "/admin/productos/categorias"; // Va al método store
            axios.post(url, {categoria: newCategory})
                .then(response =>{
                    this.$notify({
                        group: 'foo',
                        title: '',
                        text: '¡Se guardo exitósamente la categoría!'
                    });
                    const categoria = response.data;
                    // this.productsCategories.push({ nombre: newCategory });
                    this.productsCategories.push(categoria);
                    this.newProductCategory = '';
                    document.getElementById('fieldAddNewProductCategory').classList.add('is-invisible');
                })
                .catch(function (error) {
                    alert("Tuvimos un problema, por favor inténtalo más tarde");
                });
        },
        addNewProductSubcategory(event) {
            const element = event.target; // El elemento que dispara el evento
            const level = element.closest('.-level'); // El div que al que se va a agregar subcategoría
            level.querySelector('.-field-new-subcategory').classList.remove('is-hidden'); // Mostramos el input para agregar la subcategoría
        },
        cancelAddNewProductSubcategory (event) {
            const element = event.target; // El elemento que dispara el evento
            const level = element.closest('.-level'); // El div que al que se va a agregar subcategoría
            level.querySelector('.-field-new-subcategory').classList.add('is-hidden'); // Mostramos el input para agregar la subcategoría
        },
        /*saveNewProductSubcategory (event, parent_id) {*/
        saveNewProductSubcategory (event, parent) {
            const element = event.target; // El elemento que dispara el evento
            const level = element.closest('.-level'); // El div  al que se va a agregar subcategoría
            const inputSubcategory = level.querySelector('.-field-new-subcategory input[name="input-new-subcategory"]');
            const newSubcategory = inputSubcategory.value; // Mostramos el input para agregar la subcategoría
            const url = "/admin/productos/categorias"; // Va al método store

            axios.post(url, {categoria: newSubcategory, parent_id: parent.id})
                .then(response =>{
                    this.$notify({
                        group: 'foo',
                        title: '',
                        text: '¡Se guardo exitósamente la categoría!'
                    });
                    inputSubcategory.value = "";
                    this.getProductsCategories();
                    level.querySelector('.-field-new-subcategory').classList.add('is-hidden'); // Mostramos el input para agregar la subcategoría
                })
                .catch(function (error) {
                    alert("Tuvimos un problema, por favor inténtalo más tarde");
                    console.log(error);
                });

        },
        deleteNewProductSubcategory (category) {
            const confirm = window.confirm("Estas seguro de eliminar la categoría"+ category.nombre +" con todas sus subcategorías");
            if(!confirm) {
                return
            }
            const url = "/admin/productos/categorias/"+category.id; // Va al método store
            axios.delete(url, {categoriaId: category.id})
                .then(response =>{
                    this.$notify({
                        group: 'foo',
                        title: '',
                        text: '¡Se eliminó exitósamente la categoría!'
                    });
                    this.getProductsCategories();
                })
                .catch(function (error) {
                    alert("Tuvimos un problema, por favor inténtalo más tarde");
                });
        },
        openFormEditCategory(event){
            const element = event.target; // El elemento que dispara el evento
            const level = element.closest('.-level'); // El div que al que se va a agregar subcategoría
            level.querySelector('.-field-edit-category').classList.remove('is-hidden'); // Mostramos el input para agregar la subcategoría
        },
        cancelEditCategory (event) {
            const element = event.target; // El elemento que dispara el evento
            const level = element.closest('.-level'); // El div que al que se va a agregar subcategoría
            level.querySelector('.-field-edit-category').classList.add('is-hidden'); // Mostramos el input para agregar la subcategoría
            this.getProductsCategories();
        },
        updateCategory(event, category) {
            const url = "/admin/productos/categorias/"+category.id; // Va al método update
            const element = event.target; // El elemento que dispara el evento
            const level = element.closest('.-level');
            axios.put(url, {nombre: category.nombre})
                .then(response =>{
                    this.$notify({
                        group: 'foo',
                        title: '',
                        text: '¡Se actualizó exitósamente la categoría!'
                    });
                    this.getProductsCategories();
                    level.querySelector('.-field-edit-category').classList.add('is-hidden'); // Mostramos el input para agregar la subcategoría
                    // console.log(response)
                })
                .catch(function (error) {
                    alert("Tuvimos un problema, por favor inténtalo más tarde");
                });
        }
    } // End methods
});
