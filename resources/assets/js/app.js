
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('dropzone-component', require('./components/admin/components/DropZoneComponent.vue').default);

const app = new Vue({
    el: '#app',
    data: {
      //Gallery
      modalGalleryImage : '',
    },
    created: function () {
      /*this.masonryLayout(document.getElementById('gallery'), null, 3)*/
      //this.masonryLayout(document.getElementById('gallery'), document.querySelectorAll('.-gallery-item'), 3)



        this.masonryLayout(document.getElementById('gallery-portfolio'), document.querySelectorAll('.-gallery-item'), 4)


        this.masonryLayout(document.getElementById('gallery-deportivo'), document.querySelectorAll('.-gallery-item-d'), 4)
        this.masonryLayout(document.getElementById('gallery-intima'), document.querySelectorAll('.-gallery-item-i'), 4)
        this.masonryLayout(document.getElementById('gallery-tecnicos'), document.querySelectorAll('.-gallery-item-t'), 4)
        this.masonryLayout(document.getElementById('gallery-automotriz'), document.querySelectorAll('.-gallery-item-a'), 4)





    },
    methods: {

        /*
        *
        * Componentes
        *
        * */

        //Slider - simple
        nextSlide() {
            sliderItems = document.querySelectorAll('.-carousel-item')
            for(let i = 0; i < sliderItems.length; i += 1) {
                let currentItem = sliderItems[i]
                if(currentItem.classList.contains('-is-active')) {
                    let nextItem = sliderItems[i + 1]
                    if(nextItem){
                        nextItem.classList.add('-is-active')
                        currentItem.classList.remove('-is-active')
                        break
                    }
                    else {
                        currentItem.classList.remove('-is-active')
                        sliderItems[0].classList.add('-is-active')
                        break
                    }
                }
            }
        },
        previousSlide() {
            sliderItems = document.querySelectorAll('.-carousel-item')
            for(let i = 0; i < sliderItems.length; i += 1) {
                let currentItem = sliderItems[i]
                if(currentItem.classList.contains('-is-active')) {
                    let previousItem = sliderItems[i - 1]
                    if(previousItem){
                        previousItem.classList.add('-is-active')
                        currentItem.classList.remove('-is-active')
                        break
                    }
                    else {
                        const lastPosition = sliderItems.length - 1
                        currentItem.classList.remove('-is-active')
                        sliderItems[lastPosition].classList.add('-is-active')
                        break
                    }
                }
            }
        },
        // Slider - multiple
        showNextMutipleSlide (event) {
            /*
            *
            * Para aumentar el número de slides que se muestran basta con cambiar la variable
            * slidesShowed y asignar el número de slides
            * */
            const element = event.target; // Elento al que se da clic
            const slider = element.closest('.-multiple-slider'); // Slider
            const slides = slider.querySelectorAll(".-slide"); // Slides
            const slidesShowed = 3; // Número de slides que se muestran
            const totalSlides = slides.length; // Número de slides que hay en total
            let currenMarginLeft = 0; // Inicializamos en 0
            let lastSlide = slidesShowed; // Inicializamos
            /*
            *
            * Al hacer click para mostrar el slide siguiente verificamos que tenga un marginleft negativo,
            * si lo tiene es porque hay slides ocultos y reducimos el margin left para mostrar el siguiente slide oculto
            *
            * */
            const firstSlide = slides[0];
            const widthSlide =  firstSlide.offsetWidth;
            currenMarginLeft = firstSlide.style.marginLeft; //Margin left que tiene el primer slide
            currenMarginLeft = parseInt(currenMarginLeft, 10); // Parseamos a int para eliminar 'px' del valor obtenido
            if (currenMarginLeft < 0){
                const newMarginLeft = (currenMarginLeft + widthSlide)+"px";
                firstSlide.style.marginLeft = newMarginLeft;
                slider.querySelector('.-prev').classList.remove('-is-disabled')
            }
            else {
                element.closest('.-next').classList.add('-is-disabled');
            }

        },
        showPrevMutipleSlide (event) {
            /*
            *
            * Para aumentar el número de slides que se muestran basta con cambiar la variable
            * slidesShowed y asignar el número de slides
            * */
            const element = event.target; // Elento al que se da clic
            const slider = element.closest('.-multiple-slider'); // Slider
            const slides = slider.querySelectorAll(".-slide"); // Slides
            const slidesShowed = 3; // Número de slides que se muestran
            const totalSlides = slides.length; // Número de slides que hay en total
            let currenMarginLeft = 0; // Inicializamos en 0
            let lastSlide = slidesShowed; // Inicializamos
            /*
            *
            * Al hacer click para mostrar el slide anteriorr al primer slide se le da un margin
            * left negativo del tamaño del slide para que de esta manera tenga el efecto que que se
            * mueve a la izquierda
            *
            * */
            const firstSlide = slides[0];
            const widthSlide =  firstSlide.offsetWidth;
            let newMarginLeft = widthSlide * (-1); // Inicializamos marginLeft con el opuesto del tamaño del slide por ejemplo, si mide 350(px) esta variable se inicializa como -350
            currenMarginLeft = firstSlide.style.marginLeft; //Margin left que tiene el primer slide
            currenMarginLeft = parseInt(currenMarginLeft, 10); // Parseamos a int para eliminar 'px' del valor obtenido
            if (currenMarginLeft < 0){
                lastSlide =  slidesShowed + Math.abs(currenMarginLeft / widthSlide); // Obtenemos el número del último slide que se muestra
                newMarginLeft = (currenMarginLeft - widthSlide);
            }

            if (lastSlide < totalSlides) {
                firstSlide.style.marginLeft = newMarginLeft+"px";
                slider.querySelector('.-next').classList.remove('-is-disabled')
            }
            else {
                element.closest('.-prev').classList.add('-is-disabled');
            }
        }, // End showPrevMutipleSlide

        /*
        *  Masonry layout
        * */
        masonryLayout(containerElem, itemsElems, columns) {

            if (containerElem) {
                containerElem.classList.add('-masonry-layout', `-columns-${columns}`)
                let columnsElements = []

                for( let i = 1; i <= columns; i++){
                    let column = document.createElement('div')
                    column.classList.add('-masonry-column', `-column-${i}`)
                    containerElem.appendChild(column)
                    columnsElements.push(column)
                }

                let elementos = 0
                for(let m = 0; m < Math.ceil(itemsElems.length / columns); m++){
                    for(let n = 0; n < columns; n++){
                        let item = itemsElems[ m * columns + n]


                        elementos++
                        if(elementos <= itemsElems.length){
                            columnsElements[n].appendChild(item)
                            item.classList.add('-masonry-item')
                        }else{
                            break;
                        }
                    }
                }


            }

        },
        // Gallery
        openModalImageGallery (event) {
            const element = event.target;
            let image = '';
            if(element instanceof HTMLImageElement) {
                image = element.src;
            }
            else {
                image = element.querySelector('img').src;
            }
            this.modalGalleryImage = image;
            this.openModal('modal-image-gallery');
        },
        closeModalImageGallery() {
          this.closeModal('modal-image-gallery');
          this.modalGalleryImage = '';
        },

        // Productos
        changeMainImage (event) { // Al hacer hover sobre una imagen del producto la podne en lugar de la imagen principal
            const element = event.target;
            const mainProductImage = document.getElementById('main-product-image');

            const image = element.querySelector('img').src;
            mainProductImage.src = image
        },
        // Menú
        showMenu (event) {
            if(document.body.clientWidth < 768) {
                return 
            }
            // console.log(document.body.clientWidth)
            const iconLogo = document.getElementById('icon_logo')
            iconLogo.classList.remove('fadeInLeft')
            iconLogo.classList.add('fadeOutLeft')
            const logo = document.getElementById('logo')
            setTimeout(() => {
                logo.classList.remove('is-invisible')
            }, 500);
            logo.classList.remove('fadeOut')
            logo.classList.add('fadeIn')
            const navigation = document.getElementById('navbar')
            navigation.classList.add('is-active')
        },
        toggleMobileMenu(event) {
            // alert('togleamos')
            const navigation = document.getElementById('navbar')
            navigation.classList.toggle('is-active')
            // const navigation = document.getElementById('navbar')
            // navigation.classList.add('is-active')
        },
        leaveNavigation () {
            const logo = document.getElementById('logo')
            logo.classList.add('fadeOut')
            setTimeout(() => {
                logo.classList.add('is-invisible')
            }, 500);

            const iconLogo = document.getElementById('icon_logo')
            // iconLogo.classList.remove('fadeOut')
            iconLogo.classList.remove('fadeOutLeft')
            // iconLogo.classList.add('fadeIn')
            iconLogo.classList.add('fadeInLeft')

            const navigation = document.getElementById('navbar')
            navigation.classList.remove('is-active')
        },
        // Generales
        openModal (id) {
            document.getElementById(id).classList.add('is-active');
        },
        closeModal (id) {
            document.getElementById(id).classList.remove('is-active');
        }


    }

});
