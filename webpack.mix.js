let mix = require('laravel-mix');

//Browser reload
// mix.browserSync('localhost:7000');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.webpackConfig({ devtool: "inline-source-map" });
if (!mix.inProduction()) {
    mix.webpackConfig({devtool: 'inline-source-map'}).sourceMaps();
}
mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/admin.js', 'public/adminn/js')
    .sass('resources/assets/sass/cliente/cliente.sass', 'public/cliente/css')
    .sass('resources/assets/sass/admin/admin.sass', 'public/adminn/css');
