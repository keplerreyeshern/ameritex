/* Datepicker */
$(document).ready(function () {
    // Dropzone.autoDiscover = false;
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
});

$(function () {
    $(".datepicker").datepicker({
        dateFormat: "dd/mm/yy",
        language: "es",
        autoclose: true
    });
});


/* Timymce*/
/*editor_config.selector = "textarea";
// editor_config.path_absolute = "http://lai.matrix-g.com/";
editor_config.path_absolute = "http://localhost:7000/";
tinymce.init(editor_config);*/

/*var editor_config = {
    path_absolute : "/",
    selector: "textarea",
    language: 'es',
    plugins: [
        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
        "searchreplace wordcount visualblocks visualchars code fullscreen",
        "insertdatetime media nonbreaking save table contextmenu directionality",
        "emoticons template paste textcolor colorpicker textpattern",
        "preview textcolor"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

        // var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
        if (type == 'image') {
            cmsURL = cmsURL + "&type=Images";
        } else {
            cmsURL = cmsURL + "&type=Files";
        }

        tinyMCE.activeEditor.windowManager.open({
            file : cmsURL,
            title : 'Filemanager',
            width : x * 0.8,
            height : y * 0.8,
            resizable : "yes",
            close_previous : "no"
        });
    }
};

tinymce.init(editor_config);*/



/*Dropzone*/
function validateFormData(form) {
    inputs = document.querySelectorAll("#" + form + " input[required]");
    for (i = 0; i < inputs.length; i++) {
        input = inputs[i];
        if (input.value === '') {
            input.classList.add('is-danger');
            parentContainer = input.closest('.control');
            message = parentContainer.nextElementSibling;
            message.classList.remove('is-invisible');
            window.scrollTo(0, 0);
            return false
        } else {
            input.classList.remove('is-danger');
            parentContainer = input.closest('.control');
            message = parentContainer.nextElementSibling;
            message.classList.add('is-invisible');
        }
    }
    return true
}

Dropzone.options.galleryDropzone = {
    autoProcessQueue: false,
    url: "/admin/galerias",
    uploadMultiple: true,
    addRemoveLinks: true,
    parallelUploads: 30,
    headers: {
        //'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content'),
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),

    },
    init: function () {
        var submitButton = document.querySelector("#btn-submit-gallery")
        myDropzone = this;
        submitButton.addEventListener("click", function () {
            if (validateFormData("create-new-gallery")) {
                myDropzone.processQueue();
            }
            else {
                return
            }
        });
        this.on("sending", function (file, xhr, formData) {
            formData.append("nameGallery", document.getElementById('gallery-name').value);
            formData.append("dateGallery", document.getElementById('gallery-date').value);
            formData.append("descriptionGallery", document.getElementById('gallery-description').value);
        });
        myDropzone.on("complete", function (file) {
            //console.log(this)
        });
    },
    success: function (file, response) {
        // similar behavior as an HTTP redirect
        window.location.replace("/admin/galerias");
    },
    error: function (file, response) {
        alert("Tuvimos un problema, por favor inténtalo más tarde.")
        console.log(response)
    }
}

Dropzone.options.dropzoneEditGalleryy = {
    addRemoveLinks: true,
    headers: {
        'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
    },
    success: function(file, response){
        console.log(file);
        console.log(response)
    },
    error: function (file, response) {
        console.log(file);
        console.log(response);
        alert("Tuvimos un problema, por favor inténtalo más tarde.")
    }
}

// Dropzone.options.dropzoneEditGallery = {
//     addRemoveLinks: true,
//     headers: {
//         'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
//     },
//     success: function(file, response){
//         console.log(response)
//     },
//     error: function (file, response) {
//         alert("Tuvimos un problema, por favor inténtalo más tarde.")
//     }
// }
var edit = document.getElementById('_id-product')
if(!edit) {
    Dropzone.options.productDropzone = {
        autoProcessQueue: false,
        url: "/admin/productos",
        uploadMultiple: true,
        addRemoveLinks: true,
        parallelUploads: 30,
        headers: {
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        },
        init: function () {
            var submitButton = document.querySelector("#btn-submit-new-product")
            myDropzone = this;
            submitButton.addEventListener("click", function () {
                if(!myDropzone.files.length) {
                    var form = document.getElementById('form-product')
                    if (validateFormData("form-product")) {
                        form.submit();
                    }
                }
                if (validateFormData("form-product")) {
                    myDropzone.processQueue();
                    console.log(myDropzone)
                }
                else {
                    return
                }
            });
            this.on("sending", function (file, xhr, formData) {
                formData.append("nameProduct", document.querySelector('input[name="nombre"]').value);
                formData.append("pricingProduct", document.querySelector('input[name="precio"]').value);
                if(document.querySelector('input[name="fotolistado"]').files[0]){
                    formData.append("fotolistado", document.querySelector('input[name="fotolistado"]').files[0]);
                }
                formData.append("descriptionProduct", tinyMCE.get('descripcion-producto').getContent());
                formData.append("detailsProduct", tinyMCE.get('detalles-producto').getContent());
                formData.append("categories", getCheckedSubcategories());
            });
            myDropzone.on("complete", function (file) {
                //console.log(this)
            });
        },
        success: function (file, response) {
            // similar behavior as an HTTP redirect
            console.log(response)
            window.location.replace("/admin/productos");
        },
        error: function (file, response) {
            alert("Tuvimos un problema, por favor inténtalo más tarde.")
        }
    }
}
else {
    Dropzone.options.dropzoneEditGallery = {
        url: "/admin/productos/"+document.getElementById('_id-product').value,
        // method: "PUT",
        autoProcessQueue: false,
        uploadMultiple: true,
        addRemoveLinks: true,
        parallelUploads: 30,
        headers: {
            'X-CSRF-Token': document.querySelector('meta[name="csrf-token"]').getAttribute('content'),
        },
        init: function () {
            var submitButton = document.querySelector("#btn-submit-update-product")
            myDropzone = this;
            submitButton.addEventListener("click", function () {
                if(!myDropzone.files.length) {
                    var form = document.getElementById('form-product')
                    form.submit();
                }
                if (validateFormData("form-product")) {
                    myDropzone.processQueue();
                }
                else {
                    return
                }
            });
            this.on("sending", function (file, xhr, formData) {
                formData.append("nameProduct", document.querySelector('input[name="nombre"]').value);
                formData.append("pricingProduct", document.querySelector('input[name="precio"]').value);
                if(document.querySelector('input[name="fotolistado"]').files[0]){
                    formData.append("fotolistado", document.querySelector('input[name="fotolistado"]').files[0]);
                }
                formData.append("descriptionProduct", tinyMCE.get('descripcion-producto').getContent());
                formData.append("detailsProduct", tinyMCE.get('detalles-producto').getContent());
                formData.append("categories", getCheckedSubcategories());
            });
            myDropzone.on("complete", function (file) {
                // console.log(this)
            });
        },
        success: function(file, response){
            // console.log(response)
            window.location.replace("/admin/productos");
        },
        error: function (file, response) {
            alert("Tuvimos un problema, por favor inténtalo más tarde.")
            console.log(response)
        }
    }
}


function  getCheckedSubcategories() {
    var checked = document.querySelectorAll("input[type=checkbox]:checked");
    subcategories = [];
    checked.forEach(subcategorie => {
        dataSubcategorie = subcategorie.value
        subcategories.push(dataSubcategorie);
    })
    return subcategories;
}
